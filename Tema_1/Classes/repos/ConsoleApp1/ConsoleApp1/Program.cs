﻿// See https://aka.ms/new-console-template for more information

using System;

class Program
{


    public static void Main()
    {
        Console.WriteLine();
        Console.WriteLine("Hello, Pipol2222");
        Console.WriteLine();
        /*var num1 = 3;
        var num2 = 7;
        var resultado = num1 - num2;
        var resultado2 = (double)num1 / (double)num2;
        Console.WriteLine("Resultat resta: " + resultado);
        Console.WriteLine();

        Console.WriteLine("Resultat divisió: " + resultado2);*/

        // Creamos las variables
        var nota1 = 9.5;
        var nota2 = 5.7;
        var media = CalcularMedia(nota1, nota2);
        PrintAverage(nota1, nota2);

        // Ahora crearemos la función que nos calculara la media (CalcularMedia)
        // Esta función tendrá dos parámetros de entrada que serán las dos notas que utilizaremos para calcular
        // Dentro de la función crearemos una variable que guardara el valor de la nota media y será el que 
        // devolvamos con un return
        static double CalcularMedia(double a, double b)
        {
            var notaMedia = (a + b) / 2;
            return notaMedia;
        }
        // De esta manera estamos dándole a la variable media el valor de notaMedia

        static double PrintAverage(double a, double b)
        {
            var avg = CalcularMedia(a, b) ;
            Console.WriteLine("La media de: " + a.ToString() + " i " + b + "és: " + avg);
        }
        // De esta manera estamos dándole a la variable media el valor de notaMedia

        Console.WriteLine();

    }

    /*  int año = 2000;
      double xifra = 7.5;
      int xifra2 = (int) xifra;

      // 1r scope
      {
      int c = 4;  
      int d = 5; 
      var j = 7;
      var h = 23;

      Console.WriteLine(d);

      } 

      }
  static void Array()
      // Esto es un array
      // Primero se declara el array con un tamaño fijo por ejemplo 5

  {
      var arrayAños = new int[5];

      // Ahora llenamos el array con los valores deseados
      arrayAños[0] = 1929;
      arrayAños[1] = 1972;
      arrayAños[2] = 2001;
      arrayAños[3] = 2008;
      arrayAños[4] = 2020;

  }

  static void List()
      // Primero declararemos la List, a diferencia del array no tendrá un tamaño fijo
  {

      var listAños = new List<int>();

      // Añadiremos los valores utilizando la propiedad Add de las List
      listAños.Add(1929);
      listAños.Add(1972);
      listAños.Add(2001);
      listAños.Add(2008);
      listAños.Add(2020);
      }

  static void Dicccionari()
      // Esto es un Dictionary
      // Empezamos declarando nuestro diccionario
      // El primer int será la posición y el segundo el valor
  {
      var dictionaryAños = new Dictionary<int, int>(); 

      // Añadamos valores a nuestro diccionario. Utilizaremos la propiedad Add como 
      // en las List.
      dictionaryAños.Add(4, 2008);
      dictionaryAños.Add(2, 1929);
      dictionaryAños.Add(0, 2020);
      dictionaryAños.Add(1, 2001);
      dictionaryAños.Add(3, 1972);
      // Como veis primero indicamos la posición del diccionario donde irá el valor
      // y después el valor en sí.
  }
  //ENUMS
  public enum Days
  {
       Lunes = 1,
       Martes = 2,
       Miercoles = 3,
       Jueves = 4,
       Viernes = 5,
       Sabado = 6,
       Domingo = 7
  } */

}
