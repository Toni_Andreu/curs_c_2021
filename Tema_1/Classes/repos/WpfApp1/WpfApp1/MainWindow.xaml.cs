﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


            var stu1 = new Student()
            {
                Name = "lolo",
                Email = "lolo@gmail.com"
            };

            var stu2 = new Student()
            {
                Name = "Pepe",
                Email = "pepe@gmail.com"
            };


            var students = new ObservableCollection<Student>();
            students.Add(stu1);
            students.Add(stu2);

            DgStudents.ItemsSource = students;
        }

        private void BtSelectStudent_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = ((Button)sender).DataContext as Student;
        }
    }

    public class Student
    {
        public string Name {  get; set; }

        public string Email { get; set; }
    }
}
