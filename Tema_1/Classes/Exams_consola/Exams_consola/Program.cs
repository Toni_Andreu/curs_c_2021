﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            programaClases();

        }

        static void ProgramacioEstructurada()
        {
            // representar dades amb variables
            var name = "Pepe";
            var year = 1978;
            var email = "a@a.com";
            ShowPersonaInformationOld(name, year, email);
        }

        static void programaClases()
        { 
            /// creem un objecte NEW tipus Persona
            var pepe = new Persona();
            pepe.Name = "Pepe";
            pepe.Year = 1978;
            pepe.Email = "a@a.com";
           
            // forma 3 = igual a l'1!!
            var maria = new Persona
            {
                Name = "María",
                Year = 1987,
                Email = "m@m.com"
            };

            // forma 2 passant paràmetres directament, usant el constructor!!
            var lolo = new Persona("lolo", 2019, "b@b");


            //ShowPersonaInformation(Persona persona);
            ShowPersonaInformation(pepe);
            ShowPersonaInformation(lolo);
            ShowPersonaInformation(maria);

            Console.WriteLine();

            //print directe des de Persona!!! Crida a la CLasse!
            pepe.ShowInfo();
            lolo.ShowInfo();
            maria.ShowInfo();
        }

        // funció per print de les dades
        static void ShowPersonaInformationOld(string name, int year, string email)
        {
            Console.WriteLine("función estática OLD: la persona con nombre " + name
                + " que nació en el año del señor " + year
                + " y que tiene de email " + email);
        }
        
        static void ShowPersonaInformation(Persona persona)
        {
            Console.WriteLine("función estática: la persona con nombre " + persona.Name
                + " que nació en el año del señor " + persona.Year
                + " y que tiene d+e email " + persona.Email);
        }
}

    class Persona
    {
        //propietats = fields
        public string Name { get; set; }

        public int Year { get; set; }

        public string Email { get; set; }

        //Constructor = 1r mètode (funció de la CLasse)
        public Persona()
        {

        }
        // un altre constructor
        public Persona(string name, int year, string email)
        {
            this.Name = name;
            this.Year = year;
            this.Email = email;
        }


        // Métode per fer el print des de Persona => Solid
        public void ShowInfo()
        {
            Console.WriteLine("mètode de Classe: la persona con nombre " + this.Name
                    + " que nació en el año del señor " + this.Year
                    + " y que tiene de email " + this.Email);
        }

        // esto es un overload (sobrecarga) del método anterior
        public void ShowInfo(bool keepSecrets)
        {

        }
    }
}
