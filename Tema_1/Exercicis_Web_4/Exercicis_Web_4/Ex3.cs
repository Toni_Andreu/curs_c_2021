﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicis_Web_4
{
    public class Exercici3
    {
        public static void Calcula()
        {
            string st1;
            string st2;
            string st3;
            string st4;
            string st5;
            string st6;

            //demana introduir el nom de 6 ciutats
            Console.WriteLine("Escriu el nom: Barcelona");
            st1 = Console.ReadLine();
            Console.WriteLine("Escriu el nom: Madrid");
            st2 = Console.ReadLine();
            Console.WriteLine("Escriu el nom: València");
            st3 = Console.ReadLine();
            Console.WriteLine("Escriu el nom: Màlaga");
            st4 = Console.ReadLine();
            Console.WriteLine("Escriu el nom: Cadis");
            st5 = Console.ReadLine();
            Console.WriteLine("Escriu el nom: Santander");
            st6 = Console.ReadLine();

            //print
            Console.WriteLine("\n");
            Console.WriteLine("La ciutat 1: " + st1);
            Console.WriteLine("La ciutat 2: " + st2);
            Console.WriteLine("La ciutat 3: " + st3);
            Console.WriteLine("La ciutat 4: " + st4);
            Console.WriteLine("La ciutat 5: " + st5);
            Console.WriteLine("La ciutat 6: " + st6);

            //Console.WriteLine("\n");
            Console.Read();

            //crea array ciutats
            string[] arrayCiutats = new string[6] {st1, st2, st3, st4, st5, st6};
            foreach (string value in arrayCiutats)
            {
                Console.Write(value + ", ");
            }
            Console.WriteLine("\n");
            Console.Read();

            Array.Sort(arrayCiutats);
            Console.WriteLine("Array ja ordenat: ");
            foreach (string value in arrayCiutats)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\n");
            Console.Read();

            //copia a arrayModificada
            string[] arrayCiutatsModificada = new string[6];
            arrayCiutats.CopyTo(arrayCiutatsModificada, 0);
            //arrayCiutatsModificada = arrayCiutats.Replace('a', '4');

            //substitute "a" per "4"
            for (int i = 0; i < arrayCiutatsModificada.Length; i++)
            {
                arrayCiutatsModificada[i] = arrayCiutats[i].Replace('a', '4');
            }

            Array.Sort(arrayCiutatsModificada);
            Console.WriteLine("Array Modificada ja ordenada: ");

            //print llista final
            foreach (var c in arrayCiutatsModificada)
            {
                //Console.Write(c);
                Console.Write($"'{c}',");
            }
            Console.WriteLine("\n");

            Console.Read();

            //Independitza cada array, segons Lenght
            string[] arrayCiutat1 = new string[st1.Length];
            string[] arrayCiutat2 = new string[st2.Length];
            string[] arrayCiutat3 = new string[st3.Length];
            string[] arrayCiutat4 = new string[st4.Length];
            string[] arrayCiutat5 = new string[st5.Length];
            string[] arrayCiutat6 = new string[st6.Length];

            //coloca lletres
            for (int i = 0; i < arrayCiutat1.Length; i++)
            {
                arrayCiutat1[i] = st1.Substring(i,1);
            }
            for (int i = 0; i < arrayCiutat2.Length; i++)
            {
                arrayCiutat2[i] = st2.Substring(i, 1);
            }
            for (int i = 0; i < arrayCiutat3.Length; i++)
            {
                arrayCiutat3[i] = st3.Substring(i, 1);
            }
            for (int i = 0; i < arrayCiutat4.Length; i++)
            {
                arrayCiutat4[i] = st4.Substring(i, 1);
            }
            for (int i = 0; i < arrayCiutat5.Length; i++)
            {
                arrayCiutat5[i] = st5.Substring(i, 1);
            }
            for (int i = 0; i < arrayCiutat6.Length; i++)
            {
                arrayCiutat6[i] = st6.Substring(i, 1);
            }

            // print de control
            //foreach (string value in arrayCiutat1)
            //{
            //    Console.Write(value + ", ");
            //}
            //Console.WriteLine("\n");
            //Console.Read();


            //inverteix l'array
            Array.Reverse(arrayCiutat1);
            Array.Reverse(arrayCiutat2);
            Array.Reverse(arrayCiutat3);
            Array.Reverse(arrayCiutat4);
            Array.Reverse(arrayCiutat5);
            Array.Reverse(arrayCiutat6);

            //print final
            Console.WriteLine("\nArray 1 Invertit: ");
            foreach (string value in arrayCiutat1)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\nArray 2 Invertit: ");
            foreach (string value in arrayCiutat2)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\nArray 3 Invertit: ");
            foreach (string value in arrayCiutat3)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\nArray 4 Invertit: ");
            foreach (string value in arrayCiutat4)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\nArray 5 Invertit: ");
            foreach (string value in arrayCiutat5)
            {
                Console.Write(value + " ");
            }
            Console.WriteLine("\nArray 6 Invertit: ");
            foreach (string value in arrayCiutat6)
            {
                Console.Write(value + " ");
            }

            Console.WriteLine("\n");
            Console.Read();

        }
    }
}
