﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicis_Web_4
{
    public class Exercici2
    {
        public static void Calcula()
        {
            char[] lletres = { 'n', 'o', 'm', 'b', 'r', 'e' };

            for (int i = 0; i < lletres.Length; i++)
            {
                Console.Write(lletres[i] + "\n");
            }
            Console.Read();

            // passa a Llista
            List<char> lst = new List<char>();
            for (int i = 0; i < lletres.Length; i++)
            {
                lst.Add(lletres[i]);
            }
            //Console.Write(lst[3] + "\n");
           //Console.Write(lst + "\n");


            //Print vocals/consonants
            for (int i = 0; i < lst.Count; i++)
            {
                bool isNumeric = int.TryParse(lst[i].ToString(), out _);
                int vocal = 0;
                int consonant = 0;

                if (isNumeric)
                {
                    Console.WriteLine($"Això: {lst[i]} no és una lletra!!" + "\n");
                }
                    
                else if (lst[i] =='a'|| lst[i] == 'e' || lst[i] == 'i' || lst[i] == 'o' || lst[i] == 'u')
                {
                    Console.Write($"Aquesta {lst[i]} és una vocal!" + "\n");
                    vocal++;
                }
                else
                {
                    Console.Write($"Aquesta {lst[i]} és una consonant!" + "\n");
                    consonant++;
                }
               
            }

            //converteix a Dictionary
            Dictionary<char, int> dic = new Dictionary<char, int>();

            foreach (char c in lst)
            {
                if(!dic.ContainsKey(c))
                    dic.Add(c, 1);
                else dic[c]++;  
            }

            foreach (var c in dic)
            {
                Console.WriteLine($"Dictionary de Noms: {c} " + "\n");
            }

            Console.Read();

            //llista cognom
            char[] cognom = { 'a', 'p', 'e', 'l', 'l', 'i', 'd', 'o', 's' };

            // passa a Llista
            List<char> lstCognom = new List<char>();
            for (int i = 0; i < cognom.Length; i++)
            {
                lstCognom.Add(cognom[i]);
            }
            //Console.Write(lstCognom[6] + "\n");

            //converteix a Dictionary
            Dictionary<char, int> dicCognoms = new Dictionary<char, int>();

            foreach (char c in lstCognom)
            {
                if (!dicCognoms.ContainsKey(c))
                    dicCognoms.Add(c, 1);
                else dicCognoms[c]++;
            }

            foreach (var c in dicCognoms)
            {
                Console.WriteLine($"Dictionary de Cognoms: {c} " + "\n");
            }

            Console.Read();

            List<char> lstFull = new List<char>();
            //Concatena llistes
            lstFull.AddRange(lst);
            //Console.Write(lstFull[3] + "\n");
            lstFull.AddRange(lstCognom);
            //Console.Write(lstFull[6] + "\n");


            //print llista final
            foreach (var c in lstFull)
            {
                //Console.Write(c);
                Console.Write($"'{c}',");
            }

            Console.WriteLine("\n");
            Console.WriteLine("\n");
      
            Console.Read();
        }
    }
}
