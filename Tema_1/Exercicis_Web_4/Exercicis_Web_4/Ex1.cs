﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercicis_Web_4
{
    static class Constants
    {
        public const int anyTraspas = 1948;
        public const int periodicitat = 4;
    }
    
    public class Exercici1
    {
        //no modificable
        public static bool Any4(int any)
        {
            bool anyTraspas = false;
            // Divisible per 4
            if (any % 4 == 0)
            {
                anyTraspas = true;
            }
            else
            {
                anyTraspas = false;
            }
            return anyTraspas;
        }

        private static void ExecutePhase1(int dia, int mes, int anyNaixement)
        {
            int numAnysTraspas;

            //string nomComplet = "El meu nom és " + cognom1 + " " + cognom2 + ", " + nom ;
            string nomComplet = ("El meu nom és {cognom1} {cognom2}, {nom}");
            string Naixement = "Vaig neixer: " + dia + "/" + mes + "/" + anyNaixement;

            numAnysTraspas = (anyNaixement - Constants.anyTraspas) / Constants.periodicitat;

            Console.WriteLine(nomComplet + "\n");
            //Console.ReadLine();
            Console.WriteLine(Naixement + "\n");
            //Console.ReadLine();
            Console.WriteLine("Van haver-hi " + numAnysTraspas + " anys de traspàs des que vaig néixer." + "\n");
            //Console.ReadLine();
        }

        private static void ExecutePhase2(int anyNaixement)
        {


            string siTraspas = "L'any que vaig néixer va ser un any de traspàs!";
            string noTraspas = "L'any que vaig néixer va ser un any normal";
            bool resultNaixement = Any4(anyNaixement);
            // Divisible per 4
            if (resultNaixement)
            {
                Console.WriteLine(siTraspas);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine(noTraspas);
                Console.ReadLine();
            }


            //lOOP anys de traspàs des de 1964
            bool anyTraspas = false;
            for (int i = Constants.anyTraspas; i == anyNaixement; i++)
            {
                bool result = Any4(i);
                string Traspas = "L'any que vaig néixer va ser un any de traspàs!";
                if (result)
                {
                    Console.WriteLine($"L'any: {i} va ser un any de traspàs.");
                }
                else { }
            }
        }

        public static void Calcula()
        {
            string nom = "Toni";
            string cognom1 = "Andreu";
            string cognom2 = "Sin";
            int dia = 26;
            int mes = 07;
            int anyNaixement = 1964;
           


            ExecutePhase1( dia, mes, anyNaixement);
            ExecutePhase2(anyNaixement);  
        }
    } 
}
