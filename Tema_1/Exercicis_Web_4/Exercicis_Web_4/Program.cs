﻿// See https://aka.ms/new-console-template for more information
using Exercicis_Web_4;
class Program
{
    static string EscapeWord = "ESCAPA";

    static void Main(string[] args)
    {
        Console.WriteLine("Bon dia !" + "\n");

        var keepdoing = true;
        Console.WriteLine("Quin exercici vols printejar? " + "\n" +
            "1 = Variables i Bucles\n" +
            "2 = Lletres repetides\n" +
            "3 = Noms de ciutats\n" +
            "4 = Menú Restaurant\n\n" +
            "Introdueixi un nombre del 1 al 4:");

        while (keepdoing)
        {
            string input2 = Console.ReadLine();
            bool num = int.TryParse(input2, out _);
            var input = Console.ReadKey();
            
    
            if (input2 == EscapeWord)
            {
                keepdoing = false;
                Console.WriteLine("\n");
                Console.WriteLine("\n");

            }
            else if (num == false)
            {
                Console.WriteLine("******************************\n");
                Console.WriteLine("El sistema no admet lletres!" + "\nTorni a intentar-ho!\n");
                keepdoing = false;
                Console.WriteLine("\n");
                Console.WriteLine("******************************\n");
            }
            else if (input2.Length > 1)
            {
                Console.WriteLine("******************************\n");
                Console.WriteLine("Clicar un múmero només!!" + "\nTorni a intentar-ho!\n");
                //keepdoing = false;
                Console.WriteLine("\n");
                Console.WriteLine("******************************\n");
            }

            else
            {
                int reinput = int.Parse(input.KeyChar.ToString());
                Console.WriteLine("\n");
                Console.WriteLine("_____________________________________\n");

                switch (reinput)
                {
                    case 1:
                        {
                            Console.WriteLine("Anem a fer l'Exercici.1: VARIABLES I BUCLES" + "\n");
                            Console.WriteLine("_____________________________________\n");
                            Exercici1.Calcula();
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Anem a fer l'Exercici.2: LLETRES REPETIDES" + "\n");
                            Console.WriteLine("_____________________________________\n");
                            Exercici2.Calcula();

                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Anem a fer l'Exercici.3: NOMS DE CIUTATS" + "\n");
                            Console.WriteLine("_____________________________________\n");
                            Exercici3.Calcula();
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine("Anem a fer l'Exercici.4: MENÚ RESTAURANT" + "\n");
                            Console.WriteLine("_____________________________________\n");
                            Exercici4.Calcula();
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("ERROR!!  Cal introduir un nombre!! (de l'1 al 4 only)" + "\n");
                            Console.WriteLine("_____________________________________\n");
                            break;

                        }


                }
            }
        
        }
        Console.WriteLine("_____________________________________\n");
        Console.WriteLine("MOLTES GRÀCIES I FINS A LA PROPERA!\n");
        Console.WriteLine("_____________________________________\n");
        Console.WriteLine("\n");
        Console.WriteLine("\n");
    }
}