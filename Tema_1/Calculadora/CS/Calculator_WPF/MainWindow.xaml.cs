﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double lastNumber, result;
        private SelectedOperator selectedOperator;

        public MainWindow()
        {
            InitializeComponent();
            Screen.Content = "0";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int selectedValue = 0;

            if (sender == btn0)
                selectedValue = 0;
            if (sender == btn1)
                selectedValue = 1;
            if (sender == btn2)
                selectedValue = 2;
            if (sender == btn3)
                selectedValue = 3;
            if (sender == btn4)
                selectedValue = 4;
            if (sender == btn5)
                selectedValue = 5;
            if (sender == btn6)
                selectedValue = 6;
            if (sender == btn7)
                selectedValue = 7;
            if (sender == btn8)
                selectedValue = 8;
            if (sender == btn9)
                selectedValue = 9;
           
            Screen.Content = Screen.Content.ToString() == "0" ? Screen.Content = $"{selectedValue}" : Screen.Content = $"{Screen.Content}{selectedValue}";
        }

    private void btnOperation_Click(object sender, RoutedEventArgs e)
        {
            if (double.TryParse(Screen.Content.ToString(), out lastNumber))
            {
                Screen.Content = "0";
            }

            if (sender == btnMultiu)
                selectedOperator = SelectedOperator.Multiplicatiom;
            if (sender == btnDivi)
                selectedOperator = SelectedOperator.Division;
            if (sender == btnAdd)
                selectedOperator = SelectedOperator.Addition;
            if (sender == btnSubt)
                selectedOperator = SelectedOperator.Sustraction;

        }

        private void Button_Equal(object sender, RoutedEventArgs e)
        {
            double newNumber;
            if (double.TryParse(Screen.Content.ToString(), out newNumber))
            {
                switch (selectedOperator)
                {
                    case SelectedOperator.Addition:
                        result = SimpleMath.Add(lastNumber, newNumber);
                        break;
                    case SelectedOperator.Sustraction:
                        result = SimpleMath.Minus(lastNumber, newNumber);
                        break;
                    case SelectedOperator.Multiplicatiom:
                        result = SimpleMath.Multiple(lastNumber, newNumber);
                        break;
                    case SelectedOperator.Division:
                        result = SimpleMath.Divide(lastNumber, newNumber);
                        break;
                }

                Screen.Content = result.ToString();
            }
        }
        public class SimpleMath
        {
            public static double Add(double numberOne, double numberTwo)
            {
                return numberOne + numberTwo;
            }

            public static double Minus(double numberOne, double numberTwo)
            {
                return numberOne - numberTwo;
            }
            public static double Multiple(double numberOne, double numberTwo)
            {
                return numberOne * numberTwo;
            }
            public static double Divide(double numberOne, double numberTwo)
            {
                return numberOne / numberTwo;
            }
        }
        private void Button_Clear(object sender, RoutedEventArgs e)
        {
            Screen.Content = "";
        }

        private void Button_Negative(object sender, RoutedEventArgs e)
        {
            Screen.Content = double.TryParse(Screen.Content.ToString(), out lastNumber) ? lastNumber = lastNumber * (-1) : Screen.Content;
        }

        private void Button_Decimal(object sender, RoutedEventArgs e)
        {
            if (!Screen.Content.ToString().Contains("."))
                Screen.Content = $"{Screen.Content}.";
        }












        ///private void Button_Click1(object sender, RoutedEventArgs e)
        ///{
            /// LbOutput.Content += "1";
           /// AddString("1");
        ////}

        
    }
}
