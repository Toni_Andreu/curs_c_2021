﻿// See https://aka.ms/new-console-template for more information

class Program
{
    static string EscapeWorld = "ESCAPAAA";

    static void Main(string[] args)
    {
        Console.WriteLine("Benvinguts al progrma per a la gestió d'Alumnes!");
        Console.WriteLine("Introdueixi les notes del Alumnes");

        var notesAlumnes = new List<double>();
        var keepdoing = true; 

        while (keepdoing)
        {
            Console.WriteLine($"\nNota de l'Alumne {notesAlumnes.Count + 1}: ");
            var notaText = Console.ReadLine();

            if (notaText == EscapeWorld)
            {
                keepdoing = false;
            }
            else
            {
                var nota = 0.0;

                // conversió a double
                if (double.TryParse(notaText.Replace(".", ","), out nota))
                {
                    notesAlumnes.Add(nota);
                }
                else
                {
                    Console.WriteLine("La nota introduïda NO és correcta!");
                }
            }
        }

        var suma = 0.0;
        for(int i = 0; i < notesAlumnes.Count; i++)
        {
            suma += notesAlumnes[i];
        }
            
        var average = suma/notesAlumnes.Count;
        Console.WriteLine("\n\n_____________________________________\n");
        Console.WriteLine($"La mitja dels exàmens és: {average}.");
        Console.WriteLine("Per si no queda clar: La mitja dels exàmens és: {0}.", average);

        var max = notesAlumnes.Max();
        Console.WriteLine("\nLa nota més alta dels exàmens és: {0}.", max);

        var min = notesAlumnes.Min();
        Console.WriteLine("\nLa nota més baixa dels exàmens és: {0}.", min);
        Console.WriteLine("\n\n_____________________________________\n\n");
    }



}
