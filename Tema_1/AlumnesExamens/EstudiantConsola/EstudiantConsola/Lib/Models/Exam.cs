﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantConsola.Lib.Models
{
    public class Exam : Entity
    {
        public string StudentDni { get; set; }
        public string Subject { get; set; }
        public double Mark { get; set; }
        public DateTime TimeStamp { get; set; }

        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            return nom.Length != 9;
        }
        // Nom existent
        //public static bool ValidateExist(string nom)
        //{
        //    //return Student.ContainsKey(nom);
        //    return false;
        //}
        // Nota en rang 0-10.0
        public static bool ValidateRang(double mark)
        {
            return (0.0 <= mark) && (mark <= 10.0);
        }
        # endregion

        #region CONSTANTS

        //Subject not empty
        public const string ErrBuidSubject = "El nom de l'Asignatura no pot estar buid!";
        // no existeix
        public const string ErrSubjectDniNoExist = "No existeix cap Exàmen de l'Asignatura per aquest alumne!";
        // JA existeix
        public const string ErrSubjectExist = "Ja existeix un exàmen per aquest Alumne i Asignatura:";
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // Nom Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte";
        // Nom existent
        //public const string ErrNoExisteix = "El dni no pot estar buid!";
        // Mark not empty
        public const string ErrBuidMark = "La Nota no pot estar buida!";
        // Mark 0 - 10.0
        public const string ErrMarkRang = "La Nota ha de tenir un valor de 0 a 10.00!";

        #endregion

        //constructors
        public Exam(string studentDni, string subject, double mark, DateTime timeStamp)
        {
            this.StudentDni = studentDni;
            this.Subject = subject;
            this.Mark = mark;
            this.TimeStamp = timeStamp;
        }
        public Exam()
        {

        }
    }
}
