﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantConsola.Lib.Models
{
    public class Student : Entity
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public string Dni { get; set; }

        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            return nom.Length != 9;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            //return Student.ContainsKey(nom);
            return false;
        }
        # endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "El Nom de l'Alumne no pot estar buid!"; 
        // DNI Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte";
        // DNI existent
        public const string ErrExisteix = "Aquest Dni ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix el DNI:";
        #endregion

        //llista d'examens
        public List<Exam> Exams { get; set; }

        //constructors 
        public Student()
        {
            Exams = new List<Exam>();
        }

        //constructor per carrega
        public Student(string name, string surname1, string surname2, string dni)
        {
            this.Name = name;
            this.Surname1 = surname1;
            this.Surname2 = surname2;
            this.Dni = dni;
        }

        //métode per afegir examens des d'aqui
        public bool AddExam(Exam exam)
        {
            //exam.Student = this;
            Exams.Add(exam);

            return true;
        }
    }
}
