﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantConsola.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string Teacher { get; set; }

        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }
        // Nom existent
        //public static bool ValidateExist(string nom)
        //{
        //    return Student.ContainsKey(nom);
        //    return false;
        //}
        # endregion

        #region CONSTANTS
        // Nom not empty
        public const string ErrBuidSubject = "El nom de l'asignatura no pot estar buid!";
        public const string ErrBuidTeacher = "El nom del Professor no pot estar buid!";
        
        // Nom NO existent
        public const string ErrNoExisteix = "No existeix cap Asignatura com:";
        // Nom existent
        public const string ErrExisteix = "Ja existeix una Asignatura com:";

        #endregion

        //constructors
        public Subject()
        {

        }
        public Subject(string name, string teacher)
        {
            this.Name = name;
            this.Teacher = teacher;
        }
    }
}


