﻿using EstudiantConsola.Lib.Models;
using System;
using System.Collections.Generic;

namespace EstudiantConsola
{
    /// <summary>
    /// El objetivo de esta iteración es usar clases para definir:
    /// Students (Estudiante)
    /// Subjects (Materias)
    /// Exams (Exámenes)
    /// y añadir opciones extras al menú para poder manejarlas
    /// En el ejemplo crearemos las entidades (clases que definen el dominio -funcionalidad- de nuestro programa)
    /// y la opción de menú para añadir estudiantes
    /// El alumno debe completar el resto de funcionalidades CRUD (Create, Read, Update, Destroy)
    /// </summary>
    class Program
    {
        public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
        public static Dictionary<string, Exam> Exams = new Dictionary<string, Exam>();
        public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();

        //static List<double> Marks { get; set; }
        static string EscapeWord = "ESCAPAAA";

        static void Main(string[] args)
        {
            //Càrrega inicial de Dades d'exemple
            DataTest();

            ShowMainMenu();
            //Marks = new List<double>();
        }

        #region MAIN MENUS

            private static void ShowMainMenu()
        {
            Console.WriteLine("___________________________________________________\n");
            Console.WriteLine("BENVINGUTS AL PROGRAMA DE GESTIÓ D'ALUMNES");
            Console.WriteLine("___________________________________________________\n");
            Console.WriteLine("Per anar a la gestió d'Alumnes, premi a");
            Console.WriteLine("Per obtenir les Estadístiques de notes, premi e");
            Console.WriteLine("Per anar a la gestió d'Asignatures, premi s");
            Console.WriteLine("Per anar a la gestió d'Examens, premi m");
            Console.WriteLine("Per tancar el programa usi l'opció X\n");
            var keepdoing = true;

            while (keepdoing)
            {
                var option = Console.ReadKey().KeyChar;


                if (option == 'X')
                {
                    keepdoing = false;
                    Console.WriteLine("\n___________________________________________________\n");
                    Console.WriteLine("    Gràcies per la visita!!!");
                    Console.WriteLine("    FINS A LA PROPERA!!!");
                    Console.WriteLine("\n\n___________________________________________________\n");
                    Environment.Exit(0);
                }
                else if (option == 'a')
                {
                    ShowStudentsMenu();
                }
                else if (option == 'e')
                {
                    ShowStatsMenu();
                }
                else if (option == 's')
                {
                    ShowSubjectsMenu();
                }
                else if (option == 'm')
                {
                    ShowExamsMenu();
                }
            }
        }

            private static void ShowMainMenu2()
        {
            Console.WriteLine("\n\n_____________________________________\n");
            Console.WriteLine("     Tornada al menú principal");
            Console.WriteLine("_____________________________________\n");
            ShowMainMenu();
        }

        #endregion

        #region STUDENTS

        static void ShowStudentsMenu()
            {
                Console.WriteLine();
                ShowStudentsMenuOptions();

                var keepdoing = true;
                while (keepdoing)
                {
                    var text = Console.ReadLine();

                    switch (text)
                    {
                        case "all":
                            ShowAllStudents();
                            break;
                        case "add":
                            AddNewStudent();
                            break;
                        case "edit":
                            EditStudent();
                            break;
                        case "delete":
                            DeleteStudent();
                            break;
                        case "m":
                            keepdoing = false;
                            break;
                        default:
                            Console.WriteLine("Comand no reconegut, introdueixi una opció vàlida\n" +
                                "o escrigui m per a tornar al menú principal");
                            keepdoing = false;
                            break;
                    }
                }
                ShowMainMenu2();
        }

        private static void ShowStudentsMenuOptions()
        {
            Console.WriteLine("\n___________________________________________________\n");
            Console.WriteLine("--MENÚ D'ALUMNES--");
            Console.WriteLine("___________________________________________________\n");
            Console.WriteLine("Per a veure tots els Alumnes, premi all");
            Console.WriteLine("Per a afegir un nou Alumne, premi add");
            Console.WriteLine("Per a editar un Alumne, premi edit");
            Console.WriteLine("Per a esborrar un Alumne, premi delete");
            Console.WriteLine("Para volver al menú principal, premi m");
        }

        static void ShowAllStudents()
        {
            foreach (var student in Students.Values)
            {
                Console.WriteLine($"{student.Dni} {student.Name} {student.Surname1} {student.Surname2}");
            }
            Console.WriteLine("___________________________________________________\n");
        }

        static void AddNewStudent()
        {
            Console.WriteLine("Primer inserti el DNI de l'Alumne\n" +
                "o escrigui **anular** per sortir");

            var keepdoing = true;
            while (keepdoing)
            {
                var dni = Console.ReadLine();

                if (dni == "anular") // per poder sortir
                {
                    break;
                }
                else if (Student.ValidateEmpty(dni) )
                {
                    Console.WriteLine(Student.ErrBuidDni);
                }
                else if (Student.ValidateLength(dni))
                {
                    Console.WriteLine(Student.ErrNo9);
                }
                else if (Students.ContainsKey(dni))
                {
                    Console.WriteLine(Student.ErrExisteix);
                }
                else
                {
                    while (keepdoing) // fins que no possi bé el Nom!!
                    {
                        Console.WriteLine("Ara inserti el nom del nou ALumne\n" +
                            "o escrigui **anular** per sortir.");
                        var name = Console.ReadLine();

                        if (name == "anular")
                        {
                            keepdoing = false; //per sortir del bucle principal
                            break;
                        }

                        if (Student.ValidateEmpty(name))
{
                            Console.WriteLine(Student.ErrBuidNom);
                        }
                        else
                        {
                            while (keepdoing) // fins que no possi bé el T!!
                            {
                                Console.WriteLine("Ara inserti el primer cognom del nou ALumne\n" +
                                    "o escrigui **anular** per sortir.");
                                var cognom1 = Console.ReadLine();

                                if (cognom1 == "anular")
                                {
                                    keepdoing = false; //per sortir del bucle principal
                                    break;
                                }

                                if (Student.ValidateEmpty(cognom1))
                                {
                                    Console.WriteLine(Student.ErrBuidNom);
                                }
                                else
                                {
                                    while (keepdoing) // fins que no possi bé el T!!
                                    {
                                        Console.WriteLine("Ara inserti el segon cognom del nou ALumne\n" +
                                            "o escrigui **anular** per sortir.");
                                        var cognom2 = Console.ReadLine();
                                        if (cognom2 == "anular")
                                        {
                                            keepdoing = false; //per sortir del bucle principal
                                            break;
                                        }

                                        if (Student.ValidateEmpty(cognom2))
                                        {
                                            Console.WriteLine(Student.ErrBuidNom);
                                        }
                                        else
                                        {
                                            try
                                            {
                                                var student = new Student
                                                {
                                                    Id = Guid.NewGuid(),
                                                    Dni = dni,
                                                    Name = name,
                                                    Surname1 = cognom1,
                                                    Surname2 = cognom2
                                                };

                                                Students.Add(student.Dni, student);
                                                keepdoing = false; // per sortir de l'altre while!
                                            }
                                            catch (Exception ex)
                                            {
                                                string error = ex.Message;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ShowStudentsMenuOptions();
        }

        static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }

        static void DeleteStudent()
        {
            Console.WriteLine("Recerca per DNI\n" +
                "o escrigui **anular** per sortir");

            var keepdoing = true;
            while (keepdoing)
            {
                var dni = Console.ReadLine(); ;

                if (dni == "anular") // per poder sortir
                {
                    break;
                }
                else if (Student.ValidateEmpty(dni))
                {
                    Console.WriteLine(Student.ErrBuidDni);
                }
                else if (Student.ValidateLength(dni))
                {
                    Console.WriteLine(Student.ErrNo9);
                }
                else if (!Students.ContainsKey(dni))
                {
                    Console.WriteLine(Student.ErrNoExisteix + " " + dni);
                }
                else
                {
                    // Coincidencia => Confirma delete + delete
                    //Confirma delete
                    Console.WriteLine($"Està segur que vol esborrar aquest Alumne {dni} ?\n" +
                        $"Per esborrar (aquesta acció no es pot desfer!), premi 1\n" +
                        $"Cancel.lar, premi 2");
                    while (keepdoing)
                    {
                        var option = Console.ReadKey().KeyChar;
                        if (option == '1')
                        {
                            try
                            {
                                var result = Students.Remove(dni);
                                Console.WriteLine("\n___________________________________________________");
                                Console.WriteLine($"L'Alumne amb el Dni {dni} ha estat eliminat: {result}");
                                Console.WriteLine("___________________________________________________\n");
                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                            }
                        }
                        keepdoing = false; // per sortir de l'altre while!
                        break;
                    }
                }
            }

            ShowStudentsMenuOptions();
        }

        static void EditStudent()
            {
                Console.WriteLine("Per a editar un estudiant,primer escrigui el seu DNI\n" +
                "o escrigui **anular** per sortir");

                var keepdoing = true;
                while (keepdoing)
                {
                    var dni = Console.ReadLine();

                    if (dni == "anular") // per poder sortir
                    {
                        break;
                    }
                    else if (Student.ValidateEmpty(dni))
                    {
                        Console.WriteLine(Student.ErrBuidDni);
                    }
                    else if (Student.ValidateLength(dni))
                    {
                        Console.WriteLine(Student.ErrNo9);
                    }
                    else if (!Students.ContainsKey(dni))
                    {
                        Console.WriteLine(Student.ErrNoExisteix + " " + dni);
                    }
                    else
                    {
                        // Coincidencia => Carrega DNI + Name + edit
                        var dades = Students[dni];
                        Console.WriteLine($"Aquest és l'Alumne: {dades.Name} {dades.Surname1} {dades.Surname2}\n");
                        Console.WriteLine($"Quin camp vol editar?\n" +
                            $"Nom, premi 1\n" +
                            "Cognom 1, premi 2\n" +
                            "Cognom 2, premi 3\n" +
                            "m -per tornar al menú principal");

                        while (keepdoing)
                        {
                            var option = Console.ReadKey().KeyChar;
                            Console.WriteLine("\n");

                            try
                            {
                                if (option == '1')
                                {
                                    Console.WriteLine($"Va a editar el nom. Introdueixi el nom correcte:");
                                    var newValue = Console.ReadLine();
                                    dades.Name = newValue;
                                    keepdoing = false; // per sortir de l'altre while!
                                    Console.WriteLine("Update done!\n");
                                    break;
                                }
                                else if (option == '2')
                                {
                                    Console.WriteLine($"Va a editar el primer cognom. Introdueixi el cognom correcte:");
                                    var newValue = Console.ReadLine();
                                    dades.Surname1 = newValue;
                                    keepdoing = false; // per sortir de l'altre while!
                                    Console.WriteLine("Update done!\n");
                                    break;
                                }
                                else if (option == '3')
                                {
                                    Console.WriteLine($"Va a editar el segon cognom. Introdueixi el cognom correcte:");
                                    var newValue = Console.ReadLine();
                                    dades.Surname2 = newValue;
                                    keepdoing = false; // per sortir de l'altre while!
                                    Console.WriteLine("Update done!\n");
                                    break;
                                }
                                else if (option == 'm')
                                {
                                    keepdoing = false;
                                }
                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                            }
                        }
                    }
                }
            ShowStudentsMenuOptions();
            }

        #endregion

        #region SUBJECTS

        static void ShowSubjectsMenu()
        {
            Console.WriteLine();
            ShowSubjectsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllSubjects();
                        break;
                    case "add":
                        AddNewSubject();
                        break;
                    case "edit":
                        EditSubjects();
                        break;
                    case "delete":
                        DeleteSubjects();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("Comand desconegut, introdueixi una opció vàlida\n" +
                        "o escrigui **m** per tornar al menú principal");
                        keepdoing = false;
                        break;
                }
            }
            ShowMainMenu2();
        }

        private static void ShowSubjectsMenuOptions()
        {
            Console.WriteLine("\n___________________________________________________\n");
            Console.WriteLine("--MENÚ D'ASIGNATURES--\n");
            Console.WriteLine("___________________________________________________\n");

            Console.WriteLine("Per a veure totes les asignatures, escrigui all");
            Console.WriteLine("Per a afegir una nova Asignatura, escrigui add");
            Console.WriteLine("Per a editar una Asignatura, escrigui edit");
            Console.WriteLine("Per a esborrar una asignatura, escrigui delete");
            Console.WriteLine("Per a tornar al menú principal, escrigui m");
        }

        static void ShowAllSubjects()
        {
            foreach (var subject in Subjects.Values)
            {
                Console.WriteLine($"{subject.Name} {subject.Teacher}");
            }
            Console.WriteLine("___________________________________________________\n");
        }

        static void AddNewSubject()
        {
            Console.WriteLine("Primer inserti el nom de l'Asignatura\n" +
                "o escrigui **anular** per sortir");
            var keepdoing = true;
            while (keepdoing)
            {
                var name = Console.ReadLine();
                if (name == "anular") // per poder sortir
                {
                    break;
                }
                else if (Subject.ValidateEmpty(name))
                {
                    Console.WriteLine(Subject.ErrBuidSubject);
                }
                else if (Subjects.ContainsKey(name))
                {
                    Console.WriteLine(Subject.ErrExisteix);
                }
                else
                {
                    while (true) // fins que no possi bé el T!!
                    {
                        Console.WriteLine("Ara escrigui el nom del Professor\n" +
                            "o escrigui **anular** per sortir");
                        var teacher = Console.ReadLine();

                        if (teacher == "anular")
                        {
                            break;
                        }

                        if (Subject.ValidateEmpty(teacher))
                        {
                            Console.WriteLine(Subject.ErrBuidTeacher);
                        }
                        else
                        {
                            try
                            {
                                var subject = new Subject
                                {
                                    Id = Guid.NewGuid(),
                                    Name = name,
                                    Teacher = teacher
                                };
                                //var temp = new Subject(subject.Name, subject.Teacher);
                                Subjects.Add(subject.Name, subject);
                                keepdoing = false; // per sortir de l'altre while!
                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                            }
                            break;
                        }

                    }
                }

                ShowSubjectsMenuOptions();
            }
        }

        static void DeleteSubjects()
        {
            Console.WriteLine("Recerca per Nom de l'Asignatura\n" +
                "o escrigui **anular** per sortir");

            var keepdoing = true;
            while (keepdoing)
            {
                var subject = Console.ReadLine();
                if (subject == "anular") // per poder sortir
                {
                    break;
                }
                else if (Subject.ValidateEmpty(subject))
                {
                    Console.WriteLine(Subject.ErrBuidSubject);
                }
                else if (!Subjects.ContainsKey(subject))
                {
                    Console.WriteLine(Subject.ErrNoExisteix);
                }
                else
                {
                    // Coincidencia => Confirma delete + delete
                    //Confirma delete
                    Console.WriteLine($"Està segur que vol esborrar aquesta Asigantura: {subject} \n" +
                       $"Per esborrar (aquesta acció no es pot desfer!), premi 1\n" +
                       $"Cancel.lar, premi 2");
                    while (keepdoing)
                    {
                        var option = Console.ReadKey().KeyChar;
                        try
                        {
                            if (option == '1')
                            {
                                var result = Subjects.Remove(subject);
                                Console.WriteLine("\n___________________________________________________");
                                Console.WriteLine($"L'Asignatura ** {subject} ** ha estat eliminada: {result}");
                                Console.WriteLine("___________________________________________________\n");
                            }
                        }
                        catch (Exception ex)
                        {
                            string error = ex.Message;
                        }
                        keepdoing = false; // per sortir de l'altre while!
                        break;
                    }
                }
            }
            ShowSubjectsMenuOptions();
        }

        static void EditSubjects()
        {
            Console.WriteLine("Para editar una Asignatura, primer escrigui el seu nom\n" +
                "o escrigui **anular** per sortir");

            var keepdoing = true;
            while (keepdoing)
            {
                var name = Console.ReadLine();
                if (name == "anular") // per poder sortir
                {
                    break;
                }
                else if (Subject.ValidateEmpty(name))
                {
                    Console.WriteLine(Subject.ErrBuidSubject);
                }
                else if (!Subjects.ContainsKey(name))
                {
                    Console.WriteLine(Subject.ErrNoExisteix);
                }
                else
                {
                    // Coincidencia Key
                    var dades = Subjects[name];
                    Console.WriteLine($"L'Asignatura és: {dades.Name} {dades.Teacher}\n");

                    Console.WriteLine($"Quin camp vol editar?\n" +
                       $"Nom de l'Asignatura, premi 1\n" +
                       "Nom del Professor, premi 2\n" +
                       "per tornar al menú principal, premi m");

                    while (keepdoing)
                    {
                        var option = Console.ReadKey().KeyChar;
                        try
                        {
                            if (option == '1')
                            {
                                Console.WriteLine($"\nVa a editar el nom de l'Asignatura. Introdueixi el nou Nom:\n");
                                var newAsignatura = Console.ReadLine();
                                dades.Name = newAsignatura;
                                keepdoing = false; // per sortir de l'altre while!
                                Console.WriteLine("Update done!\n");
                                break;
                            }
                            else if (option == '2')
                            {
                                Console.WriteLine($"\nVa ha editar el nom del Professor. Introdueixi el nou Nom:\n");
                                var newTeacher = Console.ReadLine();
                                dades.Teacher = newTeacher;
                                keepdoing = false; // per sortir de l'altre while!
                                Console.WriteLine("Update done!\n");
                                break;
                            }
                            else if (option == 'm')
                            {
                                keepdoing = false;
                                ShowSubjectsMenuOptions();
                            }
                        }
                        catch (Exception ex)
                        {
                            string error = ex.Message;
                        }
                    }
                }
            }
            ShowSubjectsMenuOptions();
        }
        #endregion

        #region EXAMS

        static void ShowExamsMenu()
        {
            Console.WriteLine();
            ShowExamsMenuOptions();

            var keepdoing = true;
            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllExams();
                        break;
                    case "add":
                        AddNewExam();
                        break;
                    case "edit":
                        EditExams();
                        break;
                    case "delete":
                        DeleteExams();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("Comand desconegut, introdueixi una opció vàlida\n" +
                            "o escrigui m para tornar al menú principal");
                        keepdoing = false;
                        break;
                }
            }
            ShowMainMenu2();
        }

        private static void ShowExamsMenuOptions()
        {
            Console.WriteLine("\n___________________________________________________\n");
            Console.WriteLine("--MENÚ D'EXÀMENS--");
            Console.WriteLine("___________________________________________________\n");

            Console.WriteLine("Per a veure tots els exàmens escrigui all");
            Console.WriteLine("Per a afegir una nova Nota escrigui add");
            Console.WriteLine("Per a editar una Nota d'un exàmen escrigui edit");
            Console.WriteLine("Per a esborrar una nota d'un exàmen escrigui delete");
            Console.WriteLine("Per a tornar al menú principal escrigui m");
        }

        static void ShowAllExams()
        {
            foreach (var exam in Exams.Values)
            {
                Console.WriteLine($"Key: {exam.StudentDni+exam.Subject}, dades: {exam.Subject} {exam.TimeStamp} {exam.StudentDni} Nota: {exam.Mark}");
            }
            Console.WriteLine("___________________________________________________\n");
        }

        static void AddNewExam()
        {
            Console.WriteLine("Primer inserti el nom de l'Asignatura de l'exàmen\n" +
                "o escrigui **anular** per sortir\n" +
                "(per exemple: SQL, Python, Ciberseguretat, Blockchain...)");

            var keepdoing = true;
            while (keepdoing)
            {
                var subject = Console.ReadLine();

                if (subject == "anular") // per poder sortir
                {
                    break;
                }
                else if (Exam.ValidateEmpty(subject))
                {
                    Console.WriteLine(Exam.ErrBuidSubject);
                }
                else
                {
                    while (keepdoing) // fins que no possi bé el T!!
                    {
                        Console.WriteLine("Ara escrigui el DNI de l'Alumne\n" +
                            "o **anular** per sortir");
                        var dni = Console.ReadLine();
                        if (dni == "anular") // per poder sortir
                        {
                            break;
                        }
                        else if (Exam.ValidateEmpty(dni))
                        {
                            Console.WriteLine(Exam.ErrBuidDni);
                        }
                        else if (Exam.ValidateLength(dni))
                        {
                            Console.WriteLine(Exam.ErrNo9);
                        }
                        // existeix la key: DNI+Subject?
                        else if (Exams.ContainsKey(dni+subject))
                        {
                            Console.WriteLine(Exam.ErrSubjectExist + " " + dni+subject);
                        }
                        else
                        {
                            while (keepdoing) // fins que no possi bé el T!!
                            {
                                Console.WriteLine("Ara escrigui la Nota de l'Alumne\n" +
                                    "o escrigui **anular** per sortir");
                                var stringMark = Console.ReadLine();
                                double mark = Convert.ToDouble(stringMark);

                                if (stringMark == "anular")
                                {
                                    break;
                                }
                                else if (Exam.ValidateEmpty(stringMark))
                                {
                                    Console.WriteLine(Exam.ErrBuidMark);
                                }
                                // valida rango 0- 10 + double
                                else if (Exam.ValidateRang(mark))
                                {
                                    Console.WriteLine(Exam.ErrMarkRang);
                                }
                                //Ojo!  Doubles
                                else
                                {
                                    try
                                    {
                                        var exam = new Exam
                                        {
                                            Id = Guid.NewGuid(),
                                            Subject = subject,
                                            StudentDni = dni,
                                            Mark = mark,
                                            TimeStamp = DateTime.Now,
                                        };
                                        var temp = dni + subject;
                                        Exams.Add(temp, exam);
                                        keepdoing = false; // per sortir de l'altre while!
                                    }
                                    catch (Exception ex)
                                    {
                                        string error = ex.Message;
                                    }
                                    break;
                                }
                            }
                        }

                    }
                }

                ShowExamsMenuOptions();
            }
        }

        static void DeleteExams()
        {
            Console.WriteLine("Recerca per Nom de l'Asignatura\n" +
                "o escrigui **anular** per sortir\n" +
                "(per exemple: SQL, Python...)");

            var keepdoing = true;
            while (keepdoing)
            {
                var subject = Console.ReadLine(); ;

                if (subject == "anular") // per poder sortir
                {
                    break;
                }
                else if (Exam.ValidateEmpty(subject))
                {
                    Console.WriteLine(Exam.ErrBuidSubject);
                }
                else 
                {
                    while (keepdoing) // fins que no possi bé el T!!
                    {
                        Console.WriteLine("Ara escrigui el DNI de l'Alumne\n" +
                            "o escrigui **anular** per sortir");

                        var dni = Console.ReadLine();
                        if (dni == "anular") // per poder sortir
                        {
                            break;
                        }
                        else if (Exam.ValidateEmpty(dni))
                        {
                            Console.WriteLine(Exam.ErrBuidDni);
                        }
                        else if (Exam.ValidateLength(dni))
                        {
                            Console.WriteLine(Exam.ErrNo9);
                        }
                        // Ara valida existencia
                        else if (!Exams.ContainsKey(dni + subject))
                        {
                            Console.WriteLine(Exam.ErrSubjectDniNoExist);
                        }
                        else
                        {
                            //Confirma delete
                            Console.WriteLine($"Està segur que vol esborrar aquest examen de {subject} de l'Alumne {dni}?\n" +
                                $"Per esborrar (aquesta acció no es pot desfer!), premi 1\n" +
                                $"Cancel.lar, premi 2");
                            while (keepdoing)
                            {
                                var option = Console.ReadKey().KeyChar;
                                try
                                {
                                    if (option == '1')
                                    {
                                        var result = Exams.Remove(dni + subject);
                                        Console.WriteLine("\n___________________________________________________");
                                        Console.WriteLine($"La nota de l'ALumne {dni} per l'Asignatura {subject} ha estat eliminada: {result}");
                                        Console.WriteLine("___________________________________________________\n");
                                    }
                                    keepdoing = false; // per sortir de l'altre while!
                                }
                                catch (Exception ex)
                                {
                                    string error = ex.Message;
                                }
                                break;
                            }
                        }

                    }
                }
            }
            ShowExamsMenuOptions();
        }

        static void EditExams()
        {
            Console.WriteLine("Para editar una Nota ,primer escrigui el nom de l'Asignatura\n" +
                "o escrigui ***anular*** per sortir");

            var keepdoing = true;
            while (keepdoing)
            {
                var subject = Console.ReadLine();

                if (subject == "anular") // per poder sortir
                {
                    break;
                }
                else if (Exam.ValidateEmpty(subject))
                {
                    Console.WriteLine(Exam.ErrBuidSubject);
                }
                else
                {
                    while (keepdoing) // fins que no possi bé el T!!
                    {
                        Console.WriteLine("Ara escrigui el DNI de l'Alumne o **anular** per sortir");

                        var dni = Console.ReadLine();
                        if (dni == "anular") // per poder sortir
                        {
                            break;
                        }
                        else if (Exam.ValidateEmpty(dni))
                        {
                            Console.WriteLine(Exam.ErrBuidDni);
                        }
                        else if (Exam.ValidateLength(dni))
                        {
                            Console.WriteLine(Exam.ErrNo9);
                        }
                        // Ara valida existencia = coincidencia de KEY
                        else if (!Exams.ContainsKey(dni + subject))
                        {
                            Console.WriteLine(Exam.ErrSubjectDniNoExist);
                        }
                        else
                        {
                            try
                            {
                                var dades = Exams[dni + subject];
                                var oldMark = dades.Mark;
                                Console.WriteLine($"L'Exàmen de l'ALumne {dni} per l'Asignatura {subject} te una nota: {oldMark}");

                                Console.WriteLine($"\nAra introdueixi la nova Nota:");
                                var stringMark = Console.ReadLine();
                                double newmark = Convert.ToDouble(stringMark);
                                dades.Mark = newmark;
                                keepdoing = false; // per sortir de l'altre while!
                                Console.WriteLine("Update done!\n");
                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                            }
                            break;
                        }

                    }
                }
            }
            ShowExamsMenuOptions();
        }
        #endregion

        #region ESTADISTIQUES
        static void ShowStatsMenu()
        {
            Console.WriteLine();
            ShowStatsMenuOptions();
            var keepdoing = true;

            while (keepdoing)
            {
                var text = Console.ReadLine();

                switch (text)
                {
                    case "all":
                        ShowAllExamens();
                        break;
                    case "avg":
                        ShowAverage();
                        break;
                    case "max":
                        ShowMaximum();
                        break;
                    case "min":
                        ShowMinimum();
                        break;
                    case "m":
                        keepdoing = false;
                        break;
                    default:
                        Console.WriteLine("Comand no recorregut, introdueixi una opció vàlida\n" +
                            "o escrigui **anular** per sortir");
                        break;
                }
            }
            ShowMainMenu2();
        }

        private static void ShowStatsMenuOptions()
        {
            Console.WriteLine("\n___________________________________________________\n");
            Console.WriteLine("--MENÚ D'ESTADÍSTIQUES--");
            Console.WriteLine("___________________________________________________\n");
            Console.WriteLine("Per veure totes les notes, premi all");
            Console.WriteLine("Per a veure la nota mitja, premi avg");
            Console.WriteLine("Per a veure la nota más alta, premi max");
            Console.WriteLine("Per a veure la nota más baixa, premi min");
            Console.WriteLine("Per tornar al menú principal, premi m");

        }

        static void ShowAllExamens()
        {
            foreach (var x in Exams.Values)
            {
                Console.WriteLine($"{x.Subject} {x.StudentDni} {x.TimeStamp} {x.Mark}");
            }
            Console.WriteLine("___________________________________________________\n");
        }

        static void ShowAverage()
        {
            var suma = 0.0;
            try
            {
                foreach (var x in Exams.Values)
                {
                    suma += x.Mark;
                }

                var average = suma / Exams.Values.Count;
                Console.WriteLine("La nota mitja dels exàmens és: {0}", average);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        static void ShowMaximum()
        {
            var max = 0.0;
            try
            {
                foreach (var x in Exams.Values)
                {
                    if (x.Mark > max)
                        max = x.Mark;
                    //Ternario
                    // max = x.Mark > max ? x.Mark : max;
                }

                //Amb FOR: convertim a array
                //var a = Exams.Values.ToArray();
                //for (var i=0; i<a.Length; i++)
                //{
                //    var x = a[i];
                //    entra en values > mark
                //    var m = x.Mark
                //}

                Console.WriteLine("La nota més alta és: {0}", max);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        static void ShowMinimum()
        {
            var min = 10.0;
            try
            {
                foreach (var x in Exams.Values)
                {
                    if (x.Mark <= min)
                        min = x.Mark;
                }
                Console.WriteLine("La nota més baixa és: {0}", min);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        #endregion
   

        static void DataTest()
    {
        //public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
        var student1 = new Student("Pep", "Mira", "López", "38412532D");
        Students.Add(student1.Dni, student1);
        var student2 = new Student("Maria", "Mira", "López", "58412532D");
        Students.Add(student2.Dni, student2);
        var student3 = new Student("Carles", "Camps", "Rodriguez", "12345678A");
        Students.Add(student3.Dni, student3);
        var student4 = new Student("Gemma", "Jimenez", "Sanchez", "23456789A");
        Students.Add(student4.Dni, student4);
        var student5 = new Student("Victor", "Martínez", "López", "18412532D");
        Students.Add(student5.Dni, student5);
        var student6 = new Student("Anna", "Vila", "Jimenez", "28412532D");
        Students.Add(student6.Dni, student6);
        var student7 = new Student("Cèlia", "Mira", "Martinez", "48412532D");
        Students.Add(student7.Dni, student7);
        var student8 = new Student("Carla", "Saladie", "López", "68412532D");
        Students.Add(student8.Dni, student8);
        var student9 = new Student("Joan", "Vermell", "López", "78412532D");
        Students.Add(student9.Dni, student9);
        var student10 = new Student("Raquel", "Mirabet", "Sladie", "88412532D");
        Students.Add(student10.Dni, student10);

            
        //public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
        var subject1 = new Subject("C Sharp", "José Freire");
        Subjects.Add("C Sharp", subject1);
        var subject2 = new Subject("Ciberseguretat", "Ana Segura");
        Subjects.Add("Ciberseguretat", subject2);
        var subject3 = new Subject("Blockchain", "Carles Santamaria");
        Subjects.Add("Blockchain", subject3);
        var subject4 = new Subject("Flutter & Dart", "Pep Marquès");
        Subjects.Add("Flutter & Dart", subject4);
        var subject5 = new Subject("SQL", "Ângel López");
        Subjects.Add("SQL", subject5);
        var subject6 = new Subject("Python", "Carme Pla");
        Subjects.Add("Python", subject6);

        //public static Dictionary<string, Exam> Exams = new Dictionary<string, Exam>();
        //public Exam(Student studentDni, Subject subject, double mark, DateTime timeStamp)
        DateTime timestamp = DateTime.Now;
        var exam1 = new Exam("12345678A", "Python", 6.2, timestamp);
        Exams.Add("12345678A" + "Python", exam1);
        var exam2 = new Exam("28412532D", "Python", 6.9, timestamp);
        Exams.Add("28412532D" + "Python", exam2);
        var exam3 = new Exam("68412532D", "Python", 8.4, timestamp);
        Exams.Add("68412532D" + "Python", exam3);
        var exam4 = new Exam("48412532D", "Python", 2.1, timestamp);
        Exams.Add("48412532D" + "Python", exam4);
        var exam5 = new Exam("38412532D", "Python", 7.6, timestamp);
        Exams.Add("38412532D" + "Python", exam5);
        var exam6 = new Exam("58412532D", "Python", 4.2, timestamp);
        Exams.Add("58412532D" + "Python", exam6);
        var exam7 = new Exam("12345678A", "SQL", 6.2, timestamp);
        Exams.Add("12345678A" + "SQL", exam7);
        var exam8 = new Exam("28412532D", "SQL", 6.9, timestamp);
        Exams.Add("28412532D" + "SQL", exam8);
        var exam9 = new Exam("68412532D", "SQL", 8.4, timestamp);
        Exams.Add("68412532D" + "SQL", exam9);
        var exam10 = new Exam("48412532D", "SQL", 3.9, timestamp);
        Exams.Add("48412532D" + "SQL", exam10);
        var exam11 = new Exam("38412532D", "SQL", 7.6, timestamp);
        Exams.Add("38412532D" + "SQL", exam11);
        var exam12 = new Exam("58412532D", "SQL", 4.2, timestamp);
        Exams.Add("58412532D" + "SQL", exam12);
    }

    }
}

