﻿using EstudiantWPF.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantWPF.Lib.DAL
{
    public class SubjectsRepository
    {
        //public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
        public static Dictionary<string, Subject> Subjects
        {
            get
            {
                if (_subjects == null)
                {
                    _subjects = new Dictionary<string, Subject>();
                    DataSubject();
                }
                return _subjects;
            }
        }
        static Dictionary<string, Subject> _subjects;

        public List<Subject> SubjectsList
        {
            get
            {
                return GetAll();
            }
        }
        public static List<Subject> GetAll()
        {
            return Subjects.Values.ToList();
        }


        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }
        // Nom existent
        //public static bool ValidateExist(string nom)
        //{
        //    return Student.ContainsKey(nom);
        //    return false;
        //}
        #endregion

        #region CONSTANTS
        // Nom not empty
        public const string ErrBuidSubject = "El nom de l'asignatura no pot estar buid!";
        public const string ErrBuidTeacher = "El nom del Professor no pot estar buid!";

        // Nom NO existent
        public const string ErrNoExisteix = "No existeix cap Asignatura amb aquest nom:";
        // Nom existent
        public const string ErrExisteix = "Ja existeix una Asignatura amb aquest nom!";

        #endregion

        #region DATA
        static void DataSubject()
        {
            //public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
            var subject1 = new Subject("C Sharp", "José Freire");
            _subjects.Add("C Sharp", subject1);
            var subject2 = new Subject("Ciberseguretat", "Ana Segura");
            _subjects.Add("Ciberseguretat", subject2);
            var subject3 = new Subject("Blockchain", "Carles Santamaria");
            _subjects.Add("Blockchain", subject3);
            var subject4 = new Subject("Flutter & Dart", "Pep Marquès");
            _subjects.Add("Flutter & Dart", subject4);
            var subject5 = new Subject("SQL", "Ângel López");
            _subjects.Add("SQL", subject5);
            var subject6 = new Subject("Python", "Carme Pla");
            _subjects.Add("Python", subject6);
        }
        #endregion


    }
}
