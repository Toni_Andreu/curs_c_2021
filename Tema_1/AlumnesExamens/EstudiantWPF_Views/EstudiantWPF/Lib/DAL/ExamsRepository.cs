﻿using EstudiantWPF.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantWPF.Lib.DAL
{
    public class ExamsRepository
    {
        //public static Dictionary<string, Exam> Exams = new Dictionary<string, Exam>();
        //public Exam SelectedExam;
        public static Dictionary<string, Exam> Exams
        {
            get
            {
                if (_exams == null)
                {
                    _exams = new Dictionary<string, Exam>();
                    DataExam();
                }
                return _exams;
            }
        }
        static Dictionary<string, Exam> _exams;

        public List<Exam> ExamsList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Exam> GetAll()
        {
            return Exams.Values.ToList();
        }


        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            return nom.Length != 9;
        }
        // Nom existent
        //public static bool ValidateExist(string nom)
        //{
        //    //return Student.ContainsKey(nom);
        //    return false;
        //}
        // Nota en rang 0-10.0
        public static bool ValidateRang(double mark)
        {
            return (0.0 <= mark) && (mark <= 10.0);
        }
        # endregion

        #region CONSTANTS

        //Subject not empty
        public const string ErrBuidSubject = "El nom de l'Asignatura no pot estar buid!";
        // no existeix
        public const string ErrSubjectDniNoExist = "No existeix cap Exàmen de l'Asignatura per aquest alumne!";
        // JA existeix
        public const string ErrSubjectExist = "Ja existeix un exàmen per aquest Alumne i Asignatura:";
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // Nom Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte";
        // Nom existent
        //public const string ErrNoExisteix = "El dni no pot estar buid!";
        // Mark not empty
        public const string ErrBuidMark = "La Nota no pot estar buida!";
        // Mark 0 - 10.0
        public const string ErrMarkRang = "La Nota ha de tenir un valor de 0 a 10.00!";

        #endregion


        #region DATA
        static void DataExam() 
        {
            DateTime timestamp = DateTime.Now;
            var exam1 = new Exam("12345678A", "Python", 6.2, timestamp);
            _exams.Add("12345678A" + "Python", exam1);
            var exam2 = new Exam("28412532D", "Python", 6.9, timestamp);
            _exams.Add("28412532D" + "Python", exam2);
            var exam3 = new Exam("68412532D", "Python", 8.4, timestamp);
            _exams.Add("68412532D" + "Python", exam3);
            var exam4 = new Exam("48412532D", "Python", 2.1, timestamp);
            _exams.Add("48412532D" + "Python", exam4);
            var exam5 = new Exam("38412532D", "Python", 7.6, timestamp);
            _exams.Add("38412532D" + "Python", exam5);
            var exam6 = new Exam("58412532D", "Python", 4.2, timestamp);
            _exams.Add("58412532D" + "Python", exam6);
            var exam7 = new Exam("12345678A", "SQL", 6.2, timestamp);
            _exams.Add("12345678A" + "SQL", exam7);
            var exam8 = new Exam("28412532D", "SQL", 6.9, timestamp);
            _exams.Add("28412532D" + "SQL", exam8);
            var exam9 = new Exam("68412532D", "SQL", 8.4, timestamp);
            _exams.Add("68412532D" + "SQL", exam9);
            var exam10 = new Exam("48412532D", "SQL", 3.9, timestamp);
            _exams.Add("48412532D" + "SQL", exam10);
            var exam11 = new Exam("38412532D", "SQL", 7.6, timestamp);
            _exams.Add("38412532D" + "SQL", exam11);
            var exam12 = new Exam("58412532D", "SQL", 4.2, timestamp);
            _exams.Add("58412532D" + "SQL", exam12);
        }
        #endregion

    }
}
