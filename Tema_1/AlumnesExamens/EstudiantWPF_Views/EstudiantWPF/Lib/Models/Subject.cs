﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantWPF.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string Teacher { get; set; }

        //constructors
        public Subject()
        {

        }
        public Subject(string name, string teacher)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Teacher = teacher;
        }
    }
}