﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EstudiantWPF.Lib.Models
{
    public class Exam : Entity
    {
        public string StudentDni { get; set; }
        public string Subject { get; set; }
        public double Mark { get; set; }
        public DateTime TimeStamp { get; set; }

        //constructors
        public Exam(string studentDni, string subject, double mark, DateTime timeStamp)
        {
            this.Id = Guid.NewGuid();
            this.StudentDni = studentDni;
            this.Subject = subject;
            this.Mark = mark;
            this.TimeStamp = timeStamp;
        }
        public Exam()
        {

        }
    }
}
