﻿using EstudiantWPF.Lib.DAL;
using EstudiantWPF.Lib.Models;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace EstudiantWPF.Views
{
    /// <summary>
    /// Interaction logic for StudentsView.xaml
    /// </summary>
    public partial class StudentsView : UserControl
    {
        public Student SelectedStudent
        {
            get
            {
                return _selectedStudent;
            }
            set
            {
                _selectedStudent = value;
                if (value == null)
                {
                    InputDni.Text = string.Empty;
                    InputName.Text = string.Empty;
                    InputSurname1.Text = string.Empty;
                    InputSurname2.Text = string.Empty;
                    InputEmail.Text = string.Empty;

                }
                else
                {
                    InputDni.Text = value.Dni;
                    InputName.Text = value.Name;
                    InputSurname1.Text = value.Surname1;
                    InputSurname2.Text = value.Surname2;
                    InputEmail.Text = value.Email;
                }
            }
        }
        Student _selectedStudent;

        public StudentsView()
        {
            InitializeComponent();

            DgStudents.ItemsSource = StudentsRepository.GetAll();
        }



        public IEnumerable StudentsList { get; }

        #region STUDENTS

        public void AddNewStudent()
        {
            string dni = InputDni.Text;
            string name = InputName.Text;
            string cognom1 = InputSurname1.Text;
            string cognom2 = InputSurname2.Text;
            string email = InputEmail.Text;
            string error = string.Empty;

            //Validacions
            if (StudentsRepository.ValidateEmpty(dni))
            {
                MessageBox.Show(StudentsRepository.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (StudentsRepository.ValidateLength(dni))
            {
                MessageBox.Show(StudentsRepository.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (StudentsRepository.Students.ContainsKey(dni))
            {
                MessageBox.Show(StudentsRepository.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (StudentsRepository.ValidateEmpty(name))
            {
                MessageBox.Show(StudentsRepository.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (StudentsRepository.ValidateEmpty(cognom1))
            {
                MessageBox.Show(StudentsRepository.ErrBuidSurname1, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            // slguns poden NO tenir 2n cognom!!
            //else if (Student.ValidateEmpty(cognom2))
            //{
            //}
            else
            {
                try
                {
                    var student = new Student
                    {
                        Id = Guid.NewGuid(),
                        Dni = dni,
                        Name = name,
                        Surname1 = cognom1,
                        Surname2 = cognom2,
                        Email = email
                    };

                    StudentsRepository.Students.Add(student.Dni, student);
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        public static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }

        public void DeleteStudent(string key)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Alumne?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = StudentsRepository.Students.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        public static void EditStudent(string name, string surname1, string surname2, string dni, string newdni, string email)
        {
            string newdni1 = dni;
            string newname = name;
            string newcognom1 = surname1;
            string newcognom2 = surname2;
            string newemail = email;
            string error = string.Empty;

            //Validacions
            if (StudentsRepository.ValidateEmpty(newdni1))
            {
                MessageBox.Show(StudentsRepository.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (StudentsRepository.ValidateLength(newdni1))
            {
                MessageBox.Show(StudentsRepository.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            //else if (Students.ContainsKey(newdni1))
            //{
            //    MessageBox.Show(Student.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (StudentsRepository.ValidateEmpty(newname))
            {
                MessageBox.Show(StudentsRepository.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (StudentsRepository.ValidateEmpty(newcognom1))
            {
                MessageBox.Show(StudentsRepository.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            //else if (Student.ValidateEmpty(newcognom2))
            //{
            //}
            else
            {
                try
                {
                    var dades = StudentsRepository.Students[dni];
                    dades.Dni = newdni1; // decidim no canviar-ho = key
                    dades.Name = newname;
                    dades.Surname1 = newcognom1;
                    dades.Surname2 = newcognom2;
                    dades.Email = newemail;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        #endregion

        #region Btns Selects

        private void BtSelectStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentInicial();
            BtEdit.Visibility = Visibility.Visible;
            SelectedStudent = ((Button)sender).DataContext as Student;
            DadesStudent.Text = SelectedStudent.Dni + "- " + SelectedStudent.Name + " " + SelectedStudent.Surname1 + " " + SelectedStudent.Surname2;
        }

        #endregion

        #region Btns CLICK

        private void BtAddStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesStudent.Text = string.Empty;
            VisibilityStudentNew();
        }
        private void BtAddStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentNewSave();
            AddNewStudent();

            ClearDg();
            ClearInputs();
            DgStudents.ItemsSource = StudentsRepository.GetAll();
        }
        private void BtDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = ((Button)sender).DataContext as Student;
            DeleteStudent(selectedStudent.Dni);
            ClearInputs();
            ClearDg();
            BtEdit.Visibility = Visibility.Hidden;
            DgStudents.ItemsSource = StudentsRepository.GetAll();
        }
        private void BtEditStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityStudentEdit();
            InputDni.Text = SelectedStudent.Dni;
            InputName.Text = SelectedStudent.Name;
            InputSurname1.Text = SelectedStudent.Surname1;
            InputSurname2.Text = SelectedStudent.Surname2;
        }
        private void BtEditStudent_Click(object sender, RoutedEventArgs e)
        {
            string dni = SelectedStudent.Dni;
            string newdni = InputDni.Text;
            string name = InputName.Text;
            string surname1 = InputSurname1.Text;
            string surname2 = InputSurname2.Text;
            string email = InputEmail.Text;

            EditStudent(name, surname1, surname2, dni, newdni, email);
            VisibilityStudentEditSave();
            ClearDg();
            ClearInputs();

            DgStudents.ItemsSource = StudentsRepository.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilityStudentInputsON()
        {
            LbDni.Visibility = Visibility.Visible;
            InputDni.Visibility = Visibility.Visible;
            LbName.Visibility = Visibility.Visible;
            InputName.Visibility = Visibility.Visible;
            LbCognom1.Visibility = Visibility.Visible;
            InputSurname1.Visibility = Visibility.Visible;
            LbCognom2.Visibility = Visibility.Visible;
            InputSurname2.Visibility = Visibility.Visible;
            LbEmail.Visibility = Visibility.Visible;
            InputEmail.Visibility = Visibility.Visible;
        }
        void VisibilityStudentInputsOFF()
        {
            LbDni.Visibility = Visibility.Hidden;
            InputDni.Visibility = Visibility.Hidden;
            LbName.Visibility = Visibility.Hidden;
            InputName.Visibility = Visibility.Hidden;
            LbCognom1.Visibility = Visibility.Hidden;
            InputSurname1.Visibility = Visibility.Hidden;
            LbCognom2.Visibility = Visibility.Hidden;
            InputSurname2.Visibility = Visibility.Hidden;
            LbEmail.Visibility = Visibility.Hidden;
            InputEmail.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentInicial()
        {
            BtNew.Visibility = Visibility.Visible;
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveNew.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEdit()
        {
            VisibilityStudentInputsON();
            BtSaveEdit.Visibility = Visibility.Visible;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEditSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNew()
        {
            ClearDg();
            ClearInputs();
            VisibilityStudentInputsON();
            BtSaveNew.Visibility = Visibility.Visible;
            BtNew.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNewSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveNew.Visibility = Visibility.Hidden;
            BtNew.Visibility = Visibility.Visible;
        }
        void ClearDg()
        {
            DadesStudent.Text = string.Empty;
        }
        void ClearInputs()
        {
            InputDni.Text = string.Empty;
            InputName.Text = string.Empty;
            InputSurname1.Text = string.Empty;
            InputSurname2.Text = string.Empty;
            InputEmail.Text = string.Empty;
        }


        #endregion






    }
}
