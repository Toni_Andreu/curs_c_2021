﻿using EstudiantWPF.Lib.DAL;
using EstudiantWPF.Lib.Models;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;


namespace EstudiantWPF.Views
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView : UserControl
    {
        public Subject SelectedSubject
        {
            get
            {
                return _selectedSubject;
            }
            set
            {
                _selectedSubject = value;
                if (value == null)
                {
                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;

                }
                else
                {
                    InputSubject.Text = value.Name;
                    InputTeacher.Text = value.Teacher;;
                }
            }
        }
        Subject _selectedSubject;
        public SubjectsView()
        {
            InitializeComponent();
            DgSubjects.ItemsSource = SubjectsRepository.GetAll();
        }
        public IEnumerable SubjectsList { get; }


        #region SUBJECTS

        void AddNewSubject()
        {
            string name = InputSubject.Text;
            string teacher = InputTeacher.Text;
            string error = string.Empty;

            if (SubjectsRepository.ValidateEmpty(name))
            {
                MessageBox.Show(SubjectsRepository.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (SubjectsRepository.Subjects.ContainsKey(name))
            {
                MessageBox.Show(SubjectsRepository.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (SubjectsRepository.ValidateEmpty(teacher))
            {
                MessageBox.Show(SubjectsRepository.ErrBuidTeacher, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    var subject = new Subject
                    {
                        Id = Guid.NewGuid(),
                        Name = name,
                        Teacher = teacher
                    };
                    //var temp = new Subject(subject.Name, subject.Teacher);
                    SubjectsRepository.Subjects.Add(subject.Name, subject);
                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        void DeleteSubject(string key)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquesta Asignatura?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = SubjectsRepository.Subjects.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        void EditSubject(string name, string teacher)
        {
            name = InputSubject.Text;
            teacher = InputTeacher.Text;
            string error = string.Empty;

            if (SubjectsRepository.ValidateEmpty(name))
            {
                MessageBox.Show(SubjectsRepository.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //else if (Subjects.ContainsKey(name))
            //{
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (SubjectsRepository.ValidateEmpty(teacher))
            {
                MessageBox.Show(SubjectsRepository.ErrBuidTeacher, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    var dades = SubjectsRepository.Subjects[name];
                    dades.Name = name;
                    dades.Teacher = teacher;

                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;
                    DadesSubject.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        #endregion


        #region Btns Selects

        private void BtSelectSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectInicial();
            BtEditSubject.Visibility = Visibility.Visible;
            SelectedSubject = ((Button)sender).DataContext as Subject;
            InputSubject.Text = SelectedSubject.Name;
            DadesSubject.Text = SelectedSubject.Name + "  " + SelectedSubject.Teacher;
        }

        #endregion

        #region Btns CLICK

        private void BtAddSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesSubject.Text = string.Empty;
            VisibilitySubjectNew();
        }
        private void BtAddSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectNewSave();
            AddNewSubject();

            ClearDg();
            ClearInputs();
            DgSubjects.ItemsSource = SubjectsRepository.GetAll();
        }
        private void BtDeleteSubject_Click(object sender, RoutedEventArgs e)
        {
            var selectedSubject = ((Button)sender).DataContext as Subject;
            DeleteSubject(selectedSubject.Name);
            ClearInputs();
            ClearDg();
            BtEditSubject.Visibility = Visibility.Hidden;
            DgSubjects.ItemsSource = SubjectsRepository.GetAll();
        }
        private void BtEditSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectEdit();
            InputSubject.Text = SelectedSubject.Name;
            InputTeacher.Text = SelectedSubject.Teacher;
        }
        private void BtEditSubject_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar el nom de l'Asignatura = > que creien una de nova!
            string name = SelectedSubject.Name;
            string teacher = InputTeacher.Text;

            EditSubject(name, teacher);
            VisibilitySubjectEditSave();
            ClearDg();
            ClearInputs();

            DgSubjects.ItemsSource = SubjectsRepository.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilitySubjectInputsON()
        {
            LbSubject.Visibility = Visibility.Visible;
            LbTeacher.Visibility = Visibility.Visible;
            InputSubject.Visibility = Visibility.Visible;
            InputTeacher.Visibility = Visibility.Visible;
        }
        void VisibilitySubjectInputsOFF()
        {
            LbSubject.Visibility = Visibility.Hidden;
            LbTeacher.Visibility = Visibility.Hidden;
            InputSubject.Visibility = Visibility.Hidden;
            InputTeacher.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectInicial()
        {
            BtNewSubject.Visibility = Visibility.Visible;
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveNewSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEdit()
        {
            VisibilitySubjectInputsON();
            BtSaveEditSubject.Visibility = Visibility.Visible;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEditSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNew()
        {
            ClearDg();
            ClearInputs();
            VisibilitySubjectInputsON();
            BtSaveNewSubject.Visibility = Visibility.Visible;
            BtNewSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNewSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveNewSubject.Visibility = Visibility.Hidden;
            BtNewSubject.Visibility = Visibility.Visible;
        }
        void ClearDg()
        {
            DadesSubject.Text = string.Empty;
        }
        void ClearInputs()
        {
            InputSubject.Text = string.Empty;
            InputTeacher.Text = string.Empty;
        }


        #endregion


    }
}
