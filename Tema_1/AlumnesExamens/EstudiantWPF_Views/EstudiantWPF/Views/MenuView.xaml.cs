﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EstudiantWPF.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MenuView : UserControl
    {
        public MenuView()
        {
            InitializeComponent();
        }


        private void BtSortir_Click(object sender, RoutedEventArgs e)
        {
            string msg = "Vol tancar l'aplicació ?";
            string title = "Atenció!";
            MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dia == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown();
                //Application.Current.MainWindow.Close();
            }
        }
        

        public const string Convencions = "CONVENCIONS:\n" +
            "Hem usat com Key d'Alumnes, el seu Dni. Per tant, No deixem editar els Dni, només esborrar i generar de nou.\n" +
            "Hem usat com a Key de Subjects, el nom de l'Asignatura. Tampoc els deixem editar....\n" +
            "Hem usat com a Key d'Exàmens la concatenació de Dni + Asignatura. Tampoc els deixem editar...\n" +
            "";

        //Inputlegenda.Text = Convencions;
    }


}
