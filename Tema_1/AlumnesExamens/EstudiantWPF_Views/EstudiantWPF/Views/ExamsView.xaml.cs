﻿using EstudiantWPF.Lib.DAL;
using EstudiantWPF.Lib.Models;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;


namespace EstudiantWPF.Views
{
    /// <summary>
    /// Interaction logic for ExamsView.xaml
    /// </summary>
    public partial class ExamsView : UserControl
    {
        public Exam SelectedExam
        {
            get
            {
                return _selectedExam;
            }
            set
            {
                _selectedExam = value;
                if (value == null)
                {
                    InputStudentDni.Text = string.Empty;
                    InputExSubject.Text = string.Empty;
                    InputTimestamp.Text = string.Empty;
                    InputMark.Text = string.Empty;

                }
                else
                {
                    InputStudentDni.Text = value.StudentDni;
                    InputExSubject.Text = value.Subject;
                    InputTimestamp.Text = value.TimeStamp.ToString();
                    InputMark.Text = value.Mark.ToString();
                }
            }
        }
        Exam _selectedExam;
        public ExamsView()
        {
            InitializeComponent();
            DgExams.ItemsSource = ExamsRepository.GetAll();
        }
        public IEnumerable ExamsList { get; }

        #region EXAMS

        void AddNewExam()
        {
            string studentdni = InputStudentDni.Text;
            string subject = InputExSubject.Text;
            DateTime timestamp = DateTime.Now;
            string error = "";
            var stringMark = InputMark.Text;
            double mark = Convert.ToDouble(stringMark);

            //Validations
            if (ExamsRepository.ValidateEmpty(subject))
            {
                MessageBox.Show(ExamsRepository.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (ExamsRepository.ValidateEmpty(studentdni))
            {
                MessageBox.Show(ExamsRepository.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (ExamsRepository.ValidateLength(studentdni))
            {
                MessageBox.Show(ExamsRepository.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            // existeix la key: DNI+Subject?
            else if (ExamsRepository.Exams.ContainsKey(studentdni + subject))
            {
                MessageBox.Show(ExamsRepository.ErrSubjectExist, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (ExamsRepository.ValidateEmpty(stringMark))
            {
                MessageBox.Show(ExamsRepository.ErrBuidMark, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            // valida rango 0- 10 + double
            else if (ExamsRepository.ValidateRang(mark))
            {
                MessageBox.Show(ExamsRepository.ErrMarkRang, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            //Ojo!  Doubles
            else
            {
                try
                {
                    var exam = new Exam
                    {
                        Id = Guid.NewGuid(),
                        Subject = subject,
                        StudentDni = studentdni,
                        Mark = mark,
                        TimeStamp = DateTime.Now,
                    };
                    var temp = studentdni + subject;
                    ExamsRepository.Exams.Add(temp, exam);
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        void DeleteExam(string key)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Examen?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = ExamsRepository.Exams.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        void EditExam(string studentDni, string subject, DateTime timestamp, double mark)
        {
            string newstudentdni = studentDni;
            string newsubject = subject;
            double newmark = mark;
            DateTime newtimestamp = timestamp;
            string error = string.Empty;

            var dades = ExamsRepository.Exams[studentDni + subject];
            try
            {
                dades.StudentDni = newstudentdni;
                dades.Subject = newsubject;
                dades.Mark = newmark;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        #endregion

        #region Btns Selects

        private void BtSelectExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamInicial();
            BtEditExam.Visibility = Visibility.Visible;
            SelectedExam = ((Button)sender).DataContext as Exam;
            DadesExam.Text = SelectedExam.Subject +
                " (ex. " + SelectedExam.TimeStamp +
                ") de: " + SelectedExam.StudentDni +
                "  Nota: " + SelectedExam.Mark;
        }
        #endregion

        #region Btns CLICK

        private void BtAddExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesExam.Text = string.Empty;
            VisibilityExamNew();
        }
        private void BtAddExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamNewSave();
            AddNewExam();

            ClearDg();
            ClearInputs();
            DgExams.ItemsSource = ExamsRepository.GetAll();
        }
        private void BtDeleteExam_Click(object sender, RoutedEventArgs e)
        {
            var selectedExam = ((Button)sender).DataContext as Exam;
            DeleteExam(selectedExam.StudentDni + selectedExam.Subject);
            ClearInputs();
            ClearDg();
            BtEditExam.Visibility = Visibility.Hidden;
            DgExams.ItemsSource = ExamsRepository.GetAll();
        }
        private void BtEditExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityExamEdit();
            InputStudentDni.Text = SelectedExam.StudentDni;
            InputExSubject.Text = SelectedExam.Subject;
            InputTimestamp.Text = Convert.ToString(SelectedExam.TimeStamp);
            InputMark.Text = Convert.ToString(SelectedExam.Mark);
        }
        private void BtEditExam_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar DNi ni Subject = KEY!!
            string studentDni = SelectedExam.StudentDni;
            string subject = SelectedExam.Subject;
            //agafem dades dels inputs
            DateTime timestamp = Convert.ToDateTime(InputTimestamp.Text);
            Double mark = Convert.ToDouble(InputMark.Text);

            EditExam(studentDni, subject, timestamp, mark);
            VisibilityExamEditSave();
            ClearDg();
            ClearInputs();

            DgExams.ItemsSource = ExamsRepository.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilityExamInputsON()
        {
            LbStudentDni.Visibility = Visibility.Visible;
            LbExSubject.Visibility = Visibility.Visible;
            LbTimestamp.Visibility = Visibility.Visible;
            LbMark.Visibility = Visibility.Visible;
            InputStudentDni.Visibility = Visibility.Visible;
            InputExSubject.Visibility = Visibility.Visible;
            InputTimestamp.Visibility = Visibility.Visible;
            InputMark.Visibility = Visibility.Visible;
        }
        void VisibilityExamInputsOFF()
        {
            LbStudentDni.Visibility = Visibility.Hidden;
            LbExSubject.Visibility = Visibility.Hidden;
            LbTimestamp.Visibility = Visibility.Hidden;
            LbMark.Visibility = Visibility.Hidden;
            InputStudentDni.Visibility = Visibility.Hidden;
            InputExSubject.Visibility = Visibility.Hidden;
            InputTimestamp.Visibility = Visibility.Hidden;
            InputMark.Visibility = Visibility.Hidden;
        }
        void VisibilityExamInicial()
        {
            BtNewExam.Visibility = Visibility.Visible;
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveNewExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEdit()
        {
            VisibilityExamInputsON();
            BtSaveEditExam.Visibility = Visibility.Visible;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEditSave()
        {
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNew()
        {
            ClearDg();
            ClearInputs();
            VisibilityExamInputsON();
            BtSaveNewExam.Visibility = Visibility.Visible;
            BtNewExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNewSave()
        {
            VisibilityExamInputsOFF();
            BtSaveNewExam.Visibility = Visibility.Hidden;
            BtNewExam.Visibility = Visibility.Visible;
        }
        void ClearDg()
        {
            DadesExam.Text = string.Empty;
        }
        void ClearInputs()
        {
            InputStudentDni.Text = string.Empty;
            InputExSubject.Text = string.Empty;
            InputTimestamp.Text = string.Empty;
            InputMark.Text = string.Empty;
        }


        #endregion




    }
}
