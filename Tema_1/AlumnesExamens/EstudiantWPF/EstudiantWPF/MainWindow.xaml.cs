﻿using EstudiantWPF.Lib.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EstudiantWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
        public static Dictionary<string, Exam> Exams = new Dictionary<string, Exam>();
        public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();

        public Student SelectedStudent;
        public List<Student> StudentsList
        {
            get
            {
                return Students.Values.ToList();
            }
        }
        public Subject SelectedSubject;
        public List<Subject> SubjectsList
        {
            get
            {
                return Subjects.Values.ToList();
            }
        }
        public Exam SelectedExam;
        private static object Inputlegenda;

        public List<Exam> ExamsList
        {
            get
            {
                return Exams.Values.ToList();
            }
        }
        
        //static string EscapeWord = "ESCAPAAA";

        public MainWindow()
        {
            InitializeComponent();

            DataTest();

            DgStudents.ItemsSource = StudentsList;
            DgSubjects.ItemsSource = SubjectsList;
            DgExams.ItemsSource = ExamsList;
            DgEstadistics.ItemsSource = ExamsList;
            
        }

        #region DATATEST = Dades d'exemple

        //Càrrega inicial de Dades d'exemple
        static void DataTest()
        {
            //public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
            var student1 = new Student("Pep", "Mira", "López", "38412532D");
            Students.Add(student1.Dni, student1);
            var student2 = new Student("Maria", "Mira", "López", "58412532D");
            Students.Add(student2.Dni, student2);
            var student3 = new Student("Carles", "Camps", "Rodriguez", "12345678A");
            Students.Add(student3.Dni, student3);
            var student4 = new Student("Gemma", "Jimenez", "Sanchez", "23456789A");
            Students.Add(student4.Dni, student4);
            var student5 = new Student("Victor", "Martínez", "López", "18412532D");
            Students.Add(student5.Dni, student5);
            var student6 = new Student("Anna", "Vila", "Jimenez", "28412532D");
            Students.Add(student6.Dni, student6);
            var student7 = new Student("Cèlia", "Mira", "Martinez", "48412532D");
            Students.Add(student7.Dni, student7);
            var student8 = new Student("Carla", "Saladie", "López", "68412532D");
            Students.Add(student8.Dni, student8);
            var student9 = new Student("Joan", "Vermell", "López", "78412532D");
            Students.Add(student9.Dni, student9);
            var student10 = new Student("Raquel", "Mirabet", "Sladie", "88412532D");
            Students.Add(student10.Dni, student10);


            //public static Dictionary<string, Subject> Subjects = new Dictionary<string, Subject>();
            var subject1 = new Subject("C Sharp", "José Freire");
            Subjects.Add("C Sharp", subject1);
            var subject2 = new Subject("Ciberseguretat", "Ana Segura");
            Subjects.Add("Ciberseguretat", subject2);
            var subject3 = new Subject("Blockchain", "Carles Santamaria");
            Subjects.Add("Blockchain", subject3);
            var subject4 = new Subject("Flutter & Dart", "Pep Marquès");
            Subjects.Add("Flutter & Dart", subject4);
            var subject5 = new Subject("SQL", "Ângel López");
            Subjects.Add("SQL", subject5);
            var subject6 = new Subject("Python", "Carme Pla");
            Subjects.Add("Python", subject6);

            //public static Dictionary<string, Exam> Exams = new Dictionary<string, Exam>();
            //public Exam(Student studentDni, Subject subject, double mark, DateTime timeStamp)
            DateTime timestamp = DateTime.Now;
            var exam1 = new Exam("12345678A", "Python", 6.2, timestamp);
            Exams.Add("12345678A" + "Python", exam1);
            var exam2 = new Exam("28412532D", "Python", 6.9, timestamp);
            Exams.Add("28412532D" + "Python", exam2);
            var exam3 = new Exam("68412532D", "Python", 8.4, timestamp);
            Exams.Add("68412532D" + "Python", exam3);
            var exam4 = new Exam("48412532D", "Python", 2.1, timestamp);
            Exams.Add("48412532D" + "Python", exam4);
            var exam5 = new Exam("38412532D", "Python", 7.6, timestamp);
            Exams.Add("38412532D" + "Python", exam5);
            var exam6 = new Exam("58412532D", "Python", 4.2, timestamp);
            Exams.Add("58412532D" + "Python", exam6);
            var exam7 = new Exam("12345678A", "SQL", 6.2, timestamp);
            Exams.Add("12345678A" + "SQL", exam7);
            var exam8 = new Exam("28412532D", "SQL", 6.9, timestamp);
            Exams.Add("28412532D" + "SQL", exam8);
            var exam9 = new Exam("68412532D", "SQL", 8.4, timestamp);
            Exams.Add("68412532D" + "SQL", exam9);
            var exam10 = new Exam("48412532D", "SQL", 3.9, timestamp);
            Exams.Add("48412532D" + "SQL", exam10);
            var exam11 = new Exam("38412532D", "SQL", 7.6, timestamp);
            Exams.Add("38412532D" + "SQL", exam11);
            var exam12 = new Exam("58412532D", "SQL", 4.2, timestamp);
            Exams.Add("58412532D" + "SQL", exam12);
        }
        #endregion

        #region Btns Selects

        private void BtSelectStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentInicial();
            BtEdit.Visibility = Visibility.Visible;
            SelectedStudent = ((Button)sender).DataContext as Student;
            DadesStudent.Text = SelectedStudent.Dni + "- " + SelectedStudent.Name + " " + SelectedStudent.Surname1 + " " + SelectedStudent.Surname2;
        }
        private void BtSelectSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectInicial();
            BtEditSubject.Visibility = Visibility.Visible;
            SelectedSubject = ((Button)sender).DataContext as Subject;
            InputSubject.Text = SelectedSubject.Name;
            DadesSubject.Text = SelectedSubject.Name + "  " + SelectedSubject.Teacher;      
        }
        private void BtSelectExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamInicial();
            BtEditExam.Visibility = Visibility.Visible;
            SelectedExam = ((Button)sender).DataContext as Exam;
            DadesExam.Text = SelectedExam.Subject +
                " (ex. " + SelectedExam.TimeStamp +
                ") de: " + SelectedExam.StudentDni +
                "  Nota: " + SelectedExam.Mark;
        }
        #endregion

        #region VISIBILITIES

        void VisibilityStudentInputsON()
        {
            LbDni.Visibility = Visibility.Visible;
            InputDni.Visibility = Visibility.Visible;
            LbName.Visibility = Visibility.Visible;
            InputName.Visibility = Visibility.Visible;
            LbCognom1.Visibility = Visibility.Visible;
            InputSurname1.Visibility = Visibility.Visible;
            LbCognom2.Visibility = Visibility.Visible;
            InputSurname2.Visibility = Visibility.Visible;
        }
        void VisibilityStudentInputsOFF()
        {
            LbDni.Visibility = Visibility.Hidden;
            InputDni.Visibility = Visibility.Hidden;
            LbName.Visibility = Visibility.Hidden;
            InputName.Visibility = Visibility.Hidden;
            LbCognom1.Visibility = Visibility.Hidden;
            InputSurname1.Visibility = Visibility.Hidden;
            LbCognom2.Visibility = Visibility.Hidden;
            InputSurname2.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectInputsON()
        {
            LbSubject.Visibility = Visibility.Visible;
            LbTeacher.Visibility = Visibility.Visible;
            InputSubject.Visibility = Visibility.Visible;
            InputTeacher.Visibility = Visibility.Visible;
        }
        void VisibilitySubjectInputsOFF()
        {
            LbSubject.Visibility = Visibility.Hidden;
            LbTeacher.Visibility = Visibility.Hidden;
            InputSubject.Visibility = Visibility.Hidden;
            InputTeacher.Visibility = Visibility.Hidden;
        }
        void VisibilityExamInputsON()
        {
            LbStudentDni.Visibility = Visibility.Visible;
            LbExSubject.Visibility = Visibility.Visible;
            LbTimestamp.Visibility = Visibility.Visible;
            LbMark.Visibility = Visibility.Visible;
            InputStudentDni.Visibility = Visibility.Visible;
            InputExSubject.Visibility = Visibility.Visible;
            InputTimestamp.Visibility = Visibility.Visible;
            InputMark.Visibility = Visibility.Visible;
        }
        void VisibilityExamInputsOFF()
        {
            LbStudentDni.Visibility = Visibility.Hidden;
            LbExSubject.Visibility = Visibility.Hidden;
            LbTimestamp.Visibility = Visibility.Hidden;
            LbMark.Visibility = Visibility.Hidden;
            InputStudentDni.Visibility = Visibility.Hidden;
            InputExSubject.Visibility = Visibility.Hidden;
            InputTimestamp.Visibility = Visibility.Hidden;
            InputMark.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentInicial()
        {
            BtNew.Visibility = Visibility.Visible;
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveNew.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEdit()
        {
            VisibilityStudentInputsON();
            BtSaveEdit.Visibility = Visibility.Visible;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEditSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNew()
        {
            ClearDg();
            ClearInputs();
            VisibilityStudentInputsON();
            BtSaveNew.Visibility = Visibility.Visible;
            BtNew.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNewSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveNew.Visibility = Visibility.Hidden;
            BtNew.Visibility = Visibility.Visible;
        }

        void VisibilitySubjectInicial()
        {
            BtNewSubject.Visibility = Visibility.Visible;
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveNewSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEdit()
        {
            VisibilitySubjectInputsON();
            BtSaveEditSubject.Visibility = Visibility.Visible;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEditSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNew()
        {
            ClearDg();
            ClearInputs();
            VisibilitySubjectInputsON();
            BtSaveNewSubject.Visibility = Visibility.Visible;
            BtNewSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNewSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveNewSubject.Visibility = Visibility.Hidden;
            BtNewSubject.Visibility = Visibility.Visible;
        }
        
        void VisibilityExamInicial()
        {
            BtNewExam.Visibility = Visibility.Visible;
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveNewExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEdit()
        {
            VisibilityExamInputsON();
            BtSaveEditExam.Visibility = Visibility.Visible;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEditSave()
        {
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNew()
        {
            ClearDg();
            ClearInputs();
            VisibilityExamInputsON();
            BtSaveNewExam.Visibility = Visibility.Visible;
            BtNewExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNewSave()
        {
            VisibilityExamInputsOFF();
            BtSaveNewExam.Visibility = Visibility.Hidden;
            BtNewExam.Visibility = Visibility.Visible;
        }
        void ClearDg()
        {
            DadesStudent.Text = string.Empty;
            DadesSubject.Text = string.Empty;
            DadesExam.Text = string.Empty;
        }
        void ClearInputs()
        {
            InputDni.Text = string.Empty;
            InputName.Text = string.Empty;
            InputSurname1.Text = string.Empty;
            InputSurname2.Text = string.Empty;
            InputSubject.Text = string.Empty;
            InputTeacher.Text = string.Empty;
            InputStudentDni.Text = string.Empty;
            InputExSubject.Text = string.Empty;
            InputTimestamp.Text = string.Empty;
            InputMark.Text = string.Empty;
        }


        #endregion

        #region Btns CLICK

        private void BtSortir_Click(object sender, RoutedEventArgs e)
        {

            string msg = "Vol tancar l'aplicació ?";
            string title = "Atenció!";
            MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dia == MessageBoxResult.No)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown();
                //Application.Current.MainWindow.Close();
            }
        }

        private void BtAddStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesStudent.Text = string.Empty;
            VisibilityStudentNew();
        }
        private void BtAddStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentNewSave();
            AddNewStudent();

            ClearDg();
            ClearInputs();
            DgStudents.ItemsSource = StudentsList;
        }
        private void BtDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = ((Button)sender).DataContext as Student;
            DeleteStudent(selectedStudent.Dni);
            ClearInputs();
            ClearDg();
            BtEdit.Visibility = Visibility.Hidden;
            DgStudents.ItemsSource = StudentsList;
        }
        private void BtEditStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityStudentEdit();
            InputDni.Text = SelectedStudent.Dni;
            InputName.Text = SelectedStudent.Name;
            InputSurname1.Text = SelectedStudent.Surname1;
            InputSurname2.Text = SelectedStudent.Surname2;
        }
        private void BtEditStudent_Click(object sender, RoutedEventArgs e)
        {
            string dni = SelectedStudent.Dni;
            string newdni = InputDni.Text;
            string name = InputName.Text;
            string surname1 = InputSurname1.Text ;
            string surname2 = InputSurname2.Text;

            EditStudent(name, surname1, surname2, dni, newdni);
            VisibilityStudentEditSave();
            ClearDg();
            ClearInputs();

            DgStudents.ItemsSource = StudentsList;
        }

        private void BtAddSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesSubject.Text = string.Empty;
            VisibilitySubjectNew();
        }
        private void BtAddSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectNewSave();
            AddNewSubject();

            ClearDg();
            ClearInputs();
            DgSubjects.ItemsSource = SubjectsList;
        }
        private void BtDeleteSubject_Click(object sender, RoutedEventArgs e)
        {
            var selectedSubject = ((Button)sender).DataContext as Subject;
            DeleteSubject(selectedSubject.Name);
            ClearInputs();
            ClearDg();
            BtEditSubject.Visibility = Visibility.Hidden;
            DgSubjects.ItemsSource = SubjectsList;
        }
        private void BtEditSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectEdit();
            InputSubject.Text = SelectedSubject.Name;
            InputTeacher.Text = SelectedSubject.Teacher;
        }
        private void BtEditSubject_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar el nom de l'Asignatura = > que creien una de nova!
            string name = SelectedSubject.Name;
            string teacher = InputTeacher.Text;

            EditSubject(name, teacher);
            VisibilitySubjectEditSave();
            ClearDg();
            ClearInputs();

            DgSubjects.ItemsSource = SubjectsList;
        }

        private void BtAddExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesExam.Text = string.Empty;
            VisibilityExamNew();
        }
        private void BtAddExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamNewSave();
            AddNewExam();
         
            ClearDg();
            ClearInputs();
            DgExams.ItemsSource = ExamsList;
        }
        private void BtDeleteExam_Click(object sender, RoutedEventArgs e)
        {
            var selectedExam = ((Button)sender).DataContext as Exam;
            DeleteExam(selectedExam.StudentDni+selectedExam.Subject);
            ClearInputs();
            ClearDg();
            BtEditExam.Visibility = Visibility.Hidden;
            DgExams.ItemsSource = ExamsList;
        }
        private void BtEditExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityExamEdit();
            InputStudentDni.Text = SelectedExam.StudentDni;
            InputExSubject.Text = SelectedExam.Subject;
            InputTimestamp.Text = Convert.ToString(SelectedExam.TimeStamp);
            InputMark.Text = Convert.ToString(SelectedExam.Mark);
        }
        private void BtEditExam_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar DNi ni Subject = KEY!!
            string studentDni = SelectedExam.StudentDni;
            string subject = SelectedExam.Subject;
            //agafem dades dels inputs
            DateTime timestamp = Convert.ToDateTime(InputTimestamp.Text);
            Double mark = Convert.ToDouble(InputMark.Text);

            EditExam(studentDni, subject, timestamp, mark);
            VisibilityExamEditSave();
            ClearDg();
            ClearInputs();

            DgExams.ItemsSource = ExamsList;
        }


        private void StadisticsAvg_Click(object sender, RoutedEventArgs e)
        {
            ShowAverage();
        }
        private void StadisticsMax_Click(object sender, RoutedEventArgs e)
        {
            ShowMaximum();
        }
        private void StadisticsMin_Click(object sender, RoutedEventArgs e)
        {
            ShowMinimum();
        }

        #endregion

        #region TAB controls


        #endregion

        #region STUDENTS

        void AddNewStudent()
        {
            string dni = InputDni.Text;
            string name = InputName.Text;
            string cognom1 = InputSurname1.Text;
            string cognom2 = InputSurname2.Text;
            string error = "";

            //Validacions
            if (Student.ValidateEmpty(dni))
            {
                MessageBox.Show(Student.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Student.ValidateLength(dni))
            {
                MessageBox.Show(Student.ErrNo9, "Error",MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (Students.ContainsKey(dni))
            {
                MessageBox.Show(Student.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Student.ValidateEmpty(name))
            {
                MessageBox.Show(Student.ErrBuidNom, "Error",MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (Student.ValidateEmpty(cognom1))
            {
                MessageBox.Show(Student.ErrBuidSurname1, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            // slguns poden NO tenir 2n cognom!!
            //else if (Student.ValidateEmpty(cognom2))
            //{
            //}
            else
            {
                try
                {
                    var student = new Student
                    {
                        Id = Guid.NewGuid(),
                        Dni = dni,
                        Name = name,
                        Surname1 = cognom1,
                        Surname2 = cognom2
                    };

                    Students.Add(student.Dni, student);
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }

        void DeleteStudent(string key)
        {
           try
            {
                string msg = "Està segur que vol esborrar aquest Alumne?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = Students.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        static void EditStudent(string name, string surname1, string surname2, string dni, string newdni)
        {
            string newdni1 = newdni;
            string newname = name;
            string newcognom1 = surname1;
            string newcognom2 = surname2;
            string error = "";

            //Validacions
            if (Student.ValidateEmpty(newdni1))
            {
                MessageBox.Show(Student.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Student.ValidateLength(newdni1))
            {
                MessageBox.Show(Student.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            //else if (Students.ContainsKey(newdni1))
            //{
            //    MessageBox.Show(Student.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (Student.ValidateEmpty(newname))
            {
                MessageBox.Show(Student.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (Student.ValidateEmpty(newcognom1))
            {
                MessageBox.Show(Student.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            //else if (Student.ValidateEmpty(newcognom2))
            //{
            //}
            else
            {
                try
                {
                    var dades = Students[dni];
                    dades.Dni = newdni1;
                    dades.Name = newname;
                    dades.Surname1 = newcognom1;
                    dades.Surname2 = newcognom2;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        #endregion

        #region SUBJECTS

        void AddNewSubject()
        {
            string name = InputSubject.Text;
            string teacher = InputTeacher.Text;
            string error = "";

            if (Subject.ValidateEmpty(name))
            {
                MessageBox.Show(Subject.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Subjects.ContainsKey(name))
            {
                MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Subject.ValidateEmpty(teacher))
            {
                MessageBox.Show(Subject.ErrBuidTeacher, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    var subject = new Subject
                    {
                        Id = Guid.NewGuid(),
                        Name = name,
                        Teacher = teacher
                    };
                    //var temp = new Subject(subject.Name, subject.Teacher);
                    Subjects.Add(subject.Name, subject);
                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        void DeleteSubject(string key)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquesta Asignatura?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = Subjects.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        void EditSubject(string name, string teacher)
        {
            //string newname = "";
            //string newteacher = "";
            name = InputSubject.Text;
            teacher = InputTeacher.Text;
            string error = string.Empty;

            if (Subject.ValidateEmpty(name))
            {
                MessageBox.Show(Subject.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //else if (Subjects.ContainsKey(name))
            //{
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (Subject.ValidateEmpty(teacher))
            {
                MessageBox.Show(Subject.ErrBuidTeacher,"Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    var dades = Subjects[name];
                    dades.Name = name;
                    dades.Teacher = teacher;

                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;
                    DadesSubject.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        #endregion

        #region EXAMS

        void AddNewExam()
        {
            string studentdni = InputStudentDni.Text;
            string subject = InputExSubject.Text;
            DateTime timestamp = DateTime.Now;
            string error = "";
            var stringMark = InputMark.Text;
            double mark = Convert.ToDouble(stringMark);

            //Validations
            if (Exam.ValidateEmpty(subject))
            {
                MessageBox.Show(Exam.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Exam.ValidateEmpty(studentdni))
            {
                MessageBox.Show(Exam.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Exam.ValidateLength(studentdni))
            {
                MessageBox.Show(Exam.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            // existeix la key: DNI+Subject?
            else if (Exams.ContainsKey(studentdni + subject))
            {
                MessageBox.Show(Exam.ErrSubjectExist, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Exam.ValidateEmpty(stringMark))
            {
                MessageBox.Show(Exam.ErrBuidMark, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            // valida rango 0- 10 + double
            else if (Exam.ValidateRang(mark))
            {
                MessageBox.Show(Exam.ErrMarkRang, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            //Ojo!  Doubles
            else
            {
                try
                {
                    var exam = new Exam
                    {
                        Id = Guid.NewGuid(),
                        Subject = subject,
                        StudentDni = studentdni,
                        Mark = mark,
                        TimeStamp = DateTime.Now,
                    };
                    var temp = studentdni + subject;
                    Exams.Add(temp, exam);
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        void DeleteExam(string key)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Examen?";
                string title = "Atenció!"; 
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    var result = Exams.Remove(key);
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        void EditExam(string studentDni, string subject, DateTime timestamp, double mark)
        {
            string newstudentdni = studentDni;
            string newsubject = subject;
            double newmark = mark;
            DateTime newtimestamp = timestamp;
            string error = string.Empty;

            var dades = Exams[studentDni + subject];
            try
            {
                dades.StudentDni = newstudentdni;
                dades.Subject = newsubject;
                dades.Mark = newmark;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        #endregion

        #region ESTADISTIQUES

        void ShowAverage()
        {
            double suma = 0.0;
            double average1 = 0.0;

            try
            {
                foreach (var x in Exams.Values)
                {
                    suma += x.Mark;
                }
                average1 = suma / Exams.Values.Count;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            average.Text = average1.ToString();
        }

        void ShowMaximum()
        {
            var max = 0.0;
            try
            {
                foreach (var x in Exams.Values)
                {
                    if (x.Mark > max)
                        max = x.Mark;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            StadisticsMax.Text = max.ToString();
   
        }

        void ShowMinimum()
        {
            var min = 10.0;
            try
            {
                foreach (var x in Exams.Values)
                {
                    if (x.Mark <= min)
                        min = x.Mark;
                }
                Console.WriteLine("La nota més baixa és: {0}", min);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            StadisticsMin.Text = min.ToString();
        }
        #endregion


        //Dictionary a List d'objete Student
        //List<int> valueList = new List<int>(birdDictionary.Values);
        //List<Student> studentsList = new List<Student>(Students.Values);

        //myCollection = new ObservableCollection<MyClass>(myDictionary.Values)
        //var studentCollec = new ObservableCollection<Student>();
        //StudentsList = Students.Values.ToList();
        public const string Convencions = "CONVENCIONS:\n" +
            "Hem usat com Key d'Alumnes, el seu Dni. Per tant, No deixem editar els Dni, només esborrar i generar de nou.\n" +
            "Hem usat com a Key de Subjects, el nom de l'Asignatura. Tampoc els deixem editar....\n" +
            "Hem usat com a Key d'Exàmens la concatenació de Dni + Asignatura. Tampoc els deixem editar...\n" +
            "";
        public static class Legenda
        {
            public const string Convencions2 = "CONVENCIONS:\n" +
            "Hem usat com Key d'Alumnes, el seu Dni. Per tant, No deixem editar els Dni, només esborrar i generar de nou.\n" +
            "Hem usat com a Key de Subjects, el nom de l'Asignatura. Tampoc els deixem editar....\n" +
            "Hem usat com a Key d'Exàmens la concatenació de Dni + Asignatura. Tampoc els deixem editar...\n" +
            "";
        }

    }//Inputlegenda.Text = Convencions;

}
