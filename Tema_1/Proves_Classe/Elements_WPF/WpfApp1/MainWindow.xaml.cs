﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            #region crear usuaris
            var users = new List<User>();

            var user1 = new User();
            user1.Name = "Pep";
            user1.Email = "pep@pep.com";

            var user2 = new User();
            user2.Name = "María";
            user2.Email = "maria@pep.com";

            users.Add(user1);
            users.Add(user2);

            #endregion crear usuaris
            #region Coloca la lllista en DgUsers
            DgUsers.ItemsSource = users;
            #endregion

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
    public class User
    {
        public string Name {  get; set; }   
        public string Email {  get; set; }

    }
}
