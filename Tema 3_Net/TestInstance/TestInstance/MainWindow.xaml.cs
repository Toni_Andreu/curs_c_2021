﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestInstance
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static MainWindow GlobalYo { get; set; }

        public static void ReloadViewAMessage(string msg)
        {
            GlobalYo.ObjectA_1.LocalMessage = msg;
            GlobalYo.ObjectA_2.LocalMessage = msg;
        }

        public ViewA ObjectA_1 { get; set; }
        public ViewA ObjectA_2 { get; set; }
        public ViewB ObjectB { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            GlobalYo = this;

            ObjectA_1 = new ViewA();
            ObjectA_2 = new ViewA();

            ObjectB = new ViewB();
        }

    }

    public class ViewA
    {
        public static string GlobalMessage { get; set; } = "Hola desde la clase View A";
        public string LocalMessage { get; set; }
        public ViewA()
        {
        }
    }

    public class ViewB
    {
        public ViewB()
        {
            var msg = ViewA.GlobalMessage;

            MainWindow.ReloadViewAMessage("cambiado desde unbojeto de tipo ViewB");
        }

    }
}
