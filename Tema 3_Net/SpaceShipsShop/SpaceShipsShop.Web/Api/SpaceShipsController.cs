﻿using Microsoft.AspNetCore.Mvc;
using SpaceShipsShop.Lib.Models;
using SpaceShipsShop.Web.DAL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SpaceShipsShop.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SpaceShipsController : ControllerBase
    {
        // GET: api/<SpaceShipsController>
        [HttpGet]
        public IEnumerable<SpaceShip> Get()
        {
            var dbContext = new SpaceShipsShopDbContext();
            if (dbContext.SpaceShips.Count() == 0)
            {
                var nave1 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Pegasus",
                    FTLFactor = 10,
                    PassengersCapacity = 100,
                    Color = "Red",
                    Password = "1234"
                };
                var nave2 = new SpaceShip()
                {
                    Id = Guid.NewGuid(),
                    Brand = "Phoenix",
                    FTLFactor = 8,
                    PassengersCapacity = 200,
                    Color = "Black",
                    Password = "12345"
                };

                dbContext.SpaceShips.Add(nave1);
                dbContext.SpaceShips.Add(nave2);

                dbContext.SaveChanges();
            }
            return dbContext.SpaceShips.ToList();
        }

        // GET api/<SpaceShipsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SpaceShipsController>
        [HttpPost]
        public bool Post([FromBody] SpaceShip item)
        {
            //usar repositori, si existeix
            using ( var dbContext = new SpaceShipsShopDbContext())
            {
                if (item.Id != default && dbContext.SpaceShips.Any (x=>x.Id == item.Id))
                {
                    return false;
                }
                if ( item.Id == default )
                    item.Id = Guid.NewGuid();

                dbContext.Add(item);

                dbContext.SaveChanges();
                return true;
            }
        }

        // PUT api/<SpaceShipsController>/5
        //UPDATE
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SpaceShipsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
