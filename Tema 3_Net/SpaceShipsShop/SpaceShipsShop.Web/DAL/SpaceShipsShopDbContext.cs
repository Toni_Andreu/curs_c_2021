﻿using Microsoft.EntityFrameworkCore;
using SpaceShipsShop.Lib.Models;

namespace SpaceShipsShop.Web.DAL
{
    public class SpaceShipsShopDbContext : DbContext
    {
        public DbSet<SpaceShip> SpaceShips { get; set; }
        public SpaceShipsShopDbContext() : base()
        {
             
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SpaceShip>().ToTable("SpaceShips");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                     .UseMySql(connectionString: @"server=localhost;database=spaceships; uid=toni;password=mys; ",

                     new MySqlServerVersion(new Version(8, 0, 23)));


        }
    }
}
