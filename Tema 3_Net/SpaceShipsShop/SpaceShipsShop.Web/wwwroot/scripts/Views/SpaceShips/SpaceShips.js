class Spaceships
{
    Http = null;
    Spaceships = [];
    Brand;
    Password;
    FTLFactor;
    Color;
    PassengersCapacity;

    GridOptions = 
    {
        columnDefs : 
        [          // Ojo!  field en camelCase for json en DB
                { name: 'Brand', field: 'Brand'},
                { name: 'FTL', field: 'FTLFactor'},
                { name: 'Colorines', field: 'Color'},
                { name: 'Passengers Capacity', field: 'PassengersCapacity'},
        ],
        //equilavent al ItemSource!!
        data : []
      };


    constructor($http)  
    {
        this.Http = $http;
        //getall
        this.GetAll();

        //this.Spaceships.push({Modelo: "pegaso", FTL:40, Color: "Blue"});
        //this.Spaceships.push({Modelo: "Ulises", FTL:60, Color: "Red"});
        //this.Spaceships.push({Modelo: "Airbus", FTL:120, Color: "Orange"});
    }

    GetAll()
    {
        var getReq =
        {
            method: 'GET',
            url: 'api/spaceships'
        };
        //quan arribi resposta, ves a LoadShips

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error
    }

    LoadShips(ships)
    {
        this.GridOptions.data.length = 0;  // reset
        for (let i in ships)
            this.GridOptions.data.push(ships[i]);
    }


    // POST api/<StudentsController>
    Add()
    {
        var spaceShip = new SpaceShip();

        spaceShip.Brand = this.Brand;
        spaceShip.Password = this.Password;
        spaceShip.FTLFactor = this.FTLFactor;
        spaceShip.Color = this.Color;
        spaceShip.PassengersCapacity = this.PassengersCapacity;

        var postRequest = {
            method: 'POST',
            url: 'api/spaceships',
            headers: { 'Content-Type': 'application/json' },
            data: spaceShip
        };

        this.Http(postRequest).then(
            (response) => this.#OnAddSuccess(response.data), //on success                
            (error) => {
                alert("server error: " + error.statusText)
            });       // on error
    }

    // m�tode privat = #
    #OnAddSuccess(result)
    {
        if (result == true) {
            alert("s'afegit correctament)")
        }
        else {
            alert("No s'ha pogut afegir!!!)")
        }
        this.GetAll();
        ClearData();
    }

    ClearData()
    {
        this.Brand = "";
        this.Password = "";
        this.FTLFactor = "";
        this.Color = "";
        this.PassengersCapacity = "";
    }
}

App.
  component('spaceship', {   
    templateUrl: 'scripts/views/spaceships/spaceships.html',
    controller: Spaceships,
    controllerAs: "vm"
  });
