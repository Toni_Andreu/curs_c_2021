class Main
{
    Title = "Això és el Main!!!!"

    IsLoginViewActive = true;
    IsSpaceShipViewActive = false;
    IsAirportViewActive = false;
    IsPilotViewActive = false;

    LoginNavClass = "nav-link active";
    GridOptions = "nav-link";
    IsAirportClass = "nav-link";
    IsPilotClass = "nav-link";

    constructor()
    {

    }

    SelectLogin()
    {
        this.IsLoginViewActive = true;
        this.IsSpaceShipViewActive = false;
        this.IsAirportViewActive = false;
        this.IsPilotViewActive = false;
        this.LoginNavClass = "nav-link active";
        this.GridOptions = "nav-link";
        this.IsAirportClass = "nav-link";
        this.IsPilotClass = "nav-link";
    }

    SelectSpaceShips()
    {
        this.IsLoginViewActive = false;
        this.IsSpaceShipViewActive = true;
        this.IsAirportViewActive = false;
        this.IsPilotViewActive = false;
        this.LoginNavClass = "nav-link";
        this.IsAirportClass = "nav-link";
        this.GridOptions = "nav-link active";
        this.IsPilotClass = "nav-link";
    }
    SelectAirports()
    {
        this.IsLoginViewActive = false;
        this.IsSpaceShipViewActive = false;
        this.IsAirportViewActive = true;
        this.IsPilotViewActive = false;
        this.LoginNavClass = "nav-link";
        this.GridOptions = "nav-link";
        this.IsAirportClass = "nav-link active";
        this.IsPilotClass = "nav-link";
    }
    SelectPilots() {
        this.IsLoginViewActive = false;
        this.IsSpaceShipViewActive = false;
        this.IsAirportViewActive = false;
        this.IsPilotViewActive = true;
        this.LoginNavClass = "nav-link";
        this.GridOptions = "nav-link";
        this.IsAirportClass = "nav-link";
        this.IsPilotClass = "nav-link active";
    }
}
//vm = new Main();

//li diem a Angular
App.
  component('main', {   
    templateUrl: 'scripts/views/main/main.html',
    controller: Main,
    controllerAs: "vm"
  });

