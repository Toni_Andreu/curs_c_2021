﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.Lib.Models
{
    public class BookCopy : Entity
    {
        public int? BookCopyNum { get; set; }
        public bool? Avalaible { get; set; }
        public string? Code { get; set; }

        public Guid BookId { get; set; }
        public Book Book { get; set; }


        public virtual List<Loan> Loans { get; set; }

        public bool IsAvailable
        {
            get
            {
                if (Loans != null && Loans.Count > 1)
                {
                    return Loans.Last().ReturnDate != null;
                }
                return true;
            }
        }

        public BookCopy()
        {

        }
        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        #endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidAuthor = "L'autor no pot estar buid!";
        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidTitle = "El títol del llibre no pot estar buid!";
        // DNI existent
        public const string ErrExisteix = "Aquest títol ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix aquest ´titol:";
        internal static readonly object Books;
        #endregion
    }

    public enum BookCopyValidationsTypes
    {
        Ok,
        BookDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        CopyNotFound
    }
}
