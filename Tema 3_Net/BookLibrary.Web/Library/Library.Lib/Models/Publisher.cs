﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Text;
namespace Library.Lib.Models
{
    public class Publisher : Entity
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public Publisher()
        {

        }

    }
}
