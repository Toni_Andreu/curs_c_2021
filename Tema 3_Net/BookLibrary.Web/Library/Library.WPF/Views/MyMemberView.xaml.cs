﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for MyMemberView.xaml
    /// </summary>
    public partial class MyMemberView : UserControl
    {
        #region static
        private static MyMemberView Instance { get; set; }
        public static void Refresh()
        {
            //if (Instance != null && App.CurrentUser != null && !App.IsStaff)

            if (Instance != null && App.CurrentUser != null)
            {
                Instance.SelectedMember = App.CurrentUser as Member;
            }
        }
        #endregion

        public Member SelectedMember
        {
            get
            {
                return _selectedMember;
            }
            set
            {
                _selectedMember = value;
                if (value == null)
                {
                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbSurname1.Text = string.Empty;
                    TbSurname2.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                    TbPhone.Text = string.Empty;
                    TbPass.Password = string.Empty;
                    TbStatus.IsChecked = false;
                }
                else
                {
                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbSurname1.Text = value.Surname1;
                    TbSurname2.Text = value.Surname2;
                    TbEmail.Text = value.Email;
                    TbPhone.Text = value.ContactPhone;
                    TbPass.Password = value.Pass;
                    TbStatus.IsChecked = value.Status;
                }
            }
        }
        Member _selectedMember;

        public MyMemberView()
        {
            InitializeComponent();
            Instance = this;
        }
        private void BtEditMyMember_Click(object sender, RoutedEventArgs e)
        {
            EditMyMember();
        }
        public void EditMyMember()
        {
            if (SelectedMember != null)
            {
                var MembersCopy = SelectedMember.Clone();

                MembersCopy.Dni = TbDni.Text;
                MembersCopy.Name = TbName.Text;
                MembersCopy.Surname1 = TbSurname1.Text;
                MembersCopy.Surname2 = TbSurname2.Text;
                MembersCopy.Email = TbEmail.Text;
                MembersCopy.ContactPhone = TbPhone.Text;
                MembersCopy.Pass = TbPass.Password;
                MembersCopy.Status = TbStatus.IsChecked;
                string error = string.Empty;

                //Validacions
                if (Member.ValidateEmpty(MembersCopy.Dni))
                {
                    MessageBox.Show(Member.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Member.ValidateLength(MembersCopy.Dni))
                {
                    MessageBox.Show(Member.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (Member.ValidateLength(MembersCopy.ContactPhone))
                {
                    MessageBox.Show(Member.ErrPhoneNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (Member.ValidateEmpty(MembersCopy.Name))
                {
                    MessageBox.Show(Member.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (Member.ValidateEmpty(MembersCopy.Surname1))
                {
                    MessageBox.Show(Member.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                //else if (Members.ValidateEmpty(newcognom2))
                //{
                //}
                else
                {
                    try
                    {
                        using (var repo = new MembersRepository())
                        {
                            repo.Edit(MembersCopy);
                            string err = "Canvis guardats!";
                            MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                        MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }

        void ClearTbs()
        {
            TbDni.Text = string.Empty;
            TbName.Text = string.Empty;
            TbSurname1.Text = string.Empty;
            TbSurname2.Text = string.Empty;
            TbEmail.Text = string.Empty;
            TbPhone.Text = string.Empty;
            TbEmail.Text = string.Empty;
            TbPass.Password = string.Empty;
            TbStatus.IsChecked = false;
        }
    }
}
