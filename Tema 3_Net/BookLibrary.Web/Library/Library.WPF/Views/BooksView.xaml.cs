﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for BooksView.xaml
    /// </summary>
    public partial class BooksView : UserControl
    {
        public Book SelectedBook
        {
            get
            {
                return _selectedBook;
            }
            set
            {
                _selectedBook = value;
                if (value == null)
                {
                    TbAuthor.Text = string.Empty;
                    TbPublisher.Text = string.Empty;
                    TbEdition.Text = string.Empty;

                    TbTitle.Text = string.Empty;
                    TbIsbn.Text = string.Empty;
                    TbYear.Text = string.Empty;

                }
                else
                {
                    TbAuthor.Text = value.Author;
                    TbPublisher.Text = value.Synopsis;
                    TbEdition.Text = value.Edition;

                    TbTitle.Text = value.Title;
                    TbIsbn.Text = value.ISBN;
                    TbYear.Text = Convert.ToString(value.Year);
                }
            }
        }
        Book _selectedBook;
        public BooksView()
        {
            InitializeComponent();
            using (var repo = new BooksRepository())
            {
                DgBooks.ItemsSource = repo.GetAll().OrderBy(e => e.TitleAuthor);
            }
        }
        public IEnumerable BooksList { get; }

        #region Books

        public void AddNewBook()
        {
            string author = TbAuthor.Text;
            string publisher = TbPublisher.Text;
            string edition = TbEdition.Text;
            string title = TbTitle.Text;
            string isbn = TbIsbn.Text;
            string year = TbYear.Text;
            string error = string.Empty;

            //Validacions
            if (Book.ValidateEmpty(title))
            {
                MessageBox.Show(Book.ErrBuidTitle, "Error", MessageBoxButton.OK, MessageBoxImage.Error); 
                DgBookCopiesRel.ItemsSource = new List<TabControl>();
            }
            else if (Book.ValidateEmpty(author))
            {
                MessageBox.Show(Book.ErrBuidAuthor, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                DgBookCopiesRel.ItemsSource = new List<TabControl>();
            }
            else
            {
                try
                {
                    var book = new Book
                    {
                        Author = author,
                        Publisher = publisher,
                        Edition = edition,
                        Title = title,
                        ISBN = isbn,
                        Year = Convert.ToInt32(year),
                    };
                    using (var repo = new BooksRepository())
                    {
                        var addResult = repo.Add(book);
                        if (addResult == BookValidationsTypes.Ok)
                        {
                            var addCopiesResult = AddBookCopies(book, 4);

                            if (addCopiesResult == BookCopyValidationsTypes.Ok)
                            {
                                SelectedBook = repo.Get(book.Id);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }
        public void DeleteBook(Book Book)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Llibre?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {

                }
                else
                {
                    //var result = BooksRepository.Books.Remove(key); Dictionary
                    using (var repo = new BooksRepository())
                    {
                        repo.Delete(Book.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        public void EditBook()
        {
            if (SelectedBook != null)
            {
                var BookCopy = SelectedBook.Clone();

                
                BookCopy.Author = TbAuthor.Text;
                BookCopy.Publisher = TbPublisher.Text;
                BookCopy.Edition = TbEdition.Text;
                BookCopy.Title = TbTitle.Text;
                BookCopy.ISBN = TbIsbn.Text;
                BookCopy.Year = Convert.ToInt32(TbYear.Text);
                string error = string.Empty;

                //Validacions
                if (Book.ValidateEmpty(BookCopy.Title))
                {
                    MessageBox.Show(Book.ErrBuidTitle, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Book.ValidateEmpty(BookCopy.Author))
                {
                    MessageBox.Show(Book.ErrBuidAuthor, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    try
                    {
                        using (var repo = new BooksRepository())
                        {
                            repo.Edit(BookCopy);
                        }
                        DgBookCopiesRel.ItemsSource = new List<TabControl>();
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                        MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }

        public void LoadRelatedCopies(Guid id)
        {
            using (var repo = new BookCopiesRepository())
            {
                DgBookCopiesRel.ItemsSource = repo.GetAll()
                                                    .Where(s => s.BookId == id && s.IsAvailable)
                                                    .ToList();
            }
        }
        public BookCopyValidationsTypes AddBookCopies(Book book, int copies)
        {
            using (var repo = new BookCopiesRepository())
            {
                for (var i = 0; i < copies; i++)
                {
                    var bookCopy = new BookCopy()
                    {
                        BookId = book.Id,
                        BookCopyNum = i,
                        Code = book.Title.Substring(1, 3) + (i + 1),
                        Avalaible = false
                    };
                    var result = repo.Add(bookCopy);
                    if (result != BookCopyValidationsTypes.Ok)
                        return result;
                }
            }

            return BookCopyValidationsTypes.Ok;
        }

        #endregion

        #region Btns Selects

        private void BtSelectBook_Click(object sender, RoutedEventArgs e)
        {
            VisibilityBookInicial();
            //BtEdit.Visibility = Visibility.Visible;
            SelectedBook = ((Button)sender).DataContext as Book;
            var id = SelectedBook.Id;
            BtEditBook_Prep();
            //DadesBook.Text = SelectedBook.Title + " - " + SelectedBook.Author ;
            var bookId = SelectedBook.Id;
            LoadRelatedCopies(bookId);
        }

        #endregion

        #region Btns CLICK

        private void BtAddBook_ClickPrep(object sender, RoutedEventArgs e)
        {
            //DadesBook.Text = string.Empty;
            VisibilityBookNew();
        }
        private void BtAddBook_Click(object sender, RoutedEventArgs e)
        {
            VisibilityBookNewSave();
            AddNewBook();

            Clear();
            ClearTbs();
            var repo = new BooksRepository();
            DgBooks.ItemsSource = repo.GetAll();
        }
        private void BtDeleteBook_Click(object sender, RoutedEventArgs e)
        {
            var selectedBook = ((Button)sender).DataContext as Book;
            DeleteBook(selectedBook);
            ClearTbs();
            Clear();
            BtEdit.Visibility = Visibility.Hidden;
            var repo = new BooksRepository();
            DgBooks.ItemsSource = repo.GetAll();
        }
        private void BtEditBook_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityBookEdit();
            TbAuthor.Text = SelectedBook.Author;
            TbPublisher.Text = SelectedBook.Publisher;
            TbEdition.Text = SelectedBook.Edition;
            TbTitle.Text = SelectedBook.Title;
            TbIsbn.Text = SelectedBook.ISBN;
            TbYear.Text = Convert.ToString(SelectedBook.Year);
        }
        private void BtEditBook_Prep()
        {
            VisibilityBookEdit();
            TbAuthor.Text = SelectedBook.Author;
            TbPublisher.Text = SelectedBook.Publisher;
            TbEdition.Text = SelectedBook.Edition;
            TbTitle.Text = SelectedBook.Title;
            TbIsbn.Text = SelectedBook.ISBN;
            TbYear.Text = Convert.ToString(SelectedBook.Year);
        }
        private void BtEditBook_Click(object sender, RoutedEventArgs e)
        {
            EditBook();
            VisibilityBookEditSave();
            Clear();
            ClearTbs();

            var repo = new BooksRepository();
            DgBooks.ItemsSource = repo.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilityBookTbsON()
        {
            LbAuthor.Visibility = Visibility.Visible;
            TbAuthor.Visibility = Visibility.Visible;
            LbPublisher.Visibility = Visibility.Visible;
            TbPublisher.Visibility = Visibility.Visible;
            LbEdition.Visibility = Visibility.Visible;
            TbEdition.Visibility = Visibility.Visible;
            LbTitle.Visibility = Visibility.Visible;
            TbTitle.Visibility = Visibility.Visible;
            LbIsbn.Visibility = Visibility.Visible;
            TbIsbn.Visibility = Visibility.Visible;
            LbYear.Visibility = Visibility.Visible;
            TbYear.Visibility = Visibility.Visible;
        }
        void VisibilityBookTbsOFF()
        {
            LbAuthor.Visibility = Visibility.Hidden;
            TbAuthor.Visibility = Visibility.Hidden;
            LbPublisher.Visibility = Visibility.Hidden;
            TbPublisher.Visibility = Visibility.Hidden;
            LbEdition.Visibility = Visibility.Hidden;
            TbEdition.Visibility = Visibility.Hidden;
            LbTitle.Visibility = Visibility.Hidden;
            TbTitle.Visibility = Visibility.Hidden;
            LbIsbn.Visibility = Visibility.Hidden;
            TbIsbn.Visibility = Visibility.Hidden;
            LbYear.Visibility = Visibility.Hidden;
            TbYear.Visibility = Visibility.Hidden;
        }
        void VisibilityBookInicial()
        {
            BtNew.Visibility = Visibility.Visible;
            VisibilityBookTbsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveNew.Visibility = Visibility.Hidden;
        }
        void VisibilityBookEdit()
        {
            VisibilityBookTbsON();
            BtSaveEdit.Visibility = Visibility.Visible;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityBookEditSave()
        {
            VisibilityBookTbsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityBookNew()
        {
            Clear();
            ClearTbs();
            VisibilityBookTbsON();
            BtSaveNew.Visibility = Visibility.Visible;
            BtNew.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityBookNewSave()
        {
            VisibilityBookTbsOFF();
            BtSaveNew.Visibility = Visibility.Hidden;
            BtNew.Visibility = Visibility.Visible;
        }
        void Clear()
        {
            //DadesBook.Text = string.Empty;
            SelectedBook = null;
        }
        void ClearTbs()
        {
            TbAuthor.Text = string.Empty;
            TbPublisher.Text = string.Empty;
            TbEdition.Text = string.Empty;
            TbTitle.Text = string.Empty;
            TbIsbn.Text = string.Empty;
            TbYear.Text = string.Empty;
        }


        #endregion

    }
}
