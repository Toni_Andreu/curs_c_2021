﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;

namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for ReturnsView.xaml
    /// </summary>
    public partial class MyReturnsView : UserControl
    {
        public MyReturnsView()
        {
            InitializeComponent();
            //Instance = this;   //="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";//="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            ReloadData();
            LbMember.Text = "pepe";  //="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        }
        public void ReloadData()
        {
            Guid memberId = Guid.NewGuid(); // "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            ReloadHistoric(memberId);
            ReloadLoads(memberId);
        }
        private void BtSelectReturn_Click(object sender, RoutedEventArgs e)
        {
            var prestec = ((Button)sender).DataContext as Loan;
            if (prestec != null)
            {
                ReturnLoan(prestec);
                var repo = new LoansRepository();
                DgLoansRelated.ItemsSource = repo.GetAll();
            }
            else
            {
                string err = "Ha hagut un problema amb el retorn!";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void ReturnLoan(Loan prestec)
        {
            if (prestec != null)
            {
                prestec.ReturnDate = DateTime.Now;
                var memberId = prestec.MemberId;
                var prestecCopy = prestec.Clone();
                try
                {
                    using (var repo = new LoansRepository())
                    {
                        repo.Edit(prestecCopy);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ReloadHistoric(memberId);
                ReloadLoads(memberId);
            }
        }
        public void ReloadLoads(Guid id)
        {
            using (var repo = new LoansRepository())
            {
                DgLoansRelated.ItemsSource = repo.GetAll()
                                                    .Where(s => s.MemberId == id && s.ReturnDate == null)
                                                    .ToList();
            }
        }
        public void ReloadHistoric(Guid id)
        {
            using (var repo2 = new LoansRepository())
            {
                DgLoansHistoric.ItemsSource = repo2.GetAll()
                                                    .Where(s => s.MemberId == id && s.ReturnDate != null)
                                                    .ToList();
            }
        }
    }
}