﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for LoansView.xaml
    /// </summary>
    public partial class LoansView : UserControl
    {
        public LoansView()
        {
            InitializeComponent();
            ReloadData();

        }
        public void ReloadData()
        {
            using (var repo = new BooksRepository())
            {
                CbxBook.ItemsSource = repo.GetAll().OrderBy(e => e.TitleAuthor);
            }
            using (var repo = new MembersRepository())
            {
                CbxMember.ItemsSource = repo.GetAll().OrderBy(e => e.FullName);
            }
        }

        private bool ValidaLoansMember
        {
            get
            {
                var member = CbxMember.SelectedItem as Member;
                var id = member.Id;
                var limit = 3;

                using (var repo = new LoansRepository())
                {
                    int loansTotal = repo.GetAll().Count(x => x.MemberId == id && x.ReturnDate == null);
                    if (loansTotal >= limit)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
        public void ReloadLoans(Guid id)
        {
            using (var repo = new LoansRepository())
            {
                DgLoansRelated.ItemsSource = repo.GetAll()
                                                    .Where(s => s.MemberId == id && s.ReturnDate == null)
                                                    .ToList();
            }
        }
        public void ReloadCopies(Guid id)
        {
            using (var repo = new BookCopiesRepository())
            {
                DgCopiesRelated.ItemsSource = repo.GetAll()
                                                    .Where(s => s.BookId == id && s.IsAvailable)
                                                    .ToList();
            }
        }
        private void BtRelCopies_Click(object sender, RoutedEventArgs e)
        {
            var book = CbxBook.SelectedItem as Book;
            var id = book.Id;
            ReloadCopies(id);
        }
        private void BtSelectBookCopy_Click(object sender, RoutedEventArgs e)
        {
            var member = CbxMember.SelectedItem as Member;
            var copy = ((Button)sender).DataContext as BookCopy;
            if (ValidaLoansMember == false)
            {
                string err = "Aquest Lector actualment te 3 préstecs!! \nNo es pot continuar.";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (copy != null && member != null)
            {
                AddLoan(member, copy);
            }
            else
            {
                string err = "Ha hagut un problema en els Id's de Copy i/o de Member";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void AddLoan(Member member, BookCopy copy)
        {
            try
            {
                var loan = new Loan()
                {
                    MemberId = member.Id,
                    BookCopyId = copy.Id,
                    LoanDate = DateTime.Now
                };
                using (var repo = new LoansRepository())
                {
                    var addResult = repo.Add(loan);
                    string err = "Prèstec realitzat correctament!!";
                    MessageBox.Show(err, "Atenció!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //CbxMember.Text = string.Empty;
            CbxBook.Text = string.Empty;
            ReloadLoans(member.Id);



        }
        private void CbxMember_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var member = CbxMember.SelectedItem as Member;
            if (member != null)
            {
                var id = member.Id;
                ReloadLoans(id);
            }
        }
        private void CbxBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var book = CbxBook.SelectedItem as Book;
            if (book != null)
            {
                var id = book.Id;
                ReloadCopies(id);
            }
            else
            { 
                DgCopiesRelated.ItemsSource = new List<Loan>();
            
            }
        }
    }
}
