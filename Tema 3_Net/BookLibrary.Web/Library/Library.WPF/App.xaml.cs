﻿using Common.Lib.Authentification;
using Library.Lib.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Library.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        static internal User? CurrentUser { get; set; }

        static internal bool IsStaff
        {
            get
            {
                if (CurrentUser is Staff)
                    return CurrentUser is Staff;
                else
                    return CurrentUser is Member;
            }
        }
    }
}
