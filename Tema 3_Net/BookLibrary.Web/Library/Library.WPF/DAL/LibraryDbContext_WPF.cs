﻿using Library.Lib.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Library.UI.DAL
{
    public class LibraryDbContext_WPF : DbContext
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCopy> BookCopies { get; set; }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Loan> Loans { get; set; }
        public LibraryDbContext_WPF() : base()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Member>().ToTable("Members");
            modelBuilder.Entity<Book>().ToTable("Books");
            modelBuilder.Entity<BookCopy>().ToTable("BookCopies");
            modelBuilder.Entity<Staff>().ToTable("Staffs");
            modelBuilder.Entity<Loan>().ToTable("Loans");

            modelBuilder.Entity<BookCopy>()
                .HasOne(e => e.Book)
                .WithMany(s => s.BookCopies)
                .HasForeignKey(e => e.BookId);

            modelBuilder.Entity<Loan>()
                 .HasOne(e => e.Member)
                 .WithMany(s => s.Loans)
                 .HasForeignKey(e => e.MemberId);

            modelBuilder.Entity<Loan>()
                 .HasOne(e => e.BookCopy)
                 .WithMany(s => s.Loans)
                 .HasForeignKey(e => e.BookCopyId);

            //modelBuilder.Entity<Exam>()
            //    .HasOne(e => e.Subject)
            //    .WithMany(s => s.Exams)
            //    .HasForeignKey(e => e.SubjectId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // SQL = LT2BS28033\CURSO
        // SQL pecerra = 
        // MySQL pecerra = @"server=localhost;database=booklibrary;uid=Toni;password=mys;",
        // MySQL= @"server=localhost;database=booklibrary; uid=Toni;password=mys; ",
        {
            //optionsBuilder.UseSqlServer(@"server=LT2BS28033\CURSO;Database=booklibrary;Trusted_Connection = true; MultipleActiveResultSets=true");
            optionsBuilder
                    .UseMySql(connectionString: @"server=localhost;database=booklibrary; uid=toni;password=mys; ",

                    new MySqlServerVersion(new Version(8, 0, 23)));
        }
    }
}
