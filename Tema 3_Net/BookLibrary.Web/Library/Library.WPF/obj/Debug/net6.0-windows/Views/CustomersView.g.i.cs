﻿#pragma checksum "..\..\..\..\Views\CustomersView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "206C1222DC308870655DE56C571386567FB6E1F4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Library.UI.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Library.UI.Views {
    
    
    /// <summary>
    /// CustomersView
    /// </summary>
    public partial class CustomersView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 16 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DadesCustomer;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtNew;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtEdit;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtSaveEdit;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtSaveNew;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbDni;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputDni;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbName;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputName;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbCognom1;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputSurname1;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbCognom2;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputSurname2;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbPhone;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputPhone;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbEmail;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox InputEmail;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbPass;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox InputPass;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Views\CustomersView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DgCustomers;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Library.UI.WPF;V1.0.0.0;component/views/customersview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\CustomersView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.DadesCustomer = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.BtNew = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\..\Views\CustomersView.xaml"
            this.BtNew.Click += new System.Windows.RoutedEventHandler(this.BtAddCustomer_ClickPrep);
            
            #line default
            #line hidden
            return;
            case 3:
            this.BtEdit = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\..\..\Views\CustomersView.xaml"
            this.BtEdit.Click += new System.Windows.RoutedEventHandler(this.BtEditCustomer_ClickPrep);
            
            #line default
            #line hidden
            return;
            case 4:
            this.BtSaveEdit = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\..\..\Views\CustomersView.xaml"
            this.BtSaveEdit.Click += new System.Windows.RoutedEventHandler(this.BtEditCustomer_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.BtSaveNew = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\..\Views\CustomersView.xaml"
            this.BtSaveNew.Click += new System.Windows.RoutedEventHandler(this.BtAddCustomer_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.LbDni = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.InputDni = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.LbName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.InputName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.LbCognom1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.InputSurname1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.LbCognom2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.InputSurname2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.LbPhone = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.InputPhone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.LbEmail = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 17:
            this.InputEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.LbPass = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.InputPass = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 20:
            this.DgCustomers = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 21:
            
            #line 43 "..\..\..\..\Views\CustomersView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtSelectCustomer_Click);
            
            #line default
            #line hidden
            break;
            case 22:
            
            #line 50 "..\..\..\..\Views\CustomersView.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtDeleteCustomer_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

