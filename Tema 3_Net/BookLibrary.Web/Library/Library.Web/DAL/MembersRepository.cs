﻿using Library.Lib.Models;
using Library.Web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Library.UI.DAL
{
    public class MembersRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Member> MembersList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public MembersRepository()
        {
            DbContext = new LibraryDbContext();
            DataMember();
        }
        public List<Member> GetAll()
        {
            return DbContext.Members.ToList(); // llegeix la DB
        }

        public Member GetById(Guid id)
        {
            var output = DbContext.Members.Find(id);
            return output;
        }
        public Member GetByDni(string dni)
        {
            var output = DbContext.Members.FirstOrDefault(s => s.Dni == dni);
            return output;
        }
        public List<Member> GetByName(string name)
        {
            var output = DbContext.Members.Where(s => s.Name == name).ToList();
            return output;
        }
        public List<Member> GetByStatus(bool status)
        {
            var output = DbContext.Members.Where(s => s.Status == status).ToList();
            return output;
        }


        public MemberValidationsTypes Add(Member member)
        {
            if (member.Id != default(Guid))
            {
                return MemberValidationsTypes.IdNotEmpty;
            }
            var stdWithSameDni = GetByDni(member.Dni);
            if (stdWithSameDni != null && member.Id != stdWithSameDni.Id)
            {
                //MessageBox.Show(Member.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return MemberValidationsTypes.DniDuplicated;
            }
            try
            {
                if (DbContext.Members.All(s => s.Id != member.Id)) // mes eficient
                {
                    member.Id = Guid.NewGuid();
                    DbContext.Members.Add(member);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return MemberValidationsTypes.Ok;
        }
        public MemberValidationsTypes Delete(Guid id)
        {
            try
            {
                var member = DbContext.Members.Find(id);
                if (member != null)
                {
                    DbContext.Members.Remove(member);
                    DbContext.SaveChanges();
                } 
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return MemberValidationsTypes.Ok;
        }
        public MemberValidationsTypes Edit(Member member)
        {
            if (member.Id == default(Guid))
            {
                //MessageBox.Show(Member.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return MemberValidationsTypes.IdEmpty;
            }
            if (DbContext.Members.All(s => s.Id != member.Id))
            {
                //MessageBox.Show(Member.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return MemberValidationsTypes.StudentNotFound;
            }

            var stdWithSameDni = GetByDni(member.Dni);
            if (stdWithSameDni != null && member.Id != stdWithSameDni.Id)
            {
                //MessageBox.Show(Member.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return MemberValidationsTypes.DniDuplicated;
            }
            try
            {
                var existingStudent = DbContext.Members.Find(member.Id);
                existingStudent.ApplyChanges(member);

                DbContext.Members.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return MemberValidationsTypes.Ok;
        }

        #region DATA
        public void DataMember()
        {
            if (DbContext.Members.Count() == 0)
            {
                var customer1 = new Member("Pep", "Mira", "López", "38412532D", "pep@apple.com", "934556710", "pass1", false);
                DbContext.Members.Add(customer1);
                var customer2 = new Member("Maria", "Mira", "López", "58412532D", "maria@icloud.com", "934556711", "pass2", false);
                DbContext.Members.Add(customer2);
                var customer3 = new Member("Carles", "Camps", "Rodriguez", "12345678A", "carles@apple.com", "934556712", "pass3", false);
                DbContext.Members.Add(customer3);
                var customer4 = new Member("Gemma", "Jimenez", "Sanchez", "23456789A", "gemma@apple.com", "934556713", "pass4", false);
                DbContext.Members.Add(customer4);
                var customer5 = new Member("Victor", "Martínez", "López", "18412532D", "victor@apple.com", "934556714", "pass5", false);
                DbContext.Members.Add(customer5);
                var customer6 = new Member("Anna", "Vila", "Jimenez", "28412532D", "anna@apple.com", "934556715", "pass6", false);
                DbContext.Members.Add(customer6);
                var customer7 = new Member("Cèlia", "Mira", "Martinez", "48412532D", "celia@apple.com", "934556716", "pass7", false);
                DbContext.Members.Add(customer7);
                var customer8 = new Member("Carla", "Saladie", "López", "68412532D", "carla@apple.com", "934556717", "pass8", false);
                DbContext.Members.Add(customer8);
                var customer9 = new Member("Joan", "Vermell", "López", "78412532D", "joan@apple.com", "934556718", "pass9", false);
                DbContext.Members.Add(customer9);
                var customer10 = new Member("Raquel", "Mirabet", "Sladie", "88412532D", "raquel@apple.com", "934556719", "pass10", false);
                DbContext.Members.Add(customer10);

                DbContext.SaveChanges();
            }
        }

        #endregion

        public void Dispose()
        {

        }

    }
}
