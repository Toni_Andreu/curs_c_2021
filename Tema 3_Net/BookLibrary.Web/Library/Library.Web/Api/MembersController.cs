﻿using Library.Lib.Models;
using Library.UI.DAL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MembersController : ControllerBase
    {
        // GET: api/<MembersController>
        [HttpGet]
        public IEnumerable<Member> Get()
        {
            using (var repo = new MembersRepository())
            {
                return repo.GetAll();
            }
        }

        // GET api/<MembersController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<MembersController>
        [HttpPost]
        public MemberValidationsTypes Post([FromBody] Member item)
        {
            using (var repo = new MembersRepository())
            {
                return repo.Add(item);
            }
        }

        // PUT api/<MembersController>/5
        [HttpPut("{id}")]
        public MemberValidationsTypes Edit(Member Members)
        {
            using (var repo = new MembersRepository())
            {
                //return repo.Edit(Members.Id);
                return MemberValidationsTypes.Ok;
            }
        }

        // DELETE api/<MembersController>/5
        [HttpDelete("{id}")]
        public MemberValidationsTypes Delete(Member Members)
        {
            using (var repo = new MembersRepository())
            {
                return repo.Delete(Members.Id);
            }
        }
    }
}
