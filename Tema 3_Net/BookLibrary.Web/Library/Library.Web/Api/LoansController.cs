﻿using Library.Lib.Models;
using Library.UI.DAL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoansController : ControllerBase
    {
        // GET: api/<LoansController>
        [HttpGet]
        public IEnumerable<Loan> Get()
        {
            using (var repo = new LoansRepository())
            {
                return repo.GetAll();
            }
        }

        // GET api/<LoansController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<LoansController>
        [HttpPost]
        public LoanValidationsTypes Post([FromBody] Loan item)
        {
            using (var repo = new LoansRepository())
            {
                return repo.Add(item);
            }
        }

        // PUT api/<LoansController>/5
        [HttpPut("{id}")]
        public LoanValidationsTypes Put(int id, [FromBody] Loan Loans)
        {
            using (var repo = new LoansRepository())
            {
                //return repo.Edit(Members.Id);
                return LoanValidationsTypes.Ok;
            }
        }

        // DELETE api/<LoansController>/5
        [HttpDelete("{id}")]
        public LoanValidationsTypes Delete(Loan Loans)
        {
            using (var repo = new LoansRepository())
            {
                return repo.Delete(Loans.Id);
            }
        }
    }
}
