﻿using Library.Lib.Models;
using Library.UI.DAL;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        // GET: api/<LibraryController>
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            using (var repo = new BooksRepository())
            {
                return repo.GetAll();
            }
        }

        // GET api/<LibraryController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<LibraryController>
        [HttpPost]
        public BookValidationsTypes Post([FromBody] Book item)
        {
            using (var repo = new BooksRepository())
            {
                return repo.Add(item);
            }
        }

        // PUT api/<LibraryController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<LibraryController>/5
        [HttpDelete("{id}")]
        public BookValidationsTypes Delete(Book Books)
        {
            using (var repo = new BooksRepository())
            {
                return repo.Delete(Books.Id);

            }
        }
    }
}
