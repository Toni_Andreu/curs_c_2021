﻿class Start 
{
    static ShowLoginView = true;

    get IsLoginActive()
    {
        return Start.ShowLoginView;
    }

    constructor() 
    {

    }
}

App.
    component('start', 
    {
        templateUrl: 'scripts/views/start/start.html',
        controller: Start,
        controllerAs: "vm"
    });