﻿class BookDetail
{
    //Http = null;
    //#location = null;
    #booksService = null;
    #bookcopiesService = null;
    #scope = null;
    #items = [];
  
    Author = "";
    Publisher = "";
    Edition = "";
    Title = "";
    Isbn = "";
    Year = "";
    
    get Items()
    {
      return this.#items;
    }
    set Items(value)
    {
      this.#items.length = 0;
  
      for(var i in value)
        this.#items.push(value[i]);
    }

    GridBookCopies =
    {
      columnDefs:
      [
        { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.GetCopyID(row.entity)" class="fs-5 bi bi-pencil-square">'},
            { name: 'Còpia num.', field: 'BookCopyNum' },
            { name: 'Avalaible', field: 'IsAvalaible' },
            { name: 'Codi', field: 'Code' },
            { name: 'BookId', field: 'BookId' },
            { name: 'Id', field: 'Id' },

      ],
        data : this.Items,
        appScopeProvider : this    
    };

    constructor(routeParams, booksService, scope)
    {
      var id = routeParams.id;
      this.#booksService = booksService;  
      this.#bookcopiesService = booksService;  
      this.#scope = scope;  
  
      this.Find(id);
    }
  
    Find(id)
    {
      this.#booksService.Find(id).then(
        (item) => this.OnFindSuccess(item)), 
        (error) => this.OnFindError(error);
    }
  
    OnFindSuccess(item)
    {
      this.Author = item.Author;
      this.Publisher = item.Publisher;
      this.Edition = item.Edition;
      this.Title = item.Title;
      this.Isbn = item.Isbn;
      this.Year = item.Year;
  
      this.#scope.$apply();
    }
  
    OnFindError(error)
    {
      alert(error);
    }
  
  }

  BookDetail.$inject = ['$routeParams','BooksService','$scope',];

  App.
  component('bookdetail', {   
    templateUrl: 'scripts/views/NavigationViews/BooksViews/bookdetail/bookdetail.html',
    controller: BookDetail,
    controllerAs: "vm"
  });