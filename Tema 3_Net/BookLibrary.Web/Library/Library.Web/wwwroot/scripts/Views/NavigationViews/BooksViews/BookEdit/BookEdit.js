﻿class BookEdit
{
    //Http = null;
    #location = null;
    #booksService = null;
    #scope = null;
    Book = null;
  
    Author = "";
    Publisher = "";
    Edition = "";
    Title = "";
    Isbn = "";
    Year = "";
    Synopsis = "";
    Description = "";
    Signature = "";
    
    

    constructor(routeParams, location, booksService, scope)
    {
      let id = routeParams.id;
      this.#location = location;
      this.#booksService = booksService;  
      this.#scope = scope;  
  
      this.Find(id);
    }
  
    Find(id)
    {
      this.#booksService.Find(id).then(
        (item) => 
        {
           this.Book = item.Clone();
           this.Author = item.Author;
           this.Publisher = item.Publisher;
           this.Edition = item.Edition;
           this.Title = item.Title;
           this.Isbn = item.Isbn;
           this.Year = item.Year;
           this.Synopsis = this.Synopsis;
           this.Description = this.Description;
           this.Signature = this.Signature;

           this.#scope.$apply();
          }, 
        (error) => this.OnFindError(error));
    }
  
    Update()
    {
      this.Book.Author = this.Author;
      this.Book.Publisher = this.Publisher;
      this.Book.Edition = this.Edition;
      this.Book.Title = this.Title;
      this.Book.Isbn = this.Isbn;
      this.Book.Year = this.Year;
      this.Book.Synopsis = this.Synopsis;
      this.Book.Description = this.Description;
      this.Book.Signature = this.Signature;
  
      this.#booksService.Update(this.Book).then(
        (result) => this.OnUpdateSuccess(result), 
        (error) => this.OnUpdateError(error));
    }
  
    OnUpdateSuccess(result)
    {
      if (result)
        this.#location.path("/Books");
      else
        alert("no se ha podido guardar");
    }

    OnUpdateError(error)
    {
      alert(error);
    }

}

BookEdit.$inject = ['$routeParams','$location', 'BooksService', '$scope'];

App.
    component('bookedit', {
        templateUrl: 'scripts/views/navigationviews/booksviews/bookedit/bookedit.html',
        controller: BookEdit, // com es diu la Classe
        controllerAs: "vm"
    });