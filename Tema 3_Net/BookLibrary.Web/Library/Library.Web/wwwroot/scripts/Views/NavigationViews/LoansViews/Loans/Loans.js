﻿class Loans
{
  //Http = null;
  //Loans = [];

    //# = privat
  #scope = null;
  #loansService = null;
  #membersService = null;
  #bookCopiesService = null;
  #booksService = null;
  #location = null;
  #itemsLoans = [];
  #itemsCopies = [];
  //   { Id: '1', BookCopyNum: 'Option A', IsAvalaible: true, Code: "a1", BookId: "b1" },
  //   { Id: '2', BookCopyNum: 'Option B', IsAvalaible: true, Code: "a2", BookId: "b2" },
  //   { Id: '3', BookCopyNum: 'Option C', IsAvalaible: true, Code: "a3", BookId: "b3" }
  // ];
  #selectedMember = null;
  #selectedBook = null;
  model= null;
  availableMembers = [];
  availableBooks = [];
  availableCopies = [];
  availableLoans = [];
  /*availableOptions = [
    { id: '1', name: 'Option A' },
    { id: '2', name: 'Option B' },
    { id: '3', name: 'Option C', selected: true }
  ];*/
  
  get ItemsLoans()
  {
    return this.#itemsLoans;
  }
  set ItemsLoans(value)
  {
    this.#itemsLoans.length = 0;

    for(var i in value)
      this.#itemsLoans.push(value[i]);
  }

  get ItemsCopies()
  {
    return this.#itemsCopies;
  }
  set ItemsCopies(value)
  {
    this.#itemsCopies.length = 0;

    for(var i in value)
      this.#itemsCopies.push(value[i]);
  }

  get SelectedMember()
  {
    return this.#selectedMember;
  }
  set SelectedMember(value)
  {
    this.#selectedMember = value;
    this.GetMemberLoans(value.Id);
  }

  get SelectedBook()
  {
    return this.#selectedBook;
  }
  set SelectedBook(value)
  {
    this.#selectedBook = value;
    this.GetBookCopies(value.Id);
  }


  GridLoansMember =
    {
      columnDefs:
        [
                { name: 'Data prèstec', field: 'LoanDate' },
                { name: 'Data retorn', field: 'ReturnDate' },
                { name: 'BookCopyId', field: 'BookCopyId' },
                { name: 'MemberId', field: 'MemberId' },

        ],
        //#data: []
        data : this.ItemsLoans,
        //el seu controlador sóc jo = Loans
        appScopeProvider : this    
    };

    GridBookCopies =
    {
      columnDefs:
        [
            { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.SelectBookCopy(row.entity)" class="fs-5 bi bi-pencil-square"></i>'},
                { name: 'Còpia num.', field: 'BookCopyNum' },
                { name: 'Avalaible', field: 'IsAvalaible' },
                { name: 'Codi', field: 'Code' },
                { name: 'BookId', field: 'BookId' },
                { name: 'Id', field: 'Id' },

        ],
        data : this.ItemsCopies,
        appScopeProvider : this    
    };

 constructor(scope, location, loansService, membersService, booksService, bookCopiesService )
  {
    this.#scope = scope;
    this.#location = location;
    this.#loansService = loansService;
    this.#membersService = membersService;
    this.#booksService = booksService;
    this.#bookCopiesService = bookCopiesService;
    this.GetMembers();
    this.GetBooks();
  }

  GetMemberLoans(memberId)
  {
    this.#loansService.GetMembersLoans(memberId).then(
      (items) => this.OnGetMemberLoansSuccess(items), 
      (error) => this.OnGetMemberLoansError(error));
  }
  
  GetBookCopies(bookId)
  {
    this.#bookCopiesService.Find(bookId).then(
      (items) => this.OnGetBookCopiesSuccess(items), 
      (error) => this.OnGetBookCopiesError(error));
  }

  SelectBookCopy(item)
  {
    var member = this.SelectedMember.Id;
    var copy = item;
    if (copy != null && member != null)
    {
        this.AddLoan(member, copy);
    }
    else
    {
        var err = "Ha hagut un problema en els Id's de Copy i/o de Member";
        alert(err);
    }
  }

  AddLoan( member, copy)
  {
      var loan = new Loan()
      loan.MemberId = member;
      loan.BookCopyId = copy.Id;
      loan.LoanDate = new Date();
      this.#loansService.Add(loan).then(
        (result) => this.OnGetAddLoanSuccess(result), 
        (error) => this.OnGetAddLoanError(error));
  }

  //Combox
  GetMembers()
  {
    this.#membersService.GetAll().then(
      (items) => this.OnGetMemberSuccess(items), 
      (error) => this.OnGetMemberError(error));
  }

  GetBooks()
  {
    this.#booksService.GetAll().then(
      (items) => this.OnGetBooksSuccess(items), 
      (error) => this.OnGetBooksError(error));
  }

  //success/errors
  OnGetAddLoanSuccess(items)
  {
    // venim d'un true (d'un set) no rebem Items!!, sinó un resultat
    //this.itemsLoans = items;
    this.#scope.$apply();
    this.GetMemberLoans(this.SelectedMember.Id);
  }

  OnGetAddLoanError(error)
  {
    alert(error);
  }

  OnGetMemberLoansSuccess(items)
  {
    this.ItemsLoans = items;
    this.#scope.$apply();
  }

  OnGetMemberLoansError(error)
  {
    alert(error);
  }

  OnGetMemberSuccess(items)
  {
    this.availableMembers = items;
    this.#scope.$apply();
  }

  OnGetMemberError(error)
  {
    alert(error);
  }

  OnGetBooksSuccess(items)
  {
    this.availableBooks = items;
    this.#scope.$apply();
  }

  OnGetBooksError(error)
  {
    alert(error);
  }

  OnGetBookCopiesSuccess(items)
  {
    this.ItemsCopies = items;
    this.#scope.$apply();
  }

  OnGetBookCopiesError(error)
  {
    alert(error);
  }

  GotoReturns()
  {
    this.#location.path("/Loans/Returns");
  }
  
  NoEdit()
  {

  }

}

Loans.$inject = ['$scope', '$location', 'LoansService','MembersService', 'BooksService', 'BookCopiesService'];

App.
    component('loans', {
        templateUrl: 'scripts/views/navigationviews/LoansViews/Loans/Loans.html',
        controller: Loans, // com es diu la Classe
        controllerAs: "vm"
    });