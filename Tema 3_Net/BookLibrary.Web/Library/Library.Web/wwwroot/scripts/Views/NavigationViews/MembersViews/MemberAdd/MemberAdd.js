﻿class MemberAdd
{
    //Http = null;
    #location = null;
    #membersService = null;
    Members = [];

    Name = "";
    Surname1 = "";
    Surname2 = "";
    Dni = "";
    Email = "";
    ContactPhone = "";
    Pass = "";
    Status = "";
    

    constructor(location, membersService)
    {
        this.#location = location;
        this.#membersService = membersService;  
    }
  
    Add()
    {
        var member = new Book();
        member.Name = this.Name;
        member.Surname1 = this.Surname1;
        member.Surname2 = this.Surname2;
        member.Dni = this.Dni;
        member.Email = this.Email;
        member.ContactPhone = this.ContactPhone;
        member.Pass = this.Pass;
        member.Status = this.Status;


        this.#membersService.Add(member).then(
            (item) => this.OnAddSuccess(item), 
            (error) => this.OnAddError(error));
        }
      
        OnAddSuccess(result)
        {
          if (result)
            this.#location.path("/Members");
          else
            alert("No s'ha pogut guardar!");
        }
      
        OnAddError(error)
        {
          alert(error);
        }
      
      }

MemberAdd.$inject = ['$location', 'MembersService'];

App.
    component('memberadd', {
        templateUrl: 'scripts/views/navigationviews/membersviews/memberadd/memberadd.html',
        controller: MemberAdd, // com es diu la Classe
        controllerAs: "vm"
    });