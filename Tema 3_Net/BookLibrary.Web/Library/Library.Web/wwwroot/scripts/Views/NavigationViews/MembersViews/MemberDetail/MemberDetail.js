﻿class MemberDetail
{
    //Http = null;
    //#location = null;
    #membersService = null;
    #scope = null;
  
    Name = "";
    Surname1 = "";
    Surname2 = "";
    Dni = "";
    Email = "";
    ContactPhone = "";
    Pass = "";
    Status = "";
    

    constructor(routeParams, membersService, scope)
    {
      var id = routeParams.id;
      this.#membersService = membersService;  
      this.#scope = scope;  
  
      this.Find(id);
    }
  
    Find(id)
    {
      this.#membersService.Find(id).then(
        (item) => this.OnFindSuccess(item)), 
        (error) => this.OnFindError(error);
    }
  
    OnFindSuccess(item)
    {
        this.Name = item.Name;
        this.Surname1 = item.Surname1;
        this.Surname2 = item.Surname2;
        this.Dni = item.Dni;
        this.Email = item.Email;
        this.ContactPhone = item.ContactPhone;
        this.Pass = item.Email;
        this.Status = item.Status;
  
      this.#scope.$apply();
    }
  
    OnFindError(error)
    {
      alert(error);
    }
  
  }

MemberDetail.$inject = ['$routeParams','MembersService','$scope',];

App.
    component('memberdetail', {
        templateUrl: 'scripts/views/navigationviews/membersviews/memberdetail/memberdetail.html',
        controller: MemberDetail, // com es diu la Classe
        controllerAs: "vm"
    });