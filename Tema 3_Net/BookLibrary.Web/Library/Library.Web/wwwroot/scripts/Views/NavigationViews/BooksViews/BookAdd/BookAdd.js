﻿class BookAdd
{
    //Http = null;
    #location = null;
    #booksService = null;
    Books = [];

    Author = "";
    Publisher = "";
    Edition = "";
    Title = "";
    Isbn = "";
    Year = "";
    

    constructor(location, booksService)
    {
        this.#location = location;
        this.#booksService = booksService;  
    }
  
    Add()
    {
      var book = new Book();
      book.Author = this.Author;
      book.Publisher = this.Publisher;
      book.Edition = this.Edition;
      book.Title = this.Title;
      book.Isbn = this.Isbn;
      book.Year = this.Year;


      this.#booksService.Add(book,4).then(
          (item) => this.OnAddSuccess(item), 
          (error) => this.OnAddError(error));
    }
      
    OnAddSuccess(result)
    {
      if (result)
        this.#location.path("/Books");
      else
        alert("no se ha podido guardar");
    }
  
    OnAddError(error)
    {
      alert(error);
    }
      
  }

BookAdd.$inject = ['$location', 'BooksService'];

App.
    component('bookadd', {
        templateUrl: 'scripts/views/navigationviews/booksviews/bookadd/bookadd.html',
        controller: BookAdd, // com es diu la Classe
        controllerAs: "vm"
    });