﻿class MemberEdit
{
    //Http = null;
    #location = null;
    #membersService = null;
    #scope = null;
    Member = null;
  
    Name = "";
    Surname1 = "";
    Surname2 = "";
    Dni = "";
    Email = "";
    ContactPhone = "";
    Pass = "";
    Status = "";
    

    constructor(routeParams, location, membersService, scope)
    {
      let id = routeParams.id;
      this.#location = location;
      this.#membersService = membersService;  
      this.#scope = scope;  
  
      this.Find(id);
    }
  
    Find(id)
    {
      this.#membersService.Find(id).then(
        (item) => 
        {
              this.Member = item.Clone();
              this.Name = item.Name;
              this.Surname1 = item.Surname1;
              this.Surname2 = item.Surname2;
              this.Dni = item.Dni;
              this.Email = item.Email;
              this.ContactPhone = item.ContactPhone;
              this.Pass = item.Pass;
              this.Status = item.Status;

           this.#scope.$apply();
          }, 
        (error) => this.OnFindError(error));
    }
  
    Update()
    {
        this.Member.Name = this.Name;
        this.Member.Surname1 = this.Surname1;
        this.Member.Surname2 = this.Surname2;
        this.Member.Dni = this.Dni;
        this.Member.Email = this.Email;
        this.Member.ContactPhone = this.ContactPhone;
        this.Member.Pass = this.Pass;
        this.Member.Status = this.Status;
  
      this.#membersService.Update(this.Member).then(
        (item) => this.OnUpdateSuccess(item), 
        (error) => this.OnUpdateError(error));
    }
  
    OnUpdateSuccess(result)
    {
      if (result)
        this.#location.path("/Members");
      else
        alert("no se ha podido guardar");
    }

    OnUpdateError(error)
    {
      alert(error);
    }

}

MemberEdit.$inject = ['$routeParams','$location', 'MembersService', '$scope'];

App.
    component('memberedit', {
        templateUrl: 'scripts/views/navigationviews/membersviews/memberedit/memberedit.html',
        controller: MemberEdit, // com es diu la Classe
        controllerAs: "vm"
    });