﻿class LoanReturns
{
  //Http = null;
  #scope = null;
  #loansService = null;
  #membersService = null;
  #location = null;
  #itemsLoans = [];
  #selectedMember = null;
  model= null;
  availableMembers = [];
  availableLoans = [];
  availableLoansReturned = [];
  
  get ItemsLoans()
  {
    return this.#itemsLoans;
  }
  set ItemsLoans(value)
  {
    this.#itemsLoans.length = 0;

    for(var i in value)
      this.#itemsLoans.push(value[i]);
  }


  get SelectedMember()
  {
    return this.#selectedMember;
  }
  set SelectedMember(value)
  {
    this.#selectedMember = value;
    this.GetMemberLoans(value.Id);
  }

  //{ name: 'Actions', cellEditableCondition: false, cellTemplate: '<button class="btn btn-outline-primary" ng-click: >Retornar</button>'},
  // Filtrar only sense dataReturn   ************************
  GridLoansMember =
    {
      columnDefs:
        [
            { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.Return(row.entity)" class="fs-5 bi bi-pencil-square"></i>'},
                { name: 'Data prèstec', field: 'LoanDate' },
                { name: 'Data retorn', field: 'ReturnDate' },
                { name: 'BookCopyId', field: 'BookCopyId' },
                { name: 'MemberId', field: 'MemberId' },

        ],
        //#data: []
        data : this.ItemsLoans,
        //el seu controlador sóc jo = Loans
        appScopeProvider : this    
    };

    // Filtrar only AMB dataReturn   ************************
  GridLoansReturned =
  {
    columnDefs:
      [
        { name: 'LoanDate', field: 'LoanDate' },
        { name: 'ReturnDate', field: 'ReturnDate' },
        { name: 'Cognom 2', field: 'Surname2' },
        { name: 'BookCopyId', field: 'BookCopyId' },
        { name: 'MemberId', field: 'MemberId' },
      ],
      //#data: []
      data : this.ItemsReturned,
      //el seu controlador sóc jo = Members
      appScopeProvider : this    
  };




  constructor(scope, location, loansService, membersService)
  {
      this.#scope = scope;
      this.#location = location;
      this.#loansService = loansService;  
      this.#membersService = membersService;
      this.GetMembers();
  }

  GetMemberLoans(memberId)
  {
    this.#loansService.GetMembersLoans(memberId).then(
      (items) => this.OnGetMemberLoansSuccess(items), 
      (error) => this.OnGetMemberLoansError(error));
  }
  GetMemberLoansReturned(memberId)
  {
    this.#loansService.GetMembersLoansReturned(memberId).then(
      (items) => this.OnGetMemberLoansReturnedSuccess(items), 
      (error) => this.OnGetMemberLoansReturnedError(error));
  }

  /*Find(id)
    {
      this.#loansService.Find(id).then(
        (item) => 
        {
           this.Loan = item.Clone();
           this.LoanDate = item.LoanDate;
           this.ReturnDate = item.ReturnDate;
           this.BookCopyId = item.BookCopyId;
           this.MemberId = item.MemberId;

           this.#scope.$apply();
          }, 
        (error) => this.OnFindError(error));
    }*/

  Return(loan)
  {
      let clonedLoan = loan.Clone();
      clonedLoan.LoanDate = this.LoanDate;
      clonedLoan.ReturnDate = new Date();
      clonedLoan.BookCopyId = this.BookCopyId;
      clonedLoan.MemberId = this.MemberId;


      this.#loansService.Return(clonedLoan).then(
          (result) => this.OnReturnSuccess(result),  
          (error) => this.OnReturnError(error));
  }
    
  OnReturnSuccess(result)
  {
    this.#scope.$apply();
    //actualitza
    this.GetMemberLoans(this.SelectedMember.Id);
    /*if (result)
      this.#location.path("/Loans");
    else
      alert("no se ha podido guardar");*/
  }

  OnReturnError(error)
  {
    alert(error);
  }

  //Combox
  GetMembers()
  {
    this.#membersService.GetAll().then(
      (items) => this.OnGetMemberSuccess(items), 
      (error) => this.OnGetMemberError(error));
  }
  OnGetMemberSuccess(items)
  {
    this.availableMembers = items;
    this.#scope.$apply();
  }

  OnGetMemberError(error)
  {
    alert(error);
  }


  OnGetMemberLoansSuccess(items)
  {
    this.ItemsLoans = items;
    this.#scope.$apply();
  }

  OnGetMemberLoansError(error)
  {
    alert(error);
  }

  OnGetMemberLoansReturnedSuccess(items)
  {
    this.ItemsLoans = items;
    this.#scope.$apply();
  }

  OnGetMemberLoansReturnedError(error)
  {
    alert(error);
  }







  GotoLoans()
  {
    this.#location.path("/Loans");
  }


      
}

LoanReturns.$inject = ['$scope','$location', 'LoansService','MembersService'];

App.
    component('loanreturns', {
        templateUrl: 'scripts/views/navigationviews/loansviews/loanreturns/loanreturns.html',
        controller: LoanReturns, // com es diu la Classe
        controllerAs: "vm"
    });