﻿class Books
{
    //Http = null;
    //Books = [];
    //Author;
    //Publisher;
    //Edition;
    //Title;
    //Isbn;
    //Year;
    //# = privat
    #scope = null;
    #location = null;
    #items = [];
    #booksService = null;


    get Items()
    {
      return this.#items;
    }
    set Items(value)
    {
      this.#items.length = 0;
  
      for(var i in value)
        this.#items.push(value[i]);
    }

    GridOptions =
        {
           enableSorting: true,
           enableColumnResizing: true,
            columnDefs:
                [
                    { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.Edit(row.entity)" class="fs-5 bi bi-pencil-square"></i><i ng-click="grid.appScope.Detail(row.entity)" class="fs-5 bi bi-play"></i><i ng-click="grid.appScope.Delete(row.entity)" class="fs-5 bi bi-trash"></i>' , enableSorting: false },
                    { name: 'Author', field: 'Author' },
                    { name: 'Title', field: 'Title' },
                    { name: 'Year', field: 'Year' },
                    { name: 'Edition', field: 'Edition', enableSorting: false  },
                    { name: 'Publisher', field: 'Publisher' },
                    { name: 'ISBN', field: 'Isbn' },
                ],
            //#data: []
            data : this.Items,
            //el seu controlador sóc jo = Books
            appScopeProvider : this

            
        };
        constructor(scope, location, booksService)
        {
          this.#scope = scope;
          this.#location = location;
          this.#booksService = booksService;
          this.GetAll();
        }
      
        SingleFilter (renderableRows)
        {
          
        }
        GetAll()
        {
          this.#booksService.GetAll().then(
              (items)=> this.OnGetAllSuccess(items), 
              (error) => this.OnGetAllError(error));
        }
      
        OnGetAllSuccess(items)
        {
          this.Items = items;
          this.#scope.$apply();
        }
      
        OnGetAllError(error)
        {
          alert(error);
        }
      
        GotoAdd()
        {
          this.#location.path("/Books/Add");
        }
      
        Edit(item)
        {
          this.#location.path("/Books/Edit/" + item.Id)
        }
      
        Detail(item)
        {    
          //alert("mostrar " + item.Title);
          this.#location.path("/Books/Detail/" + item.Id)
        }
      
        Delete(item)
        {
          var r = confirm("Estàs segur que vols esborrar aquest Llibre: " + item.Title  + " ?");
          if (r == true) 
          {
            this.#booksService.Delete(item.Id).then(
              (result) => 
              {
                if(result)
                {
                //refresh
                  this.GetAll();
                  alert("El llibre ha estat esborrat correctament!");
                }
              }, 
              (error) => alert(error));
          } 
        }
            
      

    //constructor($http) {
    //    this.Http = $http;
    //    this.GetAll();
    //}

    //GetAll() {
    //    var getReq =
    //    {
    //        method: 'GET',
    //        url: 'api/Books'
    //    };

    //    this.Http(getReq).then(
    //        (response) => {
    //            this.LoadBooks(response.data.$values);
    //        }, // on success
    //        (error) =>
    //        {
    //            alert(error.data);
    //        });                   // on error
    //}

    //LoadBooks(books) {
    //    this.GridOptions.data.length = 0;   // clear array

    //    for (let i in books)
    //        this.GridOptions.data.push(books[i]);
    //}


}

Books.$inject = ['$scope', '$location', 'BooksService'];

App.
    component('books', {
        templateUrl: 'scripts/views/navigationviews/booksviews/bookslist/bookslist.html',
        controller: Books, // com es diu la Classe
        controllerAs: "vm"
    });