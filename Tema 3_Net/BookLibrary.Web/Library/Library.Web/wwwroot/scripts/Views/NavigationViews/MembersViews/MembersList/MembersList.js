﻿class Members
{
  //Http = null;
  //Members = [];

    //# = privat
    #scope = null;
    #location = null;
    #items = [];
    #membersService = null;

  get Items()
  {
    return this.#items;
  }
  set Items(value)
  {
    this.#items.length = 0;

    for(var i in value)
      this.#items.push(value[i]);
  }
  //b3ea7c2e-7e70-4224-a8c6-50591b28aa29

  GridOptions =
    {
      columnDefs:
        [
          { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.Edit(row.entity)" class="fs-5 bi bi-pencil-square"></i><i ng-click="grid.appScope.Detail(row.entity)" class="fs-5 bi bi-play"></i><i ng-click="grid.appScope.Delete(row.entity)" class="fs-5 bi bi-trash"></i>'},
            { name: 'Nom', field: 'Name' },
            { name: 'Cognom 1', field: 'Surname1' },
            { name: 'Cognom 2', field: 'Surname2' },
            { name: 'Dni', field: 'Dni' },
            { name: 'Email', field: 'Email' },
            { name: 'Telèfon', field: 'ContactPhone' },
            { name: 'Pass', field: 'Pass' },
            { name: 'Status', field: 'Status' },
        ],
        //#data: []
        data : this.Items,
        //el seu controlador sóc jo = Members
        appScopeProvider : this    
    };

 constructor(scope, location, membersService)
  {
    this.#scope = scope;
    this.#location = location;
     this.#membersService = membersService;
    this.GetAll();
  }

  GetAll()
  {
    this.#membersService.GetAll().then(
      (items) => this.OnGetAllSuccess(items), 
      (error) => this.OnGetAllError(error));
  }

  OnGetAllSuccess(items)
  {
    this.Items = items;
    this.#scope.$apply();
  }

  OnGetAllError(error)
  {
    alert(error);
  }

  GotoAdd()
  {
    this.#location.path("/Members/Add");
  }

  Edit(item)
  {
    this.#location.path("/Members/Edit/" + item.Id)
  }

  Detail(item)
  {    
    //alert("mostrar " + item.Title);
    this.#location.path("/Members/Detail/" + item.Id)
  }

  Delete(item)
  {
    var r = confirm("Estàs segur que vols esborrar aquest Lector: " + item.Dni  + " ?");
    if (r == true) 
    {
      this.#membersService.Delete(item.Id).then(
        (result) => 
        {
          if(result)
          {
          //refresh
            this.GetAll();
            alert("El Lector ha estat esborrat correctament!");
          }
        }, 
        (error) => alert(error));
    } 
  }
      

    //constructor($http) {
    //    this.Http = $http;
    //    this.GetAll();
    //}

    //GetAll() {
    //    var getReq =
    //    {
    //        method: 'GET',
    //        url: 'api/Members'
    //    };

    //    this.Http(getReq).then(
    //        (response) => {
    //            this.LoadMembers(response.data.$values);
    //        }, // on success
    //        (error) =>
    //        {
    //            alert(error.data);
    //        });                   // on error
    //}

    //LoadMembers(books) {
    //    this.GridOptions.data.length = 0;   // clear array

    //    for (let i in books)
    //        this.GridOptions.data.push(books[i]);
    //}


}

Members.$inject = ['$scope', '$location', 'MembersService'];

App.
    component('members', {
        templateUrl: 'scripts/views/navigationviews/MembersViews/MembersList/MembersList.html',
        controller: Members, // com es diu la Classe
        controllerAs: "vm"
    });