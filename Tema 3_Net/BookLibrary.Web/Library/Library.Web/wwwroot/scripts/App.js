﻿var App = angular.module("LibraryApp", [
    'ngRoute',
    'ui.grid']);

App.config(function ($routeProvider, $locationProvider)
{
$routeProvider
.when("/Books", { template: "<books></books>"})    
.when("/Books/Detail/:id", { template: "<bookdetail></bookdetail>"})    
.when("/Books/Edit/:id", { template: "<bookedit></bookedit>"})    
.when("/Books/Add", { template: "<bookadd></bookadd>"})   

.when("/Members", { template: "<members></members>" })
.when("/Members/Detail/:id", { template: "<memberdetail></memberdetail>" })
.when("/Members/Edit/:id", { template: "<memberedit></memberedit>" })
.when("/Members/Add", { template: "<memberadd></memberadd>" })

.when("/Loans", { template: "<loans></loans>" })
.when("/Loans/Returns", { template: "<loanreturns></loanreturns>" })

$locationProvider.html5Mode(true);
});

App.ClientGlobals = new ClientGlobals();
