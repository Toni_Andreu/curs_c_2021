﻿class Loan {
    LoanDate;
    ReturnDate;

    BookCopyId;
    MemberId;

    constructor() {

    }

    Clone()
    {
        let output = new Loan();

        output.Id = this.Id;
        output.LoanDate = this.LoanDate;
        output.ReturnDate = this.ReturnDate;
        output.BookCopyId = this.BookCopyId;
        output.MemberId = this.MemberId;

        return output;
    }

}