﻿class Member
{
    Name;
    Surname1;
    Surname2;
    Dni;
    Email;
    ContactPhone;
    Pass;
    Status;

    constructor()
    {

    }

    Clone()
    {
        let output = new Member();

        output.Id = this.Id;
        output.Name = this.Name;
        output.Surname1 = this.Surname1;
        output.Surname2 = this.Surname2;
        output.Dni = this.Dni;
        output.Email = this.Email;
        output.ContactPhone = this.ContactPhone;
        output.Pass = this.Pass;
        output.Status = this.Status;

        return output;
    }
}