﻿class Book {
    Title;
    Synopsis;
    Author;
    Isbn;
    Edition;
    Year;
    Publisher;
    Description;
    Signature;

    constructor()
    {

    }
    
    Clone()
    {
        let output = new Book();

        output.Id = this.Id;
        output.Synopsis = this.Synopsis;
        output.Title = this.Title;
        output.Author = this.Author;
        output.Isbn = this.Isbn;
        output.Edition = this.Edition;
        output.Year = this.Year;
        output.Publisher = this.Publisher;
        output.Description = this.Description;
        output.Signature = this.Signature;

        return output;
    }

}