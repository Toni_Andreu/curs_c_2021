class BookCopiesService
{
    #http = null;
    static Items = new Map();

    constructor($http)
    {
        this.#http = $http;

        if(BooksService.Items.length == 0)
        {
            var book1 = new Book();
            book1.Id = "aasdfsadf456353sfasdfasdfasdf";
            book1.Edition = "12";
            book1.Title = "The Lord of the rings";
            book1.Author = "JRR Tolkien";
            book1.Year = 1954;

            var book2 = new Book();
            book2.Id = "ahhfdghdhdv55464642";
            book2.Edition = "4";
            book2.Title = "Dune";
            book2.Author = "Frank Herbert";
            book2.Year = 1965;
            

            BooksService.Items.push(book1);
            BooksService.Items.push(book2);
        }
    }

    GetAll()
    {
        var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error


    }

    Find(id)
    {
        let myPromise = new Promise(function(myResolve, myReject) 
        {
            for(var i in BooksService.Items)
            {
                var item = BooksService.Items[i];
                if(item.Id == id)
                    myResolve(item); 

                return;
            }
        });

        return myPromise;

        
    }
}


BookCopiesService.$inject = ['$http'];

App.service('BookCopiesService', BookCopiesService);