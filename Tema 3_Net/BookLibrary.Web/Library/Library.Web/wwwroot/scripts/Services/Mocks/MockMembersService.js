class MockMembersService
{
    //#http = null;
    //  Canviem Array per Map = millor
    /*    static Items = new Array();*/

    static Items = new Map();
    static ItemsByDNI = new Map();

    constructor()
    {
        //this.#http = $http;

        if(MockMembersService.Items.size == 0)
        {
            var member1 = new Member();
            member1.Id = Guid.NewGuid();
            member1.Name = "Pep";
            member1.Surname1 = "Mira";
            member1.Surname2 = "L�pez";
            member1.Email = "pep@apple.com";
            member1.Dni = "38412532D";
            member1.ContactPhone = "934556710";
            member1.Pass = "m1234";
            member1.Status = "";

            var member2 = new Member();
            member2.Id = Guid.NewGuid();
            member2.Name = "Maria";
            member2.Surname1 = "Mira";
            member2.Surname2 = "L�pez";
            member2.Email = "maria@icloud.com";
            member2.Dni = "58412532D";
            member2.ContactPhone = "934556711";
            member2.Pass = "l1234";
            member2.Status = "";

            // Si fos array
            //MockMembersService.Items.push(member1);
            //MockMembersService.Items.push(member2);

            MockMembersService.Items.set(member1.Id, member1);
            MockMembersService.Items.set(member2.Id, member2);

            MockMembersService.ItemsByDNI.set(member1.Dni, member1);
            MockMembersService.ItemsByDNI.set(member2.Dni, member2);

        }
    }
    Add(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var existingMember = MockMembersService.ItemsByDNI.get(item.Dni);
            if (Tools.IsNullOrUndefined(existingMember))
            {
                item.Id = Guid.NewGuid();
                MockMembersService.Items.set(item.Id, item);
                MockMembersService.ItemsByDNI.set(item.Dni, item);

                onSuccess(true);
            }
            else
            {
                onError("Ja existeix un Lector amb DNI:" + item.Dni);
            }
            
        });

        return myPromise;
    }

    Update(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var memberToUpdate = MockMembersService.Items.get(item.Id);
            if (!Tools.IsNullOrUndefined(memberToUpdate))
            {
                if (memberToUpdate.Dni != item.Dni)
                {
                    var existingDni = MockMembersService.ItemsByDNI.get(item.Dni);
                    if (Tools.IsNullOrUndefined(existingDni))
                    {
                        MockMembersService.ItemsByDNI.delete(item.Dni);
                        memberToUpdate.Dni = item.Dni;
                        MockMembersService.ItemsByDNI.set(item.Dni, item);
                    }
                    else
                    {
                        onError("Ja existeix un Lector amb DNI:" + item.Dni);
                        return;
                    }

                }
                   
                memberToUpdate.Name = item.Name;
                memberToUpdate.Surname1 = item.Surname1;
                memberToUpdate.Surname2 = item.Surname2;
                memberToUpdate.Email = item.Email;
                //memberToUpdate.Dni = item.Dni; No deixarem editar DNI, millor que s'esborri el Lector i crear un de nou, correcte
                memberToUpdate.ContactPhone = item.ContactPhone;
                memberToUpdate.Pass = item.Pass;
                memberToUpdate.Status = item.Status;

                onSuccess(true);
            }
            else
            {
                onError("No existeix cap Lector amb l'ID: " + item.Id);
            }
            
        });

        return myPromise;
    }

    GetAll()
    {
        /*var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error*/

        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = Array.from(MockMembersService.Items, ([name, value]) => value);
            onSuccess(output);
        });

        return myPromise;
    }
    
    Find(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = MockMembersService.Items.get(id);
            
            if(output !== null)
                onSuccess(output);
            else
                onError("No existeix cap Lector amb l'ID: " + id);
        });

        return myPromise;
    }

    Delete(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var itemToDelete = MockMembersService.Items.get(id);
            
            if (!Tools.IsNullOrUndefined(itemToDelete))
            {
                MockMembersService.Items.delete(id);
                onSuccess(true);
            }
            else
                onError("Error a l'esborrar: No existeix cap Lector amb aquest ID: " + id);
        });

        return myPromise;
    }
    }


//MockMembersService.$inject = ['$http'];

App.service('MembersService', MockMembersService);