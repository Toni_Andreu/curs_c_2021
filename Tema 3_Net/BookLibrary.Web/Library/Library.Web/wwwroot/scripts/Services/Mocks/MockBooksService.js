class MockBooksService
{
    //#http = null;
    //  Canviem Array per Map = millor
    /*    static Items = new Array();*/

    static Items = new Map();
    static ItemsByIsbn = new Map();
    #BookCopiesService = null;

    constructor(bookCopiesService)
    {
        //this.#http = $http;
        this.#BookCopiesService = bookCopiesService;

        if (MockBooksService.Items.size == 0)
        {
            var book1 = new Book();
            book1.Id = Guid.NewGuid();
            book1.Edition = "12";
            book1.Title = "The Lord of the rings";
            book1.Author = "JRR Tolkien";
            book1.Publisher = "Planeta";
            book1.Year = 1954;
            book1.Isbn = "9781617682131";

            var book2 = new Book();
            book2.Id = Guid.NewGuid();
            book2.Edition = "4";
            book2.Title = "Dune";
            book2.Author = "Frank Herbert";
            book2.Publisher = "Ed. 62";
            book2.Year = 1965;
            book2.Isbn = "9788466353779";

            // Si fos array
            //MockBooksService.Items.push(book1);
            //MockBooksService.Items.push(book2);

            MockBooksService.Items.set(book1.Id, book1);
            MockBooksService.Items.set(book2.Id, book2);
            MockBooksService.ItemsByIsbn.set(book1.Isbn, book1);
            MockBooksService.ItemsByIsbn.set(book2.Isbn, book2);
        }
    }
    
    Add(item, copies)
    {
        let self = this;

        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var existingBook = MockBooksService.ItemsByIsbn.get(item.Isbn);
            if (Tools.IsNullOrUndefined(existingBook))
            {
                item.Id = Guid.NewGuid();
                MockBooksService.Items.set(item.Id, item);
                MockBooksService.ItemsByIsbn.set(item.Isbn, item);

                self.#CreateBookCopies(item, copies, self.#BookCopiesService)
                .then(
                    (item) => onSuccess(true), 
                    (error) => onError(error));
            }
            else
            {
                onError("Ja existeix un libre amb l'Isbn:" + item.Isbn);
            }
            
        });

        return myPromise;
    }

    Update(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var bookToUpdate = MockBooksService.Items.get(item.Id);
            if (!Tools.IsNullOrUndefined(bookToUpdate))
            {
                if (bookToUpdate.Isbn != item.Isbn)
                {
                    var existingIsbn = MockBooksService.ItemsByIsbn.get(item.Isbn);
                    if (Tools.IsNullOrUndefined(existingIsbn))
                    {
                        MockBooksService.ItemsByIsbn.delete(item.Isbn);
                        bookToUpdate.Isbn = item.Isbn;
                        MockBooksService.ItemsByIsbn.set(item.Isbn, item);
                    }
                    else
                    {
                        onError("Ja existeix un libre amb l'Isbn:" + item.Isbn);
                        return;
                    }

                }
    
                bookToUpdate.Author = item.Author;
                bookToUpdate.Publisher = item.Publisher;
                bookToUpdate.Edition = item.Edition;
                bookToUpdate.Title = item.Title;
                //bookToUpdate.Isbn = item.Isbn;
                bookToUpdate.Year = item.Year; 
                bookToUpdate.Synopsis = item.Synopsis; 
                bookToUpdate.Description = item.Description; 
                bookToUpdate.Signature = item.Signature;                    

                onSuccess(true);
            }
            else
            {
                onError("No existeix cap Llibre amb l'ID: " + item.Id);
            }
            
        });

        return myPromise;
    }

    GetAll()
    {
        /*var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error*/

        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = Array.from(MockBooksService.Items, ([name, value]) => value);
            onSuccess(output);
        });

        return myPromise;
    }
    
    Find(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = MockBooksService.Items.get(id);
            
            if(output !== null)
                onSuccess(output);
            else
                onError("No existeix cap Llibre amb l'ID: " + id);
        });

        return myPromise;
    }

    Delete(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var itemToDelete = MockBooksService.Items.get(id);
            
            if (!Tools.IsNullOrUndefined(itemToDelete))
            {
                MockBooksService.Items.delete(id);
                onSuccess(true);
            }
            else
                onError("Error a l'esborrar: No existeix cap Llibre amb l'ID: " + id);
        });

        return myPromise;
    }

    #CreateBookCopies(book, copies, svc)
    {
        //let copies = 4;
        let items = [];
        for (let i = 0; i < copies; i++)
        {
            let bookCopy = new BookCopy();
            bookCopy.BookId = book.Id;
            bookCopy.BookCopyNum = i;
            bookCopy.Code = book.Title.substring(1, 3) + (i + 1);
            bookCopy.Avalaible = false;

            items.push(bookCopy);
        }
        
        return svc.AddRange(items);
    }
}


MockBooksService.$inject = ['BookCopiesService'];

App.service('BooksService', MockBooksService);