class MockLoansService
{
    //#http = null;
    //  Canviem Array per Map = millor
    /*    static Items = new Array();*/

    static Items = new Map();

    constructor()
    {
        //this.#http = $http;

        if(MockLoansService.Items.size == 99)
        {
            var loan1 = new Book();
            loan1.Id = Guid.NewGuid();
            loan1.Edition = "12";
            loan1.Title = "The Lord of the rings";
            loan1.Author = "JRR Tolkien";
            loan1.Publisher = "Planeta";
            loan1.Year = 1954;
            loan1.Isbn = "9781617682131";

            var loan2 = new Book();
            loan2.Id = Guid.NewGuid();
            loan2.Edition = "4";
            loan2.Title = "Dune";
            loan2.Author = "Frank Herbert";
            loan2.Publisher = "Ed. 62";
            loan2.Year = 1965;
            loan2.Isbn = "9788466353779";

            // Si fos array
            //MockLoansService.Items.push(loan1);
            //MockLoansService.Items.push(loan2);

            MockLoansService.Items.set(loan1.Id, loan1);
            MockLoansService.Items.set(loan2.Id, loan2);

        }
    }

    Add(item)
    {
        let self = this;
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var existingLoan = MockLoansService.Items.get(item.Id);
            if (Tools.IsNullOrUndefined(existingLoan))
            {
                if(self.ValidaLoansMember(item) == true)
                {
                    item.Id = Guid.NewGuid();
                    MockLoansService.Items.set(item.Id, item);
                    onSuccess(true);
                }
                else
                {
                    var err = "Aquest Lector actualment te 3 préstecs!! \nNo es pot continuar.";
                    alert(err);
                }
            }
            else
            {
                onError("Ja existeix un libre amb l'ID:" + item.Id);
            }
        });

        return myPromise;
    }

    ValidaLoansMember(item)
    {
        var limit = 3;
        var loansTotal= 0;
        
        MockLoansService.Items.forEach(
            (value, key, map) =>
            {
                if (item.Id == value.MemberId && value.ReturnDate == null)
                    loansTotal++;
            });   
        
        if (loansTotal >= limit)
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }

    Return(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var loanToUpdate = MockLoansService.Items.get(item.Id);
            if (!Tools.IsNullOrUndefined(loanToUpdate))
            {
                MockLoansService.Items.set(item.Id, item);
                    
                loanToUpdate.LoanDate = item.LoanDate;
                loanToUpdate.ReturnDate = item.ReturnDate;
                loanToUpdate.BookCopyId = item.BookCopyId;
                loanToUpdate.MemberId = item.MemberId;

                onSuccess(true);
            }
            else
            {
                onError("No existeix cap Prèstec amb l'ID: " + item.Id);
            }
            
        });

        return myPromise;
    }

    GetAll()
    {
        /*var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error*/

        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = Array.from(MockLoansService.Items, ([name, value]) => value);
            onSuccess(output);
        });
            var output = [];
            
            MockLoansService.Items.forEach(
                (value, key, map) =>
                {
                    if (memberId == value.MemberId && value.ReturnDate == null)
                        output.push(value);
                });
        return myPromise;
    }

    
    GetMembersLoans(memberId)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = [];
            MockLoansService.Items.forEach((loan, key, map) =>
            {
                if(loan.MemberId == memberId)
                    output.push(loan);
            });
            
            if(output.length !== 0)
                onSuccess(output);
            //else
                //onError("No existeix cap Prèstec d'aquest usuari.");
        });
        return myPromise;
    }

    GetMembersLoansReturned(memberId)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = [];
            MockLoansService.Items.forEach((loan, key, map) =>
            {
                if (memberId == value.MemberId && value.ReturnDate != null)
                    output.push(value);
            });
            
            if(output.length !== 0)
                onSuccess(output);
            else
                onError("No existeix cap Prèstec d'aquest usuari.");
        });
        return myPromise;
    }
    
    Find(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = MockLoansService.Items.get(id);
            
            if(output !== null)
                onSuccess(output);
            else
                onError("No existeix cap Prèstec amb l'ID: " + id);
        });

        return myPromise;
    }
}


//MockLoansService.$inject = ['$http'];

App.service('LoansService', MockLoansService);