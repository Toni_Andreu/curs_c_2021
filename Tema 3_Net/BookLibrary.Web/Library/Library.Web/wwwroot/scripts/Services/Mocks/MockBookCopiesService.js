class MockBookCopiesService
{
    //#http = null;
    //  Canviem Array per Map = millor
    /*    static Items = new Array();*/

    static Items = new Map();

    constructor()
    {
        //this.#http = $http;       
    }

    AddRange(items)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            for(let i in items)
            {
                let bookCopy = items[i];
                bookCopy.Id = Guid.NewGuid();
                MockBookCopiesService.Items.set(bookCopy.Id, bookCopy);
            }
            
            onSuccess(true);            
        });

        return myPromise;
    }

    Add(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var existingBook = MockBookCopiesService.ItemsByIsbn.get(item.Isbn);
            if (Tools.IsNullOrUndefined(existingBook))
            {
                item.Id = Guid.NewGuid();
                MockBookCopiesService.Items.set(item.Id, item);
                onSuccess(true);
            }
            else
            {
                onError("Ja existeix un libre amb l'Isbn:" + item.Isbn);
            }
            
        });

        return myPromise;
    }

    Update(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var bookToUpdate = MockBooksService.Items.get(item.Id);
            if (!Tools.IsNullOrUndefined(bookToUpdate))
            {
                if (bookToUpdate.Isbn != item.Isbn)
                {
                    var existingIsbn = MockBooksService.ItemsByIsbn.get(item.Isbn);
                    if (Tools.IsNullOrUndefined(existingIsbn))
                    {
                        MockBooksService.ItemsByIsbn.delete(item.Isbn);
                        bookToUpdate.Isbn = item.Isbn;
                        MockBooksService.ItemsByIsbn.set(item.Isbn, item);
                    }
                    else
                    {
                        onError("Ja existeix un libre amb l'Isbn:" + item.Isbn);
                        return;
                    }

                }
    
                bookToUpdate.Author = item.Author;
                bookToUpdate.Publisher = item.Publisher;
                bookToUpdate.Edition = item.Edition;
                bookToUpdate.Title = item.Title;
                //bookToUpdate.Isbn = item.Isbn;
                bookToUpdate.Year = item.Year;                    

                onSuccess(true);
            }
            else
            {
                onError("No existeix cap Llibre amb l'ID: " + item.Id);
            }
            
        });

        return myPromise;
    }

    GetAll()
    {
        /*var getReq = 
        {
            method: 'GET',            
            url: 'api/spaceships'            
        };

        this.Http(getReq).then(
            (response) => this.LoadShips(response.data.$values), // on success
            (error) => alert(error.statusText));                   // on error*/

        let myPromise = new Promise(function(onSuccess, onError) 
        {
            //lambda
            var output = Array.from(MockBookCopiesService.Items, ([name, value]) => value);
            onSuccess(output);
        });

        return myPromise;
    }
    
    Find(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = [];

            // forma anònima (semblant a )
            MockBookCopiesService.Items.forEach((bookCopy, key, map) =>
            {
                if( bookCopy.BookId ==id)
                //if(id == value.BookId && value.Avalaible == true)
                    output.push(bookCopy);
            });
            
            if(output.length !== 0)
                onSuccess(output);
            else
                onError("No existeix cap Copy per aquest llibre amb l'ID: " + id);
        });

        return myPromise;
    }


    Delete(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var itemToDelete = MockBookCopiesService.Items.get(id);
            
            if (!Tools.IsNullOrUndefined(itemToDelete))
            {
                MockBookCopiesService.Items.delete(id);
                onSuccess(true);
            }
            else
                onError("Error a l'esborrar: No existeix cap Llibre amb l'ID: " + id);
        });

        return myPromise;
    }
}


//MockBooksService.$inject = ['$http'];

App.service('BookCopiesService', MockBookCopiesService);