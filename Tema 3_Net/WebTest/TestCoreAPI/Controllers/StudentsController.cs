﻿using Microsoft.AspNetCore.Mvc;
using TestCoreAPI.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestCoreAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        // GET: api/<StudentsController>
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            var std1 = new Student()
            {
                Id = new Guid(),
                Name = "Pep",
                Email = "Pep@pep.com",
                Password = "1234"
            };
            var std2 = new Student()
            {
                Id = new Guid(),
                Name = "Maria",
                Email = "Maria@pep.com",
                Password = "1234"
            };
            return new List<Student>(std1 + std2);
        }

        // GET api/<StudentsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<StudentsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<StudentsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<StudentsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
