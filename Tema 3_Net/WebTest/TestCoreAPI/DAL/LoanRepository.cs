﻿using Library.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Library.UI.DAL
{
    public class LoansRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Loan> LoansList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public LoansRepository()
        {
            DbContext = new LibraryDbContext();
            DataLoans();
        }
        public List<Loan> GetAll()
        {
            return DbContext.Loans.ToList(); // llegeix la DB
        }

        public Loan Get(Guid id)
        {
            var output = DbContext.Loans.Find(id);
            return output;
        }

        public List<Loan> GetByDate(DateTime date)
        {
            var output = DbContext.Loans.Where(s => s.LoanDate == date).ToList();
            return output;
        }
        public List<Loan> GetByMemberId(Guid id)
        {
            var output = DbContext.Loans.Where(s => s.MemberId == id).ToList();
            return output;
        }

        public LoanValidationsTypes Add(Loan loan)
        {
            if (loan.Id != default(Guid))
            {
                return LoanValidationsTypes.IdNotEmpty;
            }

            // +++++  CORREGIR   ++++++++++++++++++

            //var stdWithSameTitle = GetByTitle(loan.Title);
            //if (stdWithSameTitle != null && loan.Id != stdWithSameTitle.Id)
            //{
            //    //MessageBox.Show(Loan.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return LoanValidationsTypes.LoanDuplicated;
            //}
            try
            {
                if (DbContext.Loans.All(s => s.Id != loan.Id)) // mes eficient
                {
                    loan.Id = Guid.NewGuid();
                    DbContext.Loans.Add(loan);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return LoanValidationsTypes.Ok;
        }
        public LoanValidationsTypes Delete(Guid id)
        {
            try
            {
                var loan = DbContext.Loans.Find(id);
                if (loan != null)
                {
                    DbContext.Loans.Remove(loan);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return LoanValidationsTypes.Ok;
        }
        public LoanValidationsTypes Edit(Loan loan)
        {
            if (loan.Id == default(Guid))
            {
                //MessageBox.Show(Loan.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return LoanValidationsTypes.IdEmpty;
            }
            if (DbContext.Loans.All(s => s.Id != loan.Id))
            {
                //MessageBox.Show(Loan.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return LoanValidationsTypes.LoanNotFound;
            }
            try
            {
                var existingStudent = DbContext.Loans.Find(loan.Id);
                existingStudent.ApplyChanges(loan);

                DbContext.Loans.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return LoanValidationsTypes.Ok;
        }

        #region DATA
        public void DataLoans()
        {
            if (DbContext.Loans.Count() == 0)
            {
                //var loan1 = new Loan("Maria", "Cardeu", "Molt", "37782599F", "maria@icloud.com", "698784569", "1234", "Librarian");
                //DbContext.Loans.Add(loan1);
                

                //DbContext.SaveChanges();
            }
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}
