﻿using Library.Lib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Library.UI.DAL
{
    public class BooksRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Book> BooksList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public BooksRepository()
        {
            DbContext = new LibraryDbContext();
            LoadDemoData();
        }
        public List<Book> GetAll()
        {
            return DbContext.Books.ToList(); // llegeix la DB
        }

        public Book Get(Guid id)
        {
            var output = DbContext.Books
                .Include(x => x.BookCopies)
                .ThenInclude(x => x.Loans)
                .Include(x => x)
                .AsQueryable()
                .SingleOrDefault(x => x.Id == id);

            return output;
        }

        public List<Book> GetByTitle(string title)
        {
            var output = DbContext.Books.Where(s => s.Title == title).ToList();
            return output;
        }
        public List<Book> GetByAuthor(string author)
        {
            var output = DbContext.Books.Where(s => s.Author == author).ToList();
            return output;
        }

        public BookValidationsTypes Add(Book book)
        {
            if (book.Id != default(Guid))
            {
                return BookValidationsTypes.IdNotEmpty;
            }

            // +++++  CORREGIR   ++++++++++++++++++

            //var stdWithSameTitle = GetByTitle(book.Title);
            //if (stdWithSameTitle != null && book.Id != stdWithSameTitle.Id)
            //{
            //    //MessageBox.Show.Show(Book.ErrExisteix, "Error", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Error);
            //    return BookValidationsTypes.BookDuplicated;
            //}
            try
            {
                if (DbContext.Books.All(s => s.Id != book.Id)) // mes eficient
                {
                    book.Id = Guid.NewGuid();
                    DbContext.Books.Add(book);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show.Show(error, "Error!", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Warning);
            }
            return BookValidationsTypes.Ok;
        }
        public BookValidationsTypes Delete(Guid id)
        {
            try
            {
                var book = DbContext.Books.Find(id);
                if (book != null)
                {
                    DbContext.Books.Remove(book);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show.Show(error, "Error!", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Warning);
            }
            return BookValidationsTypes.Ok;
        }
        public BookValidationsTypes Edit(Book book)
        {
            if (book.Id == default(Guid))
            {
                //MessageBox.Show.Show(Book.ErrBuidID, "Error", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Error);
                return BookValidationsTypes.IdEmpty;
            }
            if (DbContext.Books.All(s => s.Id != book.Id))
            {
                //MessageBox.Show.Show(Book.ErrNoExisteix, "Error", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Error);
                return BookValidationsTypes.BookNotFound;
            }

            // +++++  CORREGIR   ++++++++++++++++++

            //var stdWithSameTitle = GetByTitle(book.Title);
            //if (stdWithSameTitle != null && book.Id != stdWithSameTitle.Id)
            //{
            //    // hay dos estudiantes distintos con mismo dni
            //    //MessageBox.Show.Show(Book.ErrExisteix, "Error", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Error);
            //    return BookValidationsTypes.BookDuplicated;
            //}
            try
            {
                var existingStudent = DbContext.Books.Find(book.Id);
                existingStudent.ApplyChanges(book);

                DbContext.Books.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //MessageBox.Show.Show(error, "Error!", //MessageBox.ShowButton.OK, //MessageBox.ShowImage.Warning);
            }
            return BookValidationsTypes.Ok;
        }

        #region DATA
        public void LoadDemoData()
        {
            if (DbContext.Books.Count() == 0)
            {
                var book1 = new Book("Espía de Dios", "S1", "Juan Gómez-Jurado", "9788413142449", "2a", 2006, "Roca Editorial", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book1);
                var book2 = new Book("Contrato con Dios", "S2", "Juan Gómez-Jurado", "9788413142449", "2a", 2007, "Planeta", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book2);
                var book3 = new Book("El emblema del traidor", "S3", "Juan Gómez-Jurado", "9788413142449", "2a", 2008, "Plaza & Janés", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book3);
                var book4 = new Book("La leyenda del ladrón", "S4", "Juan Gómez-Jurado", "9788413142449", "2a", 2012, "Planeta", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book4);
                var book5 = new Book("El paciente", "S5", "Juan Gómez-Jurado", "9788413142449", "2a", 2014, "Planeta", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book5);
                var book6 = new Book("La historia secreta del señor White", "S6", "Juan Gómez-Jurado", "9788413142449", "2a", 2015, "Planeta", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book6);
                var book7 = new Book("Cicatriz ", "S7", "Juan Gómez-Jurado", "9788413142449", "2a", 2015, "Ediciones B", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book7);
                var book8 = new Book("Reina roja", "S8", "Juan Gómez-Jurado", "9788413142449", "2a", 2018, "Ediciones B", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book8);
                var book9 = new Book("Loba negra", "S9", "Juan Gómez-Jurado", "9788413142449", "2a", 2019, "Ediciones B", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book9);
                var book10 = new Book("Rey blanco", "S10", "Juan Gómez-Jurado", "9788413142449", "2a", 2020, "Ediciones B", "574 pàgines- 19 cm", "N Gom");
                DbContext.Books.Add(book10);

                DbContext.SaveChanges();

                using (var repoCopies = new BookCopiesRepository())
                {
                    repoCopies.LoadDemoData();
                }
               
            }
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}
