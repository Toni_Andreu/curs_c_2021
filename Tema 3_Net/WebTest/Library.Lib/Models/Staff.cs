﻿using Common.Lib.Authentification;
using System;

namespace Library.Lib.Models
{
    public class Staff : User
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public string ContactPhone { get; set; }
        public string Pass { get; set; }
        public string? Category { get; set; }


        public Staff()
        {

        }

        //constructor per carrega
        public Staff(string name, string surname1, string surname2, string dni, string email, string contactPhone, string pass, string category)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Surname1 = surname1;
            this.Surname2 = surname2;
            this.Dni = dni;
            this.Email = email;
            this.ContactPhone = contactPhone;
            this.Pass = pass;
            this.Category = category;
        }

        public Staff Clone()
        {
            return Clone<Staff>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Staff;

            output.Name = this.Name;
            output.Surname1 = this.Surname1;
            output.Surname2 = this.Surname2;
            output.Dni = this.Dni;
            output.Email = this.Email;
            output.Dni = this.ContactPhone;
            output.Pass = this.Pass;
            output.Category = this.Category;

            return output as T;
        }


        public void ApplyChanges(Staff customer)
        {
            ApplyChanges<Staff>(customer);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var staff = entity as Staff;

            if (staff == null)
                throw new Exception("entity is not of type staff");

            this.Name = staff.Name;
            this.Surname1 = staff.Surname1;
            this.Surname2 = staff.Surname2;
            this.Dni = staff.Dni;
            this.Email = staff.Email;
            this.ContactPhone = staff.ContactPhone;
            this.Pass = staff.Pass;
            this.Category = staff.Category;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            if (nom.Length != 9)
            {
                return true;
            }
            return false;
        }

        // Dni existeix
        public static bool DniExist(string nom)
        {
            //var stdWithSameDni = StudentsRepositor.GetByDni(customer.Dni);
            //if (stdWithSameDni != null && customer.Id = stdWithSameDni.Id)
            //{
            //    return false;
            //}
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                // si el nombre está vació o compuesto de espacios
                return true;
            }
            return false;
        }

        #endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "El Nom de l'Alumne no pot estar buid!";
        public const string ErrBuidSurname1 = "El Cognom de l'Alumne no pot estar buid!";
        // DNI Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte\n" +
            "(xxxxxxxxL)";
        // DNI existent
        public const string ErrExisteix = "Aquest Dni ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix el DNI:";
        internal static readonly object Staffs;
        #endregion
    }
    public enum StaffValidationsTypes
    {
        Ok,
        WrongDniFormat,
        DniDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        StaffNotFound
    }
    public enum UserCategoryTypes
    {
        Librarian,
        Manager,
        Director
    }
}
