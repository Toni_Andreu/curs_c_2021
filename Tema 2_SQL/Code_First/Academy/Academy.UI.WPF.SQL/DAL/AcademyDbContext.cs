﻿using Academy.Lib.Models;
using Microsoft.EntityFrameworkCore;

namespace Academy.UI.WPF.SQL.DAL
{
    public class AcademyDbContext : DbContext
    {
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public AcademyDbContext() : base()
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
                modelBuilder.Entity<Student>().ToTable("Students");
                modelBuilder.Entity<Subject>().ToTable("Subjects");
                modelBuilder.Entity<Enrollment>().ToTable("Enrollments");


                modelBuilder.Entity<Enrollment>()
                    .HasOne(e => e.Student)
                    .WithMany(s => s.Enrollments)
                    .HasForeignKey(e => e.StudentId);

                modelBuilder.Entity<Enrollment>()
                    .HasOne(e => e.Subject)
                    .WithMany(s => s.Enrollments)
                    .HasForeignKey(e => e.SubjectId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                // SQL = LT2BS28033\CURSO
        {
            optionsBuilder.UseSqlServer(@"server=LT2BS28033\CURSO;Database=Academy;Trusted_Connection = true; MultipleActiveResultSets=true");
        }


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseMySQL("server=localhost;database=school;user=schooluser;password=1234");

        //    optionsBuilder
        //        .UseMySql(connectionString: @"server=localhost;database=school;uid=schooluser;password=1234;",
        //        new MySqlServerVersion(new Version(8, 0, 23)));
        //}


    }
}


