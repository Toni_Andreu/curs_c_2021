﻿using Academy.Lib.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Academy.UI.MySQL.DAL
{
    public class AcademyDbContextMySql : DbContext
    {
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public AcademyDbContextMySql() : base()
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Subject>().ToTable("Subjects");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollments");


            modelBuilder.Entity<Enrollment>()
                .HasOne(e => e.Student)
                .WithMany(s => s.Enrollments)
                .HasForeignKey(e => e.StudentId);

            modelBuilder.Entity<Enrollment>()
                .HasOne(e => e.Subject)
                .WithMany(s => s.Enrollments)
                .HasForeignKey(e => e.SubjectId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseMySql(connectionString: @"server=localhost;database=school;uid=schooluser;password=1234;",
                new MySqlServerVersion(new Version(8, 0, 23)));
        }


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //optionsBuilder.UseMySQL("server=localhost;database=school;user=schooluser;password=1234");

        //    optionsBuilder
        //        .UseMySql(connectionString: @"server=localhost;database=school;uid=schooluser;password=1234;",
        //        new MySqlServerVersion(new Version(8, 0, 23)));
        //}



    }
}

