
namespace Academy.Lib.Models
{
    using School.Lib.Models;
    using System;
    using System.Collections.Generic;
    
    public partial class Enrollment : Entity
    {
        public Guid StudentId { get; set; }
        public Student Student { get; set; }
        public Guid SubjectId { get; set; }
        public virtual Subject Subject { get; set; }

        public DateTime Date { get; set; }
    }
}
