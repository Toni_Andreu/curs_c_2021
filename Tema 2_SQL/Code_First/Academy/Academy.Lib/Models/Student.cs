
namespace Academy.Lib.Models
{
    using School.Lib.Models;
    using System;
    using System.Collections.Generic;
    
    public partial class Student : Entity
    {
        public string name { get; set; }
        public string email { get; set; }
        public string dni { get; set; }
    
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
