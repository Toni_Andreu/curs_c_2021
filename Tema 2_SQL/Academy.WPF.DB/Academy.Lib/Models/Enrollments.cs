﻿using System;

namespace Academy.Lib.Models
{
    public class Enrollment : Entity
    {
        public DateTime Date { get; set; }
        public Guid SubjectId { get; set; }
        public Guid StudentId { get; set; }
        public Student Student { get; set; }
        public Subject Subject { get; set; }


        public Enrollment()
        {

        }
    }
}
