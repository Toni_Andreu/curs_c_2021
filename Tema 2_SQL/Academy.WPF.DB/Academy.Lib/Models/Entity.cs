﻿using System;

namespace Academy.Lib.Models
{
    public class Entity
    {
        public Guid Id { get; set; }

        public virtual T Clone<T>() where T : Entity, new()
        {
            var output = new T();
            output.Id = Id;
            return output;
        }

        public virtual void ApplyChanges<T>(T entity) where T : Entity
        {
            //No l'usem: no vol editar ID's!
        }
    }
}
