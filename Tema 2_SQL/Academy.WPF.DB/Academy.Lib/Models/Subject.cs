﻿using System;
using System.Collections.Generic;

namespace Academy.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string Teacher { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }
        public virtual List<Enrollment> Enrollments { get; set; }

        //constructors
        public Subject()
        {

        }
        public Subject(string name, string teacher, string code)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Teacher = teacher;
            this.Code = code;
        }
        public Subject Clone()
        {
            return Clone<Subject>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Subject;

            output.Name = this.Name;
            output.Teacher = this.Teacher;
            output.Code = this.Code;

            return output as T;
        }


        public void ApplyChanges(Subject subject)
        {
            ApplyChanges<Subject>(subject);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var subject = entity as Subject;

            if (subject == null)
                throw new Exception("entity is not of type Subject");

            this.Name = subject.Name;
            this.Teacher = subject.Teacher;
            this.Code = subject.Code;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom))
            {
                return true;
            }
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                // si el nombre está vació o compuesto de espacios
                return true;
            }
            return false;
        }

        #endregion

        #region CONSTANTS
        // ID empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidSubject = "El nom de l'asignatura no pot estar buid!";
        public const string ErrBuidTeacher = "El nom del Professor no pot estar buid!";
        public const string ErrBuidCode = "El Codi de l'asignatura no pot estar buid!";

        // Nom NO existent
        public const string ErrNoExisteix = "No existeix cap Asignatura amb aquest nom:";
        // Nom existent
        public const string ErrExisteix = "Ja existeix una Asignatura amb aquest nom!";
        internal static readonly object Subjects;
        #endregion
    }
    public enum SubjectValidationsTypes
    {
        Ok,
        SubjectDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        SubjectNotFound
    }
}