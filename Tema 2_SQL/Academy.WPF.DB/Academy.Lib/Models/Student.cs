﻿using System;
using System.Collections.Generic;

namespace Academy.Lib.Models
{
    public class Student : Entity
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }
        public virtual List<Enrollment> Enrollments { get; set; }
        //constructors 
        public Student()
        {

        }

        //constructor per carrega
        public Student(string name, string surname1, string surname2, string dni, string email)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Surname1 = surname1;
            this.Surname2 = surname2;
            this.Dni = dni;
            this.Email = email;
        }

        public Student Clone()
        {
            return Clone<Student>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Student;

            output.Name = this.Name;
            output.Surname1 = this.Surname1;
            output.Surname2 = this.Surname2;
            output.Dni = this.Dni;
            output.Email = this.Email;

            return output as T;
        }


        public void ApplyChanges(Student student)
        {
            ApplyChanges<Student>(student);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var student = entity as Student;

            if (student == null)
                throw new Exception("entity is not of type student");

            this.Name = student.Name;
            this.Surname1 = student.Surname1;
            this.Surname2 = student.Surname2;
            this.Dni = student.Dni;
            this.Email = student.Email;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom))
            {
                return true;
            }
            return false;
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            if (nom.Length != 9)
            {
                return true;
            }
            return false;
        }

        // Dni existeix
        public static bool DniExist(string nom)
        {
            //var stdWithSameDni = StudentsRepositor.GetByDni(student.Dni);
            //if (stdWithSameDni != null && student.Id = stdWithSameDni.Id)
            //{
            //    return false;
            //}
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                // si el nombre está vació o compuesto de espacios
                return true;
            }
            return false;
        }

        #endregion
    
        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "El Nom de l'Alumne no pot estar buid!";
        public const string ErrBuidSurname1 = "El Cognom de l'Alumne no pot estar buid!";
        // DNI Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte\n" +
            "(xxxxxxxxL)";
        // DNI existent
        public const string ErrExisteix = "Aquest Dni ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix el DNI:";
        internal static readonly object Students;
        #endregion
    }
    public enum StudentValidationsTypes
    {
        Ok,
        WrongDniFormat,
        DniDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        StudentNotFound
    }
}
