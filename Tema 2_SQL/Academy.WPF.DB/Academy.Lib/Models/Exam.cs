﻿using System;

namespace Academy.Lib.Models
{
    public class Exam : Entity
    {
        public string StudentDni { get; set; }
        public string SubjectName { get; set; }
        public double Mark { get; set; }
        public DateTime TimeStamp { get; set; }
        public Guid StudentId { get; set; }
        public Guid SubjectId { get; set; }
        public Student Student { get; set; }
        public Subject Subject { get; set; }


        //constructors
        public Exam(string studentDni, string subject, double mark, DateTime timeStamp)
        {
            this.Id = Guid.NewGuid();
            this.StudentDni = studentDni;
            this.SubjectName = subject;
            this.Mark = mark;
            this.TimeStamp = timeStamp;
        }
        public Exam()
        {

        }
        public Exam Clone()
        {
            return Clone<Exam>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Exam;

            output.StudentDni = this.StudentDni;
            output.SubjectName = this.SubjectName;
            output.Mark = this.Mark;
            output.TimeStamp = this.TimeStamp;

            return output as T;
        }


        public void ApplyChanges(Exam exam)
        {
            ApplyChanges<Exam>(exam);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var exam = entity as Exam;

            if (exam == null)
                throw new Exception("entity is not of type Exam!");

            this.StudentDni = exam.StudentDni;
            this.SubjectName = exam.SubjectName;
            this.Mark = exam.Mark;
            this.TimeStamp = exam.TimeStamp;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom))
            {
                return true;
            }
            return false;
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            if (nom.Length != 9)
            {
                return true;
            }
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }
        // Nota en rang 0-10.0
        public static bool ValidateRang(double mark)
        {
            return (0.0 <= mark) && (mark <= 10.0);
        }

        #endregion

        #region CONSTANTS
        // ID empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        //Subject not empty
        public const string ErrBuidSubject = "El nom de l'Asignatura no pot estar buid!";
        // Nom NO existent
        public const string ErrNoExisteix = "No existeix cap Asignatura amb aquest nom:";
        // no existeix
        public const string ErrSubjectDniNoExist = "No existeix cap Exàmen de l'Asignatura per aquest alumne!";
        // JA existeix
        public const string ErrSubjectExist = "Ja existeix un exàmen per aquest Alumne i Asignatura:";
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // Nom Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte";
        // Nom existent
        //public const string ErrNoExisteix = "El dni no pot estar buid!";
        // Mark not empty
        public const string ErrBuidMark = "La Nota no pot estar buida!";
        // Mark 0 - 10.0
        public const string ErrMarkRang = "La Nota ha de tenir un valor de 0 a 10.00!";

        #endregion
    }
    public enum ExamValidationsTypes
    {
        Ok,
        WrongDniFormat,
        DniDuplicated,
        WrongNameFormat,
        WrongMarkFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        ExamNotFound
    }
}