﻿using Academy.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Academy.UI.WPF.DB.DAL
{
    public class SubjectsRepository : IDisposable
    {
        public AcademyDbContext DbContext { get; set; }

        public List<Subject> SubjectsList
        {
            get
            {
                return GetAll();
            }
        }
        public SubjectsRepository()
        {
            DbContext = new AcademyDbContext();
            DataSubject();
        }
        public List<Subject> GetAll()
        {
            return DbContext.Subjects.ToList(); // llegeix la DB
        }

        public Subject Get(Guid id)
        {
            var output = DbContext.Subjects.Find(id);
            return output;
        }
        public List<Subject> GetByCode(string code)
        {
            var output = DbContext.Subjects.Where(s => s.Code == code).ToList();
            return output;
        }
        public List<Subject> GetBySubject(string name)
        {
            var output = DbContext.Subjects.Where(s => s.Name == name).ToList();
            return output;
        }
        public List<Subject> GetByTeacher(string teacher)
        {
            var output = DbContext.Subjects.Where(s => s.Teacher == teacher).ToList();
            return output;
        }

        public SubjectValidationsTypes Add(Subject subject)
        {
            if (subject.Id != default(Guid))
            {
                return SubjectValidationsTypes.IdNotEmpty;
            }
            /////    +++++++  REVISAR +++++++++++++++++
            //var subWithSameName = GetBySubject(subject.Name);
            //if (subWithSameName != null && subject.Id != subWithSameName.Id)
            //{
            //    // 2 asignatures amb el mateix nom??
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return SubjectValidationsTypes.SubjectDuplicated;
            //}
            try
            {
                if (DbContext.Students.All(s => s.Id != subject.Id)) // más eficiente
                {
                    subject.Id = Guid.NewGuid();
                    DbContext.Subjects.Add(subject);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return SubjectValidationsTypes.Ok;
        }
        public SubjectValidationsTypes Delete(Guid id)
        {
            try
            {
                var subject = DbContext.Subjects.Find(id);
                if (subject != null)
                {
                    DbContext.Subjects.Remove(subject);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return SubjectValidationsTypes.Ok;
        }

        public SubjectValidationsTypes Edit(Subject subject)
        {
            if (subject.Id == default(Guid))
            {
                MessageBox.Show(Subject.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return SubjectValidationsTypes.IdEmpty;
            }
            if (DbContext.Subjects.All(s => s.Id != subject.Id))
            {
                MessageBox.Show(Subject.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return SubjectValidationsTypes.SubjectNotFound;
            }

            /////    +++++++  REVISAR +++++++++++++++++
            //var subWithSameName = GetBySubject(subject.Name);
            //if (subWithSameName != null && subject.Id != subWithSameName.Id)
            //{
            //    // 2 asignatures amb el mateix nom??
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return SubjectValidationsTypes.SubjectDuplicated;
            //}
            try
            {
                var existingSubject = DbContext.Subjects.Find(subject.Id);
                existingSubject.ApplyChanges(subject);

                DbContext.Subjects.Update(existingSubject);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return SubjectValidationsTypes.Ok;
        }

        #region DATA
        public void DataSubject()
        {
            if (DbContext.Subjects.Count() == 0)
            {
                var subject1 = new Subject("C Sharp", "José Freire", "CS1");
                DbContext.Subjects.Add(subject1);
                var subject2 = new Subject("Ciberseguretat", "Ana Segura", "CIB1");
                DbContext.Subjects.Add(subject2);
                var subject3 = new Subject("Blockchain", "Carles Santamaria", "BL1");
                DbContext.Subjects.Add(subject3);
                var subject4 = new Subject("Flutter & Dart", "Pep Marquès", "F&D2");
                DbContext.Subjects.Add(subject4);
                var subject5 = new Subject("SQL", "Ângel López", "SQL5");
                DbContext.Subjects.Add(subject5);
                var subject6 = new Subject("Python", "Carme Pla", "Pyt6");
                DbContext.Subjects.Add(subject6);
                
                DbContext.SaveChanges();
            }
        }

        #endregion

        public void Dispose()
        {

        }

    }
}
