﻿using Academy.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Academy.UI.WPF.DB.DAL
{
    public class StudentsRepository : IDisposable
    {
        public AcademyDbContext DbContext { get; set; }

        //private Dictionary<string, Student> Students
        //{
        //    get
        //    {
        //        if (_students == null)
        //        {
        //            _students = new Dictionary<string, Student>();
        //            DataStudent();
        //        }
        //        return _students;
        //    }
        //}
        //Dictionary<string, Student> _students;

        public List<Student> StudentsList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public StudentsRepository()
        {
            DbContext = new AcademyDbContext();
            DataStudent();
        }
        public List<Student> GetAll()
        {
            //return Students.Values.ToList(); = llegia el Dictionary
            return DbContext.Students.ToList(); // llegeix la DB
        }

        public Student Get(Guid id)
        {
            // si existe un Student con es id en la DB me lo devuelve
            //var student = DbContext.Students.FirstOrDefault(x => x.Id == id);
            //para el id es mejor el find
            var output = DbContext.Students.Find(id);
            return output;
        }
        public Student GetByDni(string dni)
        {
            //foreach (var item in Students)
            //{
            //    var student = item.Value;
            //    if (student.Dni == dni)
            //        return student;
            //}
            //return default(Student);
            var output = DbContext.Students.FirstOrDefault(s => s.Dni == dni);
            return output;
        }
        public List<Student> GetByName(string name)
        {
            //var output = new List<Student>();
            //foreach (var item in Students)
            //{
            //    var student = item.Value;

            //    if (student.Name == name)
            //        output.Add(student);
            //}
            //// devolvemos la lista
            //return output;

            var output = DbContext.Students.Where(s => s.Name == name).ToList();
            return output;
        }

        public StudentValidationsTypes Add(Student student)
        {
            if (student.Id != default(Guid))
            {
                return StudentValidationsTypes.IdNotEmpty;
            }
            var stdWithSameDni = GetByDni(student.Dni);
            if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
            {
                MessageBox.Show(Student.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StudentValidationsTypes.DniDuplicated;
            }
            try
            {
                if (DbContext.Students.All(s => s.Id != student.Id)) // más eficiente
                {
                    student.Id = Guid.NewGuid();
                    DbContext.Students.Add(student);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StudentValidationsTypes.Ok;
        }
        public StudentValidationsTypes Delete(Guid id)
        {
            try
            {
                var student = DbContext.Students.Find(id);
                if (student != null)
                {
                    DbContext.Students.Remove(student);
                    DbContext.SaveChanges();
                } 
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StudentValidationsTypes.Ok;
        }
        public StudentValidationsTypes Edit(Student student)
        {
            if (student.Id == default(Guid))
            {
                MessageBox.Show(Student.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StudentValidationsTypes.IdEmpty;
            }
            if (DbContext.Students.All(s => s.Id != student.Id))
            {
                MessageBox.Show(Student.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StudentValidationsTypes.StudentNotFound;
            }
            // comprobamos que no haya otro alumno diferente
            // con el mismo dni

            var stdWithSameDni = GetByDni(student.Dni);
            if (stdWithSameDni != null && student.Id != stdWithSameDni.Id)
            {
                // hay dos estudiantes distintos con mismo dni
                MessageBox.Show(Student.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StudentValidationsTypes.DniDuplicated;
            }
            try
            {
                var existingStudent = DbContext.Students.Find(student.Id);
                existingStudent.ApplyChanges(student);

                DbContext.Students.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StudentValidationsTypes.Ok;
        }

        #region DATA
        public void DataStudent()
        {
            if (DbContext.Students.Count() == 0)
            {
                var student1 = new Student("Pep", "Mira", "López", "38412532D", "pep@apple.com");
                DbContext.Students.Add(student1);
                var student2 = new Student("Maria", "Mira", "López", "58412532D", "maria@apple.com");
                DbContext.Students.Add(student2);
                var student3 = new Student("Carles", "Camps", "Rodriguez", "12345678A", "carles@apple.com");
                DbContext.Students.Add(student3);
                var student4 = new Student("Gemma", "Jimenez", "Sanchez", "23456789A", "gemma@apple.com");
                DbContext.Students.Add(student4);
                var student5 = new Student("Victor", "Martínez", "López", "18412532D", "victor@apple.com");
                DbContext.Students.Add(student5);
                var student6 = new Student("Anna", "Vila", "Jimenez", "28412532D", "anna@apple.com");
                DbContext.Students.Add(student6);
                var student7 = new Student("Cèlia", "Mira", "Martinez", "48412532D", "celia@apple.com");
                DbContext.Students.Add(student7);
                var student8 = new Student("Carla", "Saladie", "López", "68412532D", "carla@apple.com");
                DbContext.Students.Add(student8);
                var student9 = new Student("Joan", "Vermell", "López", "78412532D", "joan@apple.com");
                DbContext.Students.Add(student9);
                var student10 = new Student("Raquel", "Mirabet", "Sladie", "88412532D", "raquel@apple.com");
                DbContext.Students.Add(student10);
                //_students.Add(student10.Dni, student10);

                DbContext.SaveChanges();
            }
        }

        #endregion

        public void Dispose()
        {

        }

    }
}
