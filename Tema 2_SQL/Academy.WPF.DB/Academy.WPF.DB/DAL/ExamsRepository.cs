﻿using Academy.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Academy.UI.WPF.DB.DAL
{
    public class ExamsRepository : IDisposable
    {
        public AcademyDbContext DbContext { get; set; }

        public List<Exam> ExamsList
        {
            get
            {
                return GetAll();
            }
        }
        public ExamsRepository()
        {
            DbContext = new AcademyDbContext();
            DataExam();
        }
        public List<Exam> GetAll()
        {
            return DbContext.Exams.ToList();
        }
        public Exam Get(Guid id)
        {
            var output = DbContext.Exams.Find(id);
            return output;
        }
        public Exam GetByDni(string dni)
        {
            var output = DbContext.Exams.FirstOrDefault(s => s.StudentDni == dni);
            return output;
        }
        public List<Exam> GetBySubject(string subject)
        {
            var output = DbContext.Exams.Where(s => s.SubjectName == subject).ToList();
            return output;
        }
        public List<Exam> GetByTimestamp(DateTime timeStamp)
        {
            var output = DbContext.Exams.Where(s => s.TimeStamp == timeStamp).ToList();
            return output;
        }

        public ExamValidationsTypes Add(Exam exam)
        {
            if (exam.Id != default(Guid))
            {
                return ExamValidationsTypes.IdNotEmpty;
            }
            /////    +++++++  REVISAR +++++++++++++++++
            //var subWithSameName = GetBySubject(subject.Name);
            //if (subWithSameName != null && subject.Id != subWithSameName.Id)
            //{
            //    // 2 asignatures amb el mateix nom??
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return SubjectValidationsTypes.SubjectDuplicated;
            //}
            try
            {
                if (DbContext.Exams.All(s => s.Id != exam.Id)) // más eficiente
                {
                    exam.Id = Guid.NewGuid();
                    DbContext.Exams.Add(exam);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return ExamValidationsTypes.Ok;
        }
        public ExamValidationsTypes Delete(Guid id)
        {
            try
            {
                var exam = DbContext.Exams.Find(id);
                if (exam != null)
                {
                    DbContext.Exams.Remove(exam);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return ExamValidationsTypes.Ok;
        }

        public ExamValidationsTypes Edit(Exam exam)
        {
            if (exam.Id == default(Guid))
            {
                MessageBox.Show(Exam.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return ExamValidationsTypes.IdEmpty;
            }
            if (DbContext.Exams.All(s => s.Id != exam.Id))
            {
                MessageBox.Show(Exam.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return ExamValidationsTypes.ExamNotFound;
            }

            /////    +++++++  REVISAR +++++++++++++++++
            //var subWithSameName = GetBySubject(subject.Name);
            //if (subWithSameName != null && subject.Id != subWithSameName.Id)
            //{
            //    // 2 asignatures amb el mateix nom??
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return SubjectValidationsTypes.SubjectDuplicated;
            //}
            try
            {
                var existingExam = DbContext.Exams.Find(exam.Id);
                existingExam.ApplyChanges(exam);

                DbContext.Exams.Update(existingExam);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return ExamValidationsTypes.Ok;
        }

        #region DATA
        public void DataExam() 
        {
            if (!DbContext.Exams.Any())
            {
                //DateTime timestamp = DateTime.Now;
                //var exam1 = new Exam("12345678A", "Python", 6.2, timestamp);
                //DbContext.Exams.Add(exam1);
                //var exam2 = new Exam("28412532D", "Python", 6.9, timestamp);
                //DbContext.Exams.Add(exam2);
                //var exam3 = new Exam("68412532D", "Python", 8.4, timestamp);
                //DbContext.Exams.Add(exam3);
                //var exam4 = new Exam("48412532D", "Python", 2.1, timestamp);
                //DbContext.Exams.Add(exam4);
                //var exam5 = new Exam("38412532D", "Python", 7.6, timestamp);
                //DbContext.Exams.Add(exam5);
                //var exam6 = new Exam("58412532D", "Python", 4.2, timestamp);
                //DbContext.Exams.Add(exam6);
                //var exam7 = new Exam("12345678A", "SQL", 6.2, timestamp);
                //DbContext.Exams.Add(exam7);
                //var exam8 = new Exam("28412532D", "SQL", 6.9, timestamp);
                //DbContext.Exams.Add(exam8);
                //var exam9 = new Exam("68412532D", "SQL", 8.4, timestamp);
                //DbContext.Exams.Add(exam9);
                //var exam10 = new Exam("48412532D", "SQL", 3.9, timestamp);
                //DbContext.Exams.Add(exam10);
                //var exam11 = new Exam("38412532D", "SQL", 7.6, timestamp);
                //DbContext.Exams.Add(exam11);
                //var exam12 = new Exam("58412532D", "SQL", 4.2, timestamp);
                //DbContext.Exams.Add(exam12);

                DbContext.SaveChanges();
            }
        }
        #endregion

        public void Dispose()
        {
        }
    }
}
