﻿using Academy.Lib.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Academy.UI.WPF.DB.DAL
{
    public class AcademyDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public AcademyDbContext() : base()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Subject>().ToTable("Subjects");
            modelBuilder.Entity<Exam>().ToTable("Exams");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollments");

            modelBuilder.Entity<Exam>()
                .HasOne(e => e.Student)
                .WithMany(s => s.Exams)
                .HasForeignKey(e => e.StudentId);

            modelBuilder.Entity<Exam>()
                .HasOne(e => e.Subject)
                .WithMany(s => s.Exams)
                .HasForeignKey(e => e.SubjectId);

            //modelBuilder.Entity<Enrollment>()
            //    .HasOne(e => e.Student)
            //    .WithMany(s => s.Enrollments)
            //    .HasForeignKey(e => e.StudentId);

            //modelBuilder.Entity<Enrollment>()
            //    .HasOne(e => e.Subject)
            //    .WithMany(s => s.Enrollments)
            //    .HasForeignKey(e => e.SubjectId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // SQL = LT2BS28033\CURSO
        // SQL pecerra = 
        // MySQL pecerra = @"server=localhost;database=Academy;uid=Toni;password=mys;",
        // MySQL= @"server=localhost;database=Academy; uid=toni;password=Mys_12345; ",
        {
            //optionsBuilder.UseSqlServer(@"server=LT2BS28033\CURSO;Database=AcademyDB;Trusted_Connection = true; MultipleActiveResultSets=true");
            optionsBuilder
                    .UseMySql(connectionString: @"server=localhost;database=Academy; uid=toni;password=Mys_12345; ",

                    new MySqlServerVersion(new Version(8, 0, 23)));
        }

        //MySQL
        //optionsBuilder
        //        .UseMySql(connectionString: @"server=localhost;database=AcademyDB;uid=Toni;password=mys;",
        //        new MySqlServerVersion(new Version(8, 0, 23)));

    }
}
