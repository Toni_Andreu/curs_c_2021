﻿using Academy.Lib.Models;
using Academy.UI.WPF.DB.DAL;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;


namespace Academy.UI.WPF.DB.Views
{
    /// <summary>
    /// Interaction logic for ExamsView.xaml
    /// </summary>
    public partial class ExamsView : UserControl
    {
        public Exam SelectedExam
        {
            get
            {
                return _selectedExam;
            }
            set
            {
                _selectedExam = value;
                if (value == null)
                {
                    InputStudentDni.Text = string.Empty;
                    InputExSubject.Text = string.Empty;
                    InputTimestamp.Text = string.Empty;
                    InputMark.Text = string.Empty;

                }
                else
                {
                    InputStudentDni.Text = value.StudentDni;
                    InputExSubject.Text = value.SubjectName;
                    InputTimestamp.Text = value.TimeStamp.ToString();
                    InputMark.Text = value.Mark.ToString();
                }
            }
        }
        Exam _selectedExam;
        public ExamsView()
        {
            InitializeComponent();
            using (var repo = new ExamsRepository())
            {
                DgExams.ItemsSource = repo.GetAll();
            }
        }
        public IEnumerable ExamsList { get; }

        #region EXAMS

        void AddNewExam()
        {
            string studentdni = InputStudentDni.Text;
            string subject = InputExSubject.Text;
            DateTime timestamp = DateTime.Now;
            string error = "";
            var stringMark = InputMark.Text;
            double mark = Convert.ToDouble(stringMark);

            //Validations
            if (Exam.ValidateEmpty(subject))
            {
                MessageBox.Show(Exam.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Exam.ValidateEmpty(studentdni))
            {
                MessageBox.Show(Exam.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Exam.ValidateLength(studentdni))
            {
                MessageBox.Show(Exam.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            //// existeix la key: DNI+Subject?
            //else if (Exam.ContainsKey(studentdni + subject))
            //{
            //    MessageBox.Show(Exam.ErrSubjectExist, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (Exam.ValidateEmpty(stringMark))
            {
                MessageBox.Show(Exam.ErrBuidMark, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            // valida rango 0- 10 + double
            else if (Exam.ValidateRang(mark))
            {
                MessageBox.Show(Exam.ErrMarkRang, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            //Ojo!  Doubles
            else
            {
                try
                {
                    var exam = new Exam
                    {
                        SubjectName = subject,
                        StudentDni = studentdni,
                        Mark = mark,
                        TimeStamp = DateTime.Now,
                    };
                    using (var repo = new ExamsRepository())
                    {
                        repo.Add(exam);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        public void DeleteExam(Exam exam)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Examen?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    using (var repo = new ExamsRepository())
                    {
                        repo.Delete(exam.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        public void EditExam()
        {
            if (SelectedExam != null)
            {
                var examCopy = SelectedExam.Clone();

                examCopy.StudentDni = InputStudentDni.Text;
                examCopy.SubjectName = InputExSubject.Text;
                var stringMark = InputMark.Text;
                examCopy.Mark = Convert.ToDouble(stringMark);
                var stringDate = InputTimestamp.Text;
                examCopy.TimeStamp = Convert.ToDateTime(stringDate);
                string error = string.Empty;

                //Validations
                if (Exam.ValidateEmpty(examCopy.SubjectName))
                {
                    MessageBox.Show(Exam.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Exam.ValidateEmpty(examCopy.StudentDni))
                {
                    MessageBox.Show(Exam.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Exam.ValidateLength(examCopy.StudentDni))
                {
                    MessageBox.Show(Exam.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                //// existeix la key: DNI+Subject?
                //else if (Exam.Exams.ContainsKey(studentdni + subject))
                //{
                //    MessageBox.Show(Exam.ErrSubjectExist, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //}
                else if (Exam.ValidateEmpty(stringMark))
                {
                    MessageBox.Show(Exam.ErrBuidMark, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                // valida rango 0- 10 + double
                else if (Exam.ValidateRang(examCopy.Mark))
                {
                    MessageBox.Show(Exam.ErrMarkRang, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                //Ojo!  Doubles
                else
                {
                    try
                    {
                        using (var repo = new ExamsRepository())
                        {
                            repo.Edit(examCopy);
                        }
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                        MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }
        #endregion

        #region Btns Selects

        private void BtSelectExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamInicial();
            BtEditExam.Visibility = Visibility.Visible;
            SelectedExam = ((Button)sender).DataContext as Exam;
            DadesExam.Text = SelectedExam.SubjectName +
                " (ex. " + SelectedExam.TimeStamp +
                ") de: " + SelectedExam.StudentDni +
                "  Nota: " + SelectedExam.Mark;
        }
        #endregion

        #region Btns CLICK

        private void BtAddExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesExam.Text = string.Empty;
            VisibilityExamNew();
        }
        private void BtAddExam_Click(object sender, RoutedEventArgs e)
        {
            VisibilityExamNewSave();
            AddNewExam();

            Clear();
            ClearInputs();
            var repo = new ExamsRepository();
            DgExams.ItemsSource = repo.GetAll();
        }
        private void BtDeleteExam_Click(object sender, RoutedEventArgs e)
        {
            var selectedExam = ((Button)sender).DataContext as Exam;
            DeleteExam(selectedExam);
            ClearInputs();
            Clear();
            BtEditExam.Visibility = Visibility.Hidden;
            var repo = new ExamsRepository();
            DgExams.ItemsSource = repo.GetAll();
        }
        private void BtEditExam_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityExamEdit();
            InputStudentDni.Text = SelectedExam.StudentDni;
            InputExSubject.Text = SelectedExam.SubjectName;
            InputTimestamp.Text = Convert.ToString(SelectedExam.TimeStamp);
            InputMark.Text = Convert.ToString(SelectedExam.Mark);
        }
        private void BtEditExam_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar DNi ni Subject = KEY!!
            string studentDni = SelectedExam.StudentDni;
            string subject = SelectedExam.SubjectName;
            //agafem dades dels inputs
            DateTime timestamp = Convert.ToDateTime(InputTimestamp.Text);
            Double mark = Convert.ToDouble(InputMark.Text);

            EditExam();
            VisibilityExamEditSave();
            Clear();
            ClearInputs();
            var repo = new ExamsRepository();
            DgExams.ItemsSource = repo.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilityExamInputsON()
        {
            LbStudentDni.Visibility = Visibility.Visible;
            LbExSubject.Visibility = Visibility.Visible;
            LbTimestamp.Visibility = Visibility.Visible;
            LbMark.Visibility = Visibility.Visible;
            InputStudentDni.Visibility = Visibility.Visible;
            InputExSubject.Visibility = Visibility.Visible;
            InputTimestamp.Visibility = Visibility.Visible;
            InputMark.Visibility = Visibility.Visible;
        }
        void VisibilityExamInputsOFF()
        {
            LbStudentDni.Visibility = Visibility.Hidden;
            LbExSubject.Visibility = Visibility.Hidden;
            LbTimestamp.Visibility = Visibility.Hidden;
            LbMark.Visibility = Visibility.Hidden;
            InputStudentDni.Visibility = Visibility.Hidden;
            InputExSubject.Visibility = Visibility.Hidden;
            InputTimestamp.Visibility = Visibility.Hidden;
            InputMark.Visibility = Visibility.Hidden;
        }
        void VisibilityExamInicial()
        {
            BtNewExam.Visibility = Visibility.Visible;
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveNewExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEdit()
        {
            VisibilityExamInputsON();
            BtSaveEditExam.Visibility = Visibility.Visible;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamEditSave()
        {
            VisibilityExamInputsOFF();
            BtSaveEditExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNew()
        {
            Clear();
            ClearInputs();
            VisibilityExamInputsON();
            BtSaveNewExam.Visibility = Visibility.Visible;
            BtNewExam.Visibility = Visibility.Hidden;
            BtEditExam.Visibility = Visibility.Hidden;
            BtSaveEditExam.Visibility = Visibility.Hidden;
        }
        void VisibilityExamNewSave()
        {
            VisibilityExamInputsOFF();
            BtSaveNewExam.Visibility = Visibility.Hidden;
            BtNewExam.Visibility = Visibility.Visible;
        }
        void Clear()
        {
            DadesExam.Text = string.Empty;
            SelectedExam = null;
        }
        void ClearInputs()
        {
            InputStudentDni.Text = string.Empty;
            InputExSubject.Text = string.Empty;
            InputTimestamp.Text = string.Empty;
            InputMark.Text = string.Empty;
        }


        #endregion




    }
}
