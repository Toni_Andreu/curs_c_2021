﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Academy.UI.WPF.DB.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MenuView : UserControl
    {
        public MenuView()
        {
            InitializeComponent();

            Inputlegenda.Text = Convencions;
        }


        private void BtSortir_Click(object sender, RoutedEventArgs e)
        {
            string msg = "Vol tancar l'aplicació ?";
            string title = "Atenció!";
            MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dia == MessageBoxResult.No)
            {
            }
            else
            {
                Application.Current.Shutdown();
                //Application.Current.MainWindow.Close();
            }
        }
        

        public const string Convencions = " \n" +
            "* No deixem editar directament en els Bindings, cal usar la botonera preparada al efecte\n" +
            "";

        //Inputlegenda.Text = Convencions;
    }


}
