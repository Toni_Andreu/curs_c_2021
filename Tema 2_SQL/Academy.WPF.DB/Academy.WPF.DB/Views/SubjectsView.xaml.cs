﻿using Academy.Lib.Models;
using Academy.UI.WPF.DB.DAL;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;


namespace Academy.UI.WPF.DB.Views
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView : UserControl
    {
        public Subject SelectedSubject
        {
            get
            {
                return _selectedSubject;
            }
            set
            {
                _selectedSubject = value;
                if (value == null)
                {
                    InputSubject.Text = string.Empty;
                    InputTeacher.Text = string.Empty;
                    InputCode.Text = string.Empty;

                }
                else
                {
                    InputSubject.Text = value.Name;
                    InputTeacher.Text = value.Teacher;
                    InputCode.Text = value.Code;
                }
            }
        }
        Subject _selectedSubject;
        public SubjectsView()
        {
            InitializeComponent();
            using (var repo = new SubjectsRepository())
            {
                DgSubjects.ItemsSource = repo.GetAll();
            }
        }
        public IEnumerable SubjectsList { get; }


        #region SUBJECTS

        public void AddNewSubject()
        {
            string name = InputSubject.Text;
            string teacher = InputTeacher.Text;
            string code = InputCode.Text;
            string error = string.Empty;

            if (Subject.ValidateEmpty(name))
            {
                MessageBox.Show(Subject.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //else if (Subject.Subjects.ContainsKey(name))
            //{
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (Subject.ValidateEmpty(teacher))
            {
                MessageBox.Show(Subject.ErrBuidTeacher, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    var subject = new Subject
                    {
                        Name = name,
                        Teacher = teacher,
                        Code = code
                    };
                    using (var repo = new SubjectsRepository())
                    {
                        repo.Add(subject);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public void DeleteSubject(Subject subject)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquesta Asignatura?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    return;
                }
                else
                {
                    using (var repo = new SubjectsRepository())
                    {
                        repo.Delete(subject.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void EditSubject()
        {
            var subjectCopy = SelectedSubject.Clone();

            subjectCopy.Name = InputSubject.Text;
            subjectCopy.Teacher = InputTeacher.Text;
            subjectCopy.Code = InputCode.Text;
            string error = string.Empty;

            if (Subject.ValidateEmpty(subjectCopy.Name))
            {
                MessageBox.Show(Subject.ErrBuidSubject, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            //else if (Subjects.ContainsKey(name))
            //{
            //    MessageBox.Show(Subject.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            else if (Subject.ValidateEmpty(subjectCopy.Teacher))
            {
                MessageBox.Show(Subject.ErrBuidTeacher, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (Subject.ValidateEmpty(subjectCopy.Code))
            {
                MessageBox.Show(Subject.ErrBuidCode, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                try
                {
                    using (var repo = new SubjectsRepository())
                    {
                        repo.Edit(subjectCopy);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        #endregion


        #region Btns Selects

        private void BtSelectSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectInicial();
            BtEditSubject.Visibility = Visibility.Visible;
            SelectedSubject = ((Button)sender).DataContext as Subject;
            InputSubject.Text = SelectedSubject.Name;
            DadesSubject.Text = SelectedSubject.Name + "  " + SelectedSubject.Teacher + "  " + SelectedSubject.Code;
        }

        #endregion

        #region Btns CLICK

        private void BtAddSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesSubject.Text = string.Empty;
            VisibilitySubjectNew();
        }
        private void BtAddSubject_Click(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectNewSave();
            AddNewSubject();

            Clear();
            ClearInputs();
            var repo = new SubjectsRepository();
            DgSubjects.ItemsSource = repo.GetAll();
        }
        private void BtDeleteSubject_Click(object sender, RoutedEventArgs e)
        {
            var selectedSubject = ((Button)sender).DataContext as Subject;
            DeleteSubject(selectedSubject);
            ClearInputs();
            Clear();
            BtEditSubject.Visibility = Visibility.Hidden;
            var repo = new SubjectsRepository();
            DgSubjects.ItemsSource = repo.GetAll();
        }
        private void BtEditSubject_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilitySubjectEdit();
            InputSubject.Text = SelectedSubject.Name;
            InputTeacher.Text = SelectedSubject.Teacher;
            InputCode.Text = SelectedSubject.Code;
        }
        private void BtEditSubject_Click(object sender, RoutedEventArgs e)
        {
            //No deixem editar el nom de l'Asignatura = > que creien una de nova!
            string name = SelectedSubject.Name;
            string teacher = InputTeacher.Text;
            string code = InputCode.Text;

            EditSubject();
            VisibilitySubjectEditSave();
            Clear();
            ClearInputs();
            var repo = new SubjectsRepository();
            DgSubjects.ItemsSource = repo.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilitySubjectInputsON()
        {
            LbSubject.Visibility = Visibility.Visible;
            LbTeacher.Visibility = Visibility.Visible;
            InputSubject.Visibility = Visibility.Visible;
            InputTeacher.Visibility = Visibility.Visible;
            InputCode.Visibility = Visibility.Visible;
        }
        void VisibilitySubjectInputsOFF()
        {
            LbSubject.Visibility = Visibility.Hidden;
            LbTeacher.Visibility = Visibility.Hidden;
            InputSubject.Visibility = Visibility.Hidden;
            InputTeacher.Visibility = Visibility.Hidden;
            InputCode.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectInicial()
        {
            BtNewSubject.Visibility = Visibility.Visible;
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveNewSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEdit()
        {
            VisibilitySubjectInputsON();
            BtSaveEditSubject.Visibility = Visibility.Visible;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectEditSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveEditSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNew()
        {
            Clear();
            ClearInputs();
            VisibilitySubjectInputsON();
            BtSaveNewSubject.Visibility = Visibility.Visible;
            BtNewSubject.Visibility = Visibility.Hidden;
            BtEditSubject.Visibility = Visibility.Hidden;
            BtSaveEditSubject.Visibility = Visibility.Hidden;
        }
        void VisibilitySubjectNewSave()
        {
            VisibilitySubjectInputsOFF();
            BtSaveNewSubject.Visibility = Visibility.Hidden;
            BtNewSubject.Visibility = Visibility.Visible;
        }
        void Clear()
        {
            DadesSubject.Text = string.Empty;
            SelectedSubject = null;
        }
        void ClearInputs()
        {
            InputSubject.Text = string.Empty;
            InputTeacher.Text = string.Empty;
            InputCode.Text = string.Empty;
        }


        #endregion


    }
}
