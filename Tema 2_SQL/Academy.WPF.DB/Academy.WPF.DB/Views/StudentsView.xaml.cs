﻿using Academy.Lib.Models;
using Academy.UI.WPF.DB.DAL;
using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Academy.UI.WPF.DB.Views
{
    /// <summary>
    /// Interaction logic for StudentsView.xaml
    /// </summary>
    public partial class StudentsView : UserControl
    {
        public Student SelectedStudent
        {
            get
            {
                return _selectedStudent;
            }
            set
            {
                _selectedStudent = value;
                if (value == null)
                {
                    InputDni.Text = string.Empty;
                    InputName.Text = string.Empty;
                    InputSurname1.Text = string.Empty;
                    InputSurname2.Text = string.Empty;
                    InputEmail.Text = string.Empty;

                }
                else
                {
                    InputDni.Text = value.Dni;
                    InputName.Text = value.Name;
                    InputSurname1.Text = value.Surname1;
                    InputSurname2.Text = value.Surname2;
                    InputEmail.Text = value.Email;
                }
            }
        }
        Student _selectedStudent;

        public StudentsView()
        {
            InitializeComponent();
            //Inicialitza el DataGrid de Students
            using (var repo = new StudentsRepository())
            {
                DgStudents.ItemsSource = repo.GetAll();
            }
        }

        public IEnumerable StudentsList { get; }

        #region STUDENTS

        public void AddNewStudent()
        {
            string dni = InputDni.Text;
            string name = InputName.Text;
            string cognom1 = InputSurname1.Text;
            string cognom2 = InputSurname2.Text;
            string email = InputEmail.Text;
            string error = string.Empty;

            //Validacions
            if (Student.ValidateEmpty(dni))
            {
                MessageBox.Show(Student.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Student.ValidateLength(dni))
            {
                MessageBox.Show(Student.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (Student.ValidateEmpty(name))
            {
                MessageBox.Show(Student.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (Student.ValidateEmpty(cognom1))
            {
                MessageBox.Show(Student.ErrBuidSurname1, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            // slguns poden NO tenir 2n cognom!!
            //else if (Student.ValidateEmpty(cognom2))
            //{
            //}
            else
            {
                try
                {
                    var student = new Student
                    {
                        Dni = dni,
                        Name = name,
                        Surname1 = cognom1,
                        Surname2 = cognom2,
                        Email = email
                    };
                    using (var repo = new StudentsRepository())
                    {
                        repo.Add(student);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }

        public static void AddMark(string text)
        {
            var mark = 0.0;

            if (double.TryParse(text, out mark))
            {
                //Marks.Add(mark);
                Console.WriteLine("Nota introducida correctamente, añada otra nota más, pulse all para lista todas las notas o pulse m para volver al menú principal");
            }
            else
            {
                Console.WriteLine($"La nota introducida {text} no está en un formato correcto, vuelva a introducirla correctamente");
            }
        }

        public void DeleteStudent(Student student)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Alumne?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {
                    
                }
                else
                {
                    //var result = StudentsRepository.Students.Remove(key); Dictionary
                    using (var repo = new StudentsRepository())
                    {
                        repo.Delete(student.Id);
                    }                    
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        public void EditStudent()
        {
            if (SelectedStudent != null)
            {
                var studentCopy = SelectedStudent.Clone();

                studentCopy.Dni = InputDni.Text;
                studentCopy.Name = InputName.Text;
                studentCopy.Surname1 = InputSurname1.Text;
                studentCopy.Surname2 = InputSurname2.Text;
                studentCopy.Email = InputEmail.Text;
                string error = string.Empty;

                //Validacions
                if (Student.ValidateEmpty(studentCopy.Dni))
                {
                    MessageBox.Show(Student.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Student.ValidateLength(studentCopy.Dni))
                {
                    MessageBox.Show(Student.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (Student.ValidateEmpty(studentCopy.Name))
                {
                    MessageBox.Show(Student.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (Student.ValidateEmpty(studentCopy.Surname1))
                {
                    MessageBox.Show(Student.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                //else if (Student.ValidateEmpty(newcognom2))
                //{
                //}
                else
                {
                    try
                    {
                        using (var repo = new StudentsRepository())
                        {
                            repo.Edit(studentCopy);
                        }
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                        MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }
        #endregion

        #region Btns Selects

        private void BtSelectStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentInicial();
            BtEdit.Visibility = Visibility.Visible;
            SelectedStudent = ((Button)sender).DataContext as Student;
            DadesStudent.Text = SelectedStudent.Dni + "- " + SelectedStudent.Name + " " + SelectedStudent.Surname1 + " " + SelectedStudent.Surname2;
        }

        #endregion

        #region Btns CLICK

        private void BtAddStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesStudent.Text = string.Empty;
            VisibilityStudentNew();
        }
        private void BtAddStudent_Click(object sender, RoutedEventArgs e)
        {
            VisibilityStudentNewSave();
            AddNewStudent();

            Clear();
            ClearInputs();
            var repo = new StudentsRepository();
            DgStudents.ItemsSource = repo.GetAll();
        }
        private void BtDeleteStudent_Click(object sender, RoutedEventArgs e)
        {
            var selectedStudent = ((Button)sender).DataContext as Student;
            DeleteStudent(selectedStudent);
            ClearInputs();
            Clear();
            BtEdit.Visibility = Visibility.Hidden;
            var repo = new StudentsRepository();
            DgStudents.ItemsSource = repo.GetAll();
        }
        private void BtEditStudent_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityStudentEdit();
            //var selectedStudent = ((Button)sender).DataContext as Student;
            InputDni.Text = SelectedStudent.Dni;
            InputName.Text = SelectedStudent.Name;
            InputSurname1.Text = SelectedStudent.Surname1;
            InputSurname2.Text = SelectedStudent.Surname2;
        }
        private void BtEditStudent_Click(object sender, RoutedEventArgs e)
        {
            EditStudent();
            VisibilityStudentEditSave();
            Clear();
            ClearInputs();

            var repo = new StudentsRepository();
            DgStudents.ItemsSource = repo.GetAll();
        }

        #endregion

        #region VISIBILITIES

        void VisibilityStudentInputsON()
        {
            LbDni.Visibility = Visibility.Visible;
            InputDni.Visibility = Visibility.Visible;
            LbName.Visibility = Visibility.Visible;
            InputName.Visibility = Visibility.Visible;
            LbCognom1.Visibility = Visibility.Visible;
            InputSurname1.Visibility = Visibility.Visible;
            LbCognom2.Visibility = Visibility.Visible;
            InputSurname2.Visibility = Visibility.Visible;
            LbEmail.Visibility = Visibility.Visible;
            InputEmail.Visibility = Visibility.Visible;
        }
        void VisibilityStudentInputsOFF()
        {
            LbDni.Visibility = Visibility.Hidden;
            InputDni.Visibility = Visibility.Hidden;
            LbName.Visibility = Visibility.Hidden;
            InputName.Visibility = Visibility.Hidden;
            LbCognom1.Visibility = Visibility.Hidden;
            InputSurname1.Visibility = Visibility.Hidden;
            LbCognom2.Visibility = Visibility.Hidden;
            InputSurname2.Visibility = Visibility.Hidden;
            LbEmail.Visibility = Visibility.Hidden;
            InputEmail.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentInicial()
        {
            BtNew.Visibility = Visibility.Visible;
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveNew.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEdit()
        {
            VisibilityStudentInputsON();
            BtSaveEdit.Visibility = Visibility.Visible;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentEditSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNew()
        {
            Clear();
            ClearInputs();
            VisibilityStudentInputsON();
            BtSaveNew.Visibility = Visibility.Visible;
            BtNew.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityStudentNewSave()
        {
            VisibilityStudentInputsOFF();
            BtSaveNew.Visibility = Visibility.Hidden;
            BtNew.Visibility = Visibility.Visible;
        }
        void Clear()
        {
            DadesStudent.Text = string.Empty;
            SelectedStudent = null;
        }
        void ClearInputs()
        {
            InputDni.Text = string.Empty;
            InputName.Text = string.Empty;
            InputSurname1.Text = string.Empty;
            InputSurname2.Text = string.Empty;
            InputEmail.Text = string.Empty;
        }


        #endregion

    }
}
