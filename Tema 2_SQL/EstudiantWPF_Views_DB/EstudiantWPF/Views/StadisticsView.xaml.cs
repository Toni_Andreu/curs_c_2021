﻿using Academy.UI.WPF.DB.Lib.DAL;
using Academy.UI.WPF.DB.Lib.Models;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Academy.UI.WPF.DB.Views
{
    /// <summary>
    /// Interaction logic for Stadistics.xaml
    /// </summary>
    public partial class StadisticsView : UserControl
    {
        public StadisticsView()
        {
            InitializeComponent();
            DgEstadistics.ItemsSource = ExamsRepository.GetAll();
        }


        private void StadisticsAvg_Click(object sender, RoutedEventArgs e)
        {
            ShowAverage();
        }
        private void StadisticsMax_Click(object sender, RoutedEventArgs e)
        {
            ShowMaximum();
        }
        private void StadisticsMin_Click(object sender, RoutedEventArgs e)
        {
            ShowMinimum();
        }

        #region ESTADISTIQUES

        void ShowAverage()
        {
            double suma = 0.0;
            double average1 = 0.0;

            try
            {
                foreach (var x in ExamsRepository.Exams.Values)
                {
                    suma += x.Mark;
                }
                average1 = suma / ExamsRepository.Exams.Values.Count;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            average.Text = average1.ToString();
        }

        void ShowMaximum()
        {
            var max = 0.0;
            try
            {
                foreach (var x in ExamsRepository.Exams.Values)
                {
                    if (x.Mark > max)
                        max = x.Mark;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            StadisticsMax.Text = max.ToString();

        }

        void ShowMinimum()
        {
            var min = 10.0;
            try
            {
                foreach (var x in ExamsRepository.Exams.Values)
                {
                    if (x.Mark <= min)
                        min = x.Mark;
                }
                Console.WriteLine("La nota més baixa és: {0}", min);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            StadisticsMin.Text = min.ToString();
        }
        #endregion


    }
}
