﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.UI.WPF.DB.Lib.Models
{
    public class Subject : Entity
    {
        public string Name { get; set; }
        public string Teacher { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Exam> Exams { get; set; }

        //constructors
        public Subject()
        {

        }
        public Subject(string name, string teacher)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Teacher = teacher;
        }
    }
}