﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.UI.WPF.DB.Lib.Models
{
    public class Student : Entity
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Exam> Exams { get; set; }

        //constructors 
        public Student()
        {
            
        }

        //constructor per carrega
        public Student(string name, string surname1, string surname2, string dni, string email)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Surname1 = surname1;
            this.Surname2 = surname2;
            this.Dni = dni;
            this.Email = email;
        }
        public Student Clone()
        {
            var output = new Student
            {
                Id = this.Id,
                Name = this.Name,
                Surname1 = this.Surname1,
                Surname2 = this.Surname2,
                Dni = this.Dni,
                Email = this.Email
            };

            return output;
        }
    }
}
