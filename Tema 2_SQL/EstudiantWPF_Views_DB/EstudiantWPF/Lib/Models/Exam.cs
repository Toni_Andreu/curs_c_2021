﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.UI.WPF.DB.Lib.Models
{
    public class Exam : Entity
    {
        public string StudentDni { get; set; }
        public string SubjectName { get; set; }
        public double Mark { get; set; }
        public DateTime TimeStamp { get; set; }
        public Guid StudentId { get; set; }
        public Guid SubjectId { get; set; }
        public Student Student { get; set; }
        public Subject Subject { get; set; }


        //constructors
        public Exam(string studentDni, string subject, double mark, DateTime timeStamp)
        {
            this.Id = Guid.NewGuid();
            this.StudentDni = studentDni;
            this.SubjectName = subject;
            this.Mark = mark;
            this.TimeStamp = timeStamp;
        }
        public Exam()
        {

        }
    }
}
