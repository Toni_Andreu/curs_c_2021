﻿using Academy.UI.WPF.DB.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.UI.WPF.DB.Lib.DAL
{
    public class StudentsRepository
    {
        //public static Dictionary<string, Student> Students = new Dictionary<string, Student>();
        //public Student SelectedStudent;
        public static Dictionary<string, Student> Students
        {
            get
            {
                if (_students == null)
                {
                    _students = new Dictionary<string, Student>();
                    DataStudent();
                }
                return _students;
            }
        }
        static Dictionary<string, Student> _students;

        public List<Student> StudentsList
        {
            get
            {
                return GetAll();
            }
        }

        public static List<Student> GetAll()
        {
            return Students.Values.ToList();
        }



        #region VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            return string.IsNullOrEmpty(nom);
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            return nom.Length != 9;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            return false;
        }
        #endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "El Nom de l'Alumne no pot estar buid!";
        public const string ErrBuidSurname1 = "El Cognom de l'Alumne no pot estar buid!";
        // DNI Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte\n" +
            "(xxxxxxxxL)";
        // DNI existent
        public const string ErrExisteix = "Aquest Dni ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix el DNI:";
        #endregion

        #region DATA
        static void DataStudent()
        {
            var student1 = new Student("Pep", "Mira", "López", "38412532D", "pep@apple.com");
            _students.Add(student1.Dni, student1);
            var student2 = new Student("Maria", "Mira", "López", "58412532D", "maria@apple.com");
            _students.Add(student2.Dni, student2);
            var student3 = new Student("Carles", "Camps", "Rodriguez", "12345678A", "carles@apple.com");
            _students.Add(student3.Dni, student3);
            var student4 = new Student("Gemma", "Jimenez", "Sanchez", "23456789A", "gemma@apple.com");
            _students.Add(student4.Dni, student4);
            var student5 = new Student("Victor", "Martínez", "López", "18412532D", "victor@apple.com");
            _students.Add(student5.Dni, student5);
            var student6 = new Student("Anna", "Vila", "Jimenez", "28412532D", "anna@apple.com");
            _students.Add(student6.Dni, student6);
            var student7 = new Student("Cèlia", "Mira", "Martinez", "48412532D", "celia@apple.com");
            _students.Add(student7.Dni, student7);
            var student8 = new Student("Carla", "Saladie", "López", "68412532D", "carla@apple.com");
            _students.Add(student8.Dni, student8);
            var student9 = new Student("Joan", "Vermell", "López", "78412532D", "joan@apple.com");
            _students.Add(student9.Dni, student9);
            var student10 = new Student("Raquel", "Mirabet", "Sladie", "88412532D", "raquel@apple.com");
            _students.Add(student10.Dni, student10);
        }
        #endregion

    }
}
