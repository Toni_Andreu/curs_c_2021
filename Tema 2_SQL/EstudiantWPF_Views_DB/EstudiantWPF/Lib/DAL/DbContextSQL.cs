﻿using Academy.UI.WPF.DB.Lib.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Academy.UI.WPF.DB.Views_DB.Lib.DAL
{
    public class DbContextSQL : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbContextSQL() : base()
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().ToTable("Students");
            modelBuilder.Entity<Subject>().ToTable("Subjects");
            modelBuilder.Entity<Exam>().ToTable("Exams");


            modelBuilder.Entity<Exam>()
                .HasOne(e => e.Student)
                .WithMany(s => s.Exams)
                .HasForeignKey(e => e.StudentId);

            modelBuilder.Entity<Exam>()
                .HasOne(e => e.Subject)
                .WithMany(s => s.Exams)
                .HasForeignKey(e => e.SubjectId);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // SQL = LT2BS28033\CURSO
        // SQL pecerra = 
        // MySQL pecerra = @"server=localhost;database=AcademyDB;uid=Toni;password=mys;",
        {
            optionsBuilder.UseSqlServer(@"server=LT2BS28033\CURSO;Database=AcademyDB;Trusted_Connection = true; MultipleActiveResultSets=true");
        }

        //MySQL
        //optionsBuilder
        //        .UseMySql(connectionString: @"server=localhost;database=AcademyDB;uid=Toni;password=mys;",
        //        new MySqlServerVersion(new Version(8, 0, 23)));

    }
}
