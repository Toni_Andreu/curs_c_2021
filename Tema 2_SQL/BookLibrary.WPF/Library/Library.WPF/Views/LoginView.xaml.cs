﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }
        private void BtLogin_Click(object sender, RoutedEventArgs c)
        {
            var email = TbEmail.Text;
            string pass = TbPass.Password;

            //TbEmail.Text = string.Empty;
            TbPass.Password = string.Empty;

            using (var staffRepo = new StaffsRepository())
            {
                var user = staffRepo.GetAll()
                            .FirstOrDefault(x => x.Email == email && x.Pass == pass);

                if (user != null)
                {
                    LoadAdminUser(user);
                    return;
                }
            }

            using (var membersRepo = new MembersRepository())
            {
                var user = membersRepo.GetAll()
                            .FirstOrDefault(x => x.Email == email && x.Pass == pass);

                if (user != null)
                {
                    LoadMemberUser(user);
                    return;
                }
                else
                {
                    MessageBox.Show("Accés incorrecte.\n" +
                       "Provi de nou.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            ////login librarian
            //if (pass == "1234" && user == "maria@icloud.com")

            //confirma login DB : "maria@icloud.com", "pass2"
            //`Id` = 'b996d58f-bdb3-4290-ab93-cea9e409c1c4'
        }

        private void BtSortir_Click(object sender, RoutedEventArgs e)
        {
            string msg = "Vol tancar l'aplicació ?";
            string title = "Atenció!";
            MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (dia == MessageBoxResult.No)
            {
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        public void LoadAdminUser(Staff user)
        {
            MainWindow.LibrarianView(user);
        }

        public void LoadMemberUser(Member user)
        {
            MainWindow.UserView(user);
            MyMemberView.Refresh();
        }
    }
}