﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for MyLoansView.xaml
    /// </summary>
    public partial class MyLoansView : UserControl
    {
        #region static
<<<<<<< HEAD
        private static MyLoansView Instance { get; set; }
=======
        private static MyMemberView Instance { get; set; }
>>>>>>> 882b89a59a83d05db6f38611b4d5c500304929b2
        public static void Refresh()
        {
            if (Instance != null && App.CurrentUser != null)
            {
                Instance.SelectedMember = App.CurrentUser as Member;
            }
        }
        #endregion
        public Member SelectedMember
        {
            get
            {
                return _selectedMember;
            }
            set
            {
                _selectedMember = value;
                if (value == null)
                {
                    LbMember.Text = string.Empty;
                }
                else
                {
                    LbMember.Text = value.FullName;
                }
            }
        }
        Member _selectedMember;
        public MyLoansView()
        {
            InitializeComponent();

<<<<<<< HEAD
            Instance = this; 
=======
            //Instance = this;  //="XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
>>>>>>> 882b89a59a83d05db6f38611b4d5c500304929b2
            ReloadData();
        }
        public void ReloadData()
        {
            using (var repo = new BooksRepository())
            {
                CbxBook.ItemsSource = repo.GetAll().OrderBy(e => e.TitleAuthor);
            }
            //var MembersCopy = SelectedMember.Clone();
        }

        private bool ValidaLoansMember
        { 
            get
            {
                return true;
            }

        }
            //get
            //{
            //    Guid member = Guid.NewGuid(); // "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            //    //var id = member.Id;
            //    //var limit = 3;

            //    //using (var repo = new LoansRepository())
            //    //{
            //    //    int loansTotal = repo.GetAll().Count(x => x.MemberId == id && x.ReturnDate == null);
            //    //    if (loansTotal >= limit)
            //    //    {
            //    //        return false;
            //    //    }
            //    //    else
            //    //    {
            //    //        return true;
            //    //    }
            //    //}
            //}
        //}
        public void ReloadLoans(Guid id)
        {
            using (var repo = new LoansRepository())
            {
                DgLoansRelated.ItemsSource = repo.GetAll()
                                                    .Where(s => s.MemberId == id && s.ReturnDate == null)
                                                    .ToList();
            }
        }
        public void ReloadCopies(Guid id)
        {
            using (var repo = new BookCopiesRepository())
            {
                DgCopiesRelated.ItemsSource = repo.GetAll()
                                                    .Where(s => s.BookId == id && s.IsAvailable)
                                                    .ToList();
            }
        }
        private void BtRelCopies_Click(object sender, RoutedEventArgs e)
        {
            var book = CbxBook.SelectedItem as Book;
            var id = book.Id;
            ReloadCopies(id);
        }
        private void BtSelectBookCopy_Click(object sender, RoutedEventArgs e)
        {
            var MembersCopy = SelectedMember.Clone();
            Guid MyMemberId = MembersCopy.Id;

            var copy = ((Button)sender).DataContext as BookCopy;
            if (ValidaLoansMember == false)
            {
                string err = "Aquest Lector actualment te 3 préstecs!! \nNo es pot continuar.";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (copy != null && MyMemberId != null)
            {
                AddLoan(MyMemberId, copy);
            }
            else
            {
                string err = "Ha hagut un problema en els Id's de Copy i/o de Member";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddLoan(Guid myMemberId, BookCopy copy)
        {
            throw new NotImplementedException();
        }

        public void AddLoan(Member MyMemberId, BookCopy copy)
        {
            try
            {
                var loan = new Loan()
                {
                    MemberId = MyMemberId.Id,
                    BookCopyId = copy.Id,
                    LoanDate = DateTime.Now
                };
                using (var repo = new LoansRepository())
                {
                    var addResult = repo.Add(loan);
                    string err = "Prèstec realitzat correctament!!";
                    MessageBox.Show(err, "Atenció!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            //CbxMember.Text = string.Empty;
            CbxBook.Text = string.Empty;
            ReloadLoans(MyMemberId.Id);



        }

        private void CbxBook_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var book = CbxBook.SelectedItem as Book;
            if (book != null)
            {
                var id = book.Id;
                ReloadCopies(id);
            }
            else
            {
                DgCopiesRelated.ItemsSource = new List<Loan>();

            }
        }
    }
}
