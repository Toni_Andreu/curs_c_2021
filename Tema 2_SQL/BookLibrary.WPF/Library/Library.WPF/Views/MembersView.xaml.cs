﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for MemberssView.xaml
    /// </summary>
    public partial class MembersView : UserControl
    {
        public Member SelectedMembers
        {
            get
            {
                return _selectedMembers;
            }
            set
            {
                _selectedMembers = value;
                if (value == null)
                {
                    TbDni.Text = string.Empty;
                    TbName.Text = string.Empty;
                    TbSurname1.Text = string.Empty;
                    TbSurname2.Text = string.Empty;
                    TbEmail.Text = string.Empty;
                    TbPhone.Text = string.Empty;
                    TbPass.Password = string.Empty;
                    TbStatus.IsChecked = false;


                }
                else
                {
                    TbDni.Text = value.Dni;
                    TbName.Text = value.Name;
                    TbSurname1.Text = value.Surname1;
                    TbSurname2.Text = value.Surname2;
                    TbEmail.Text = value.Email;
                    TbPhone.Text = value.ContactPhone;
                    TbPass.Password = value.Pass;
                    TbStatus.IsChecked = value.Status;
                }
            }
        }
        Member _selectedMembers;

        public MembersView()
        {
            InitializeComponent();

            Refresh();
        }

        public void Refresh()
        {
            using (var repo = new MembersRepository())
            {
                DgMembers.ItemsSource = repo.GetAll().OrderBy(e => e.FullName);
            }
        }
        public IEnumerable MembersList { get; }

        #region Members

        public void AddNewMembers()
        {
            string dni = TbDni.Text;
            string name = TbName.Text;
            string cognom1 = TbSurname1.Text;
            string cognom2 = TbSurname2.Text;
            string email = TbEmail.Text;
            string phone = TbPhone.Text;
            string pass = TbPass.Password;
            bool status = (bool)TbStatus.IsChecked;
            string error = string.Empty;

            //Validacions
            if (Member.ValidateEmpty(dni))
            {
                MessageBox.Show(Member.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (Member.ValidateLength(dni))
            {
                MessageBox.Show(Member.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (Member.ValidateLength(phone))
            {
                MessageBox.Show(Member.ErrPhoneNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else if (Member.ValidateEmpty(name))
            {
                MessageBox.Show(Member.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (Member.ValidateEmpty(cognom1))
            {
                MessageBox.Show(Member.ErrBuidSurname1, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            // slguns poden NO tenir 2n cognom!!
            //else if (Members.ValidateEmpty(cognom2))
            //{
            //}
            else
            {
                try
                {
                    var Members = new Member
                    {
                        Dni = dni,
                        Name = name,
                        Surname1 = cognom1,
                        Surname2 = cognom2,
                        Email = email,
                        ContactPhone = phone,
                        Pass = pass,
                        Status= status,
                    };
                    using (var repo = new MembersRepository())
                    {
                        repo.Add(Members);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
        }
        public void DeleteMembers(Member Members)
        {
            try
            {
                string msg = "Està segur que vol esborrar aquest Lector?";
                string title = "Atenció!";
                MessageBoxResult dia = MessageBox.Show(msg, title, MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (dia == MessageBoxResult.No)
                {

                }
                else
                {
                    using (var repo = new MembersRepository())
                    {
                        repo.Delete(Members.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        public void EditMembers()
        {
            if (SelectedMembers != null)
            {
                var MembersCopy = SelectedMembers.Clone();

                MembersCopy.Dni = TbDni.Text;
                MembersCopy.Name = TbName.Text;
                MembersCopy.Surname1 = TbSurname1.Text;
                MembersCopy.Surname2 = TbSurname2.Text;
                MembersCopy.Email = TbEmail.Text;
                MembersCopy.ContactPhone = TbPhone.Text;
                MembersCopy.Pass = TbPass.Password;
                MembersCopy.Status = TbStatus.IsChecked;
                string error = string.Empty;

                //Validacions
                if (Member.ValidateEmpty(MembersCopy.Dni))
                {
                    MessageBox.Show(Member.ErrBuidDni, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (Member.ValidateLength(MembersCopy.Dni))
                {
                    MessageBox.Show(Member.ErrNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (Member.ValidateLength(MembersCopy.ContactPhone))
                {
                    MessageBox.Show(Member.ErrPhoneNo9, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else if (Member.ValidateEmpty(MembersCopy.Name))
                {
                    MessageBox.Show(Member.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if (Member.ValidateEmpty(MembersCopy.Surname1))
                {
                    MessageBox.Show(Member.ErrBuidNom, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                //else if (Members.ValidateEmpty(newcognom2))
                //{
                //}
                else
                {
                    try
                    {
                        using (var repo = new MembersRepository())
                        {
                            repo.Edit(MembersCopy);
                        }
                    }
                    catch (Exception ex)
                    {
                        string err = ex.Message;
                        MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
            }
        }
        #endregion

        #region Btns Selects

        private void BtSelectMember_Click(object sender, RoutedEventArgs e)
        {
            VisibilityMembersInicial();
            BtEdit.Visibility = Visibility.Visible;
            Refresh();
            SelectedMembers = ((Button)sender).DataContext as Member;
            DadesMember.Text = SelectedMembers.Dni + "- " + SelectedMembers.Name + " " + SelectedMembers.Surname1 + " " + SelectedMembers.Surname2 + " "
                + SelectedMembers.ContactPhone + " " + SelectedMembers.Email;
        }

        #endregion

        #region Btns CLICK

        private void BtAddMember_ClickPrep(object sender, RoutedEventArgs e)
        {
            DadesMember.Text = string.Empty;
            VisibilityMembersNew();
        }
        private void BtAddMember_Click(object sender, RoutedEventArgs e)
        {
            VisibilityMembersNewSave();
            AddNewMembers();

            Clear();
            ClearTbs();
            var repo = new MembersRepository();
            DgMembers.ItemsSource = repo.GetAll();
        }
        private void BtDeleteMember_Click(object sender, RoutedEventArgs e)
        {
            var selectedMembers = ((Button)sender).DataContext as Member;
            DeleteMembers(selectedMembers);
            ClearTbs();
            Clear();
            BtEdit.Visibility = Visibility.Hidden;
            var repo = new MembersRepository();
            DgMembers.ItemsSource = repo.GetAll();
        }
        private void BtEditMember_ClickPrep(object sender, RoutedEventArgs e)
        {
            VisibilityMembersEdit();
            TbDni.Text = SelectedMembers.Dni;
            TbName.Text = SelectedMembers.Name;
            TbSurname1.Text = SelectedMembers.Surname1;
            TbSurname2.Text = SelectedMembers.Surname2;
            TbPhone.Text = SelectedMembers.ContactPhone;
            TbEmail.Text = SelectedMembers.Email;
            TbPass.Password = SelectedMembers.Pass;
            TbStatus.IsChecked = SelectedMembers.Status;
        }
        private void BtEditMember_Click(object sender, RoutedEventArgs e)
        {
            EditMembers();
            VisibilityMembersEditSave();
            Clear();
            ClearTbs();

            var repo = new MembersRepository();
            DgMembers.ItemsSource = repo.GetAll();
        }
        
        #endregion

        #region VISIBILITIES

        void VisibilityMembersTbsON()
        {
            LbDni.Visibility = Visibility.Visible;
            TbDni.Visibility = Visibility.Visible;
            LbName.Visibility = Visibility.Visible;
            TbName.Visibility = Visibility.Visible;
            LbCognom1.Visibility = Visibility.Visible;
            TbSurname1.Visibility = Visibility.Visible;
            LbCognom2.Visibility = Visibility.Visible;
            TbSurname2.Visibility = Visibility.Visible;
            LbPhone.Visibility = Visibility.Visible;
            TbPhone.Visibility = Visibility.Visible;
            LbEmail.Visibility = Visibility.Visible;
            TbEmail.Visibility = Visibility.Visible;
            LbPass.Visibility = Visibility.Visible;
            TbPass.Visibility = Visibility.Visible;
            LbStatus.Visibility = Visibility.Visible;
            TbStatus.Visibility = Visibility.Visible;
        }
        void VisibilityMembersTbsOFF()
        {
            LbDni.Visibility = Visibility.Hidden;
            TbDni.Visibility = Visibility.Hidden;
            LbName.Visibility = Visibility.Hidden;
            TbName.Visibility = Visibility.Hidden;
            LbCognom1.Visibility = Visibility.Hidden;
            TbSurname1.Visibility = Visibility.Hidden;
            LbCognom2.Visibility = Visibility.Hidden;
            TbSurname2.Visibility = Visibility.Hidden;
            LbPhone.Visibility = Visibility.Hidden;
            TbPhone.Visibility = Visibility.Hidden;
            LbEmail.Visibility = Visibility.Hidden;
            TbEmail.Visibility = Visibility.Hidden;
            LbPass.Visibility = Visibility.Hidden;
            TbPass.Visibility = Visibility.Hidden;
            LbStatus.Visibility = Visibility.Hidden;
            TbStatus.Visibility = Visibility.Hidden;
        }
        void VisibilityMembersInicial()
        {
            BtNew.Visibility = Visibility.Visible;
            VisibilityMembersTbsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveNew.Visibility = Visibility.Hidden;
        }
        void VisibilityMembersEdit()
        {
            VisibilityMembersTbsON();
            BtSaveEdit.Visibility = Visibility.Visible;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityMembersEditSave()
        {
            VisibilityMembersTbsOFF();
            BtSaveEdit.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityMembersNew()
        {
            Clear();
            ClearTbs();
            VisibilityMembersTbsON();
            BtSaveNew.Visibility = Visibility.Visible;
            BtNew.Visibility = Visibility.Hidden;
            BtEdit.Visibility = Visibility.Hidden;
            BtSaveEdit.Visibility = Visibility.Hidden;
        }
        void VisibilityMembersNewSave()
        {
            VisibilityMembersTbsOFF();
            BtSaveNew.Visibility = Visibility.Hidden;
            BtNew.Visibility = Visibility.Visible;
        }
        void Clear()
        {
            DadesMember.Text = string.Empty;
            SelectedMembers = null;
        }
        void ClearTbs()
        {
            TbDni.Text = string.Empty;
            TbName.Text = string.Empty;
            TbSurname1.Text = string.Empty;
            TbSurname2.Text = string.Empty;
            TbEmail.Text = string.Empty;
            TbPhone.Text = string.Empty;
            TbEmail.Text = string.Empty;
            TbPass.Password = string.Empty;
            TbStatus.IsChecked = false;
        }


        #endregion

    }
}
