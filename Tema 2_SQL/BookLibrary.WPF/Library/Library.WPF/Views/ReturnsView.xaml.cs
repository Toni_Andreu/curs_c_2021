﻿using Library.Lib.Models;
using Library.UI.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Library.UI.Views
{
    /// <summary>
    /// Interaction logic for ReturnsView.xaml
    /// </summary>
    public partial class ReturnsView : UserControl
    {
        public ReturnsView()
        {
            InitializeComponent(); 
            ReloadData();
        }
        public void ReloadData()
        {
            using (var repo = new MembersRepository())
            {
                CbxMember.ItemsSource = repo.GetAll().OrderBy(e => e.FullName);
            }
        }
        private void BtSelectReturn_Click(object sender, RoutedEventArgs e)
        {
            var prestec = ((Button)sender).DataContext as Loan;
            if (prestec != null)
            {
                ReturnLoan(prestec);
                var repo = new LoansRepository();
                //DgLoansRelated.ItemsSource = repo.GetAll();
            }
            else
            {
                string err = "Ha hagut un problema amb el retorn!";
                MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        public void ReturnLoan(Loan prestec)
        {
            if (prestec != null)
            {
                prestec.ReturnDate = DateTime.Now;
                var memberId = prestec.MemberId;
                var prestecCopy = prestec.Clone();
                try
                {
                    using (var repo = new LoansRepository())
                    {
                        repo.Edit(prestecCopy);
                    }
                }
                catch (Exception ex)
                {
                    string err = ex.Message;
                    MessageBox.Show(err, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ReloadHistoric(memberId);
                ReloadLoads(memberId);
            }
        }
        public void ReloadLoads(Guid id)
        {
            using (var repo = new LoansRepository())
            {
                DgLoansRelated.ItemsSource = null;
                var items = repo.GetAll()
                                .Where(s => s.MemberId == id && s.ReturnDate == null)
                                .ToList();

                DgLoansRelated.ItemsSource = items;
            }
        }
        public void ReloadHistoric(Guid id)
        {
            using (var repo2 = new LoansRepository())
            {
                DgLoansHistoric.ItemsSource = null;
                var items = repo2.GetAll()
                                .Where(s => s.MemberId == id && s.ReturnDate != null)
                                .ToList();
                DgLoansHistoric.ItemsSource = items;
            }
        }
        private void CbxMember_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var member = CbxMember.SelectedItem as Member;
            if (member != null)
            {
                var id = member.Id;
                ReloadLoads(id);
                ReloadHistoric(id);
            }
            else
            {
                DgLoansRelated.ItemsSource = new List<TabControl>();

            }

        }
    }
}
