﻿using Library.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Library.UI.DAL
{
    public class BookCopiesRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<BookCopy> BookCopiesList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public BookCopiesRepository()
        {
            DbContext = new LibraryDbContext();
            LoadDemoData();
        }
        public List<BookCopy> GetAll()
        {
            return DbContext.BookCopies.ToList(); // llegeix la DB
        }

        public BookCopy Get(Guid id)
        {
            var output = DbContext.BookCopies.Find(id);
            return output;
        }
        public List<BookCopy> GetByNum(int number)
        {
            var output = DbContext.BookCopies.Where(s => s.BookCopyNum == number).ToList();
            return output;
        }
        public List<BookCopy> GetByAllByBookId(Guid id)
        {
            var output = DbContext.BookCopies.Where(s => s.BookId == id).ToList();
            return output;
        }
        public List<BookCopy> GetByReturn(bool Avalaible)
        {
            var output = DbContext.BookCopies.Where(s => s.Avalaible == Avalaible).ToList();
            return output;
        }

        public BookCopyValidationsTypes Add(BookCopy bookCopy)
        {
            if (bookCopy.Id != default(Guid))
            {
                return BookCopyValidationsTypes.IdNotEmpty;
            }
            try
            {
                if (DbContext.BookCopies.All(s => s.Id != bookCopy.Id)) // mes eficient
                {
                    bookCopy.Id = Guid.NewGuid();
                    DbContext.BookCopies.Add(bookCopy);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return BookCopyValidationsTypes.Ok;
        }
        public BookCopyValidationsTypes Delete(Guid id)
        {
            try
            {
                var bookCopy = DbContext.BookCopies.Find(id);
                if (bookCopy != null)
                {
                    DbContext.BookCopies.Remove(bookCopy);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return BookCopyValidationsTypes.Ok;
        }
        public BookCopyValidationsTypes Edit(BookCopy bookCopy)
        {
            if (bookCopy.Id == default(Guid))
            {
                MessageBox.Show(BookCopy.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return BookCopyValidationsTypes.IdEmpty;
            }
            if (DbContext.BookCopies.All(s => s.Id != bookCopy.Id))
            {
                MessageBox.Show(BookCopy.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return BookCopyValidationsTypes.CopyNotFound;
            }
            try
            {
                var existingStudent = DbContext.BookCopies.Find(bookCopy.Id);
                existingStudent.ApplyChanges(bookCopy);

                DbContext.BookCopies.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return BookCopyValidationsTypes.Ok;
        }

        #region DATA
        public void LoadDemoData()
        {
            if (DbContext.BookCopies.Count() == 0)
            {
                using (var repo = new BooksRepository())
                {
                    var books = repo.GetAll();
                    foreach (var book in books)
                    {
                        for (var i = 0; i < 4; i++)
                        {
                            var bookCopy = new BookCopy()
                            {
                                BookId = book.Id,
                                BookCopyNum = i,
                                Code = book.Title.Substring(0, 3) + (i+1),
                                Avalaible = false
                            };

                            DbContext.BookCopies.Add(bookCopy);
                        }
                    }
                }
                DbContext.SaveChanges();
            }
        }
        public void AddBookCopies(Book book, int copies)
        {            
            
        }

    #endregion

    public void Dispose()
        {
            
        }
    }
}
