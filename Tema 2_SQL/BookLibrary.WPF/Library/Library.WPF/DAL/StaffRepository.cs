﻿using Library.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Library.UI.DAL
{
    public class StaffsRepository : IDisposable
    {
        public LibraryDbContext DbContext { get; set; }

        public List<Staff> StaffsList
        {
            get
            {
                return GetAll();
            }
        }
        //constructor de la classe
        public StaffsRepository()
        {
            DbContext = new LibraryDbContext();
            DataStaffs();
        }
        public List<Staff> GetAll()
        {
            return DbContext.Staffs.ToList(); // llegeix la DB
        }

        public Staff Get(Guid id)
        {
            var output = DbContext.Staffs.Find(id);
            return output;
        }
        public List<Staff> GetByCategory(string category)
        {
            var output = DbContext.Staffs.Where(s => s.Category == category).ToList();
            return output;
        }
        public List<Staff> GetByName(string name)
        {
            var output = DbContext.Staffs.Where(s => s.Name == name).ToList();
            return output;
        }

        public StaffValidationsTypes Add(Staff staff)
        {
            if (staff.Id != default(Guid))
            {
                return StaffValidationsTypes.IdNotEmpty;
            }

            // +++++  CORREGIR   ++++++++++++++++++

            //var stdWithSameTitle = GetByTitle(staff.Title);
            //if (stdWithSameTitle != null && staff.Id != stdWithSameTitle.Id)
            //{
            //    MessageBox.Show(Staff.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return StaffValidationsTypes.StaffDuplicated;
            //}
            try
            {
                if (DbContext.Staffs.All(s => s.Id != staff.Id)) // mes eficient
                {
                    staff.Id = Guid.NewGuid();
                    DbContext.Staffs.Add(staff);
                    DbContext.SaveChanges();

                    return StaffValidationsTypes.IdDuplicated;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StaffValidationsTypes.IdDuplicated;
        }
        public StaffValidationsTypes Delete(Guid id)
        {
            try
            {
                var staff = DbContext.Staffs.Find(id);
                if (staff != null)
                {
                    DbContext.Staffs.Remove(staff);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StaffValidationsTypes.Ok;
        }
        public StaffValidationsTypes Edit(Staff staff)
        {
            if (staff.Id == default(Guid))
            {
                MessageBox.Show(Staff.ErrBuidID, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StaffValidationsTypes.IdEmpty;
            }
            if (DbContext.Staffs.All(s => s.Id != staff.Id))
            {
                MessageBox.Show(Staff.ErrNoExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return StaffValidationsTypes.StaffNotFound;
            }

            // +++++  CORREGIR   ++++++++++++++++++

            //var stdWithSameTitle = GetByTitle(staff.Title);
            //if (stdWithSameTitle != null && staff.Id != stdWithSameTitle.Id)
            //{
            //    // hay dos estudiantes distintos con mismo dni
            //    MessageBox.Show(Staff.ErrExisteix, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    return StaffValidationsTypes.StaffDuplicated;
            //}
            try
            {
                var existingStudent = DbContext.Staffs.Find(staff.Id);
                existingStudent.ApplyChanges(staff);

                DbContext.Staffs.Update(existingStudent);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return StaffValidationsTypes.Ok;
        }

        #region DATA
        public void DataStaffs()
        {
            if (DbContext.Staffs.Count() == 0)
            {
                var staff1 = new Staff("Maria", "Cardeu", "Molt", "37782599F", "maria@icloud.com", "698784569", "1234", "Librarian");
                DbContext.Staffs.Add(staff1);
                
                DbContext.SaveChanges();
            }
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}
