﻿using Common.Lib.Authentification;
using Library.Lib.Models;
using Library.UI;
using Library.UI.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, iMenuHandler
    {
        static MainWindow Instance { get; set; }


        public MainWindow()
        {
            InitializeComponent();

            Instance = this;
            //LoginView.MenuHandler = this;
           
        }
        public static void LibrarianView(Staff user)
        {
            App.CurrentUser = user;
            Instance.MembersTab.Visibility = Visibility.Visible;
            Instance.BooksTab.Visibility = Visibility.Visible;
            Instance.LoansTab.Visibility = Visibility.Visible;
            Instance.ReturnsTab.Visibility = Visibility.Visible;
            Instance.MyMemberTab.Visibility = Visibility.Collapsed;
            Instance.MyLoansTab.Visibility = Visibility.Collapsed;
            Instance.MyReturnsTab.Visibility = Visibility.Collapsed;

            Instance.TabViews.SelectedIndex = 1;
        }


        public static void UserView(Member user)
        {
            App.CurrentUser = user;
            Instance.MembersTab.Visibility = Visibility.Collapsed;
            Instance.BooksTab.Visibility = Visibility.Collapsed;
            Instance.LoansTab.Visibility = Visibility.Collapsed;
            Instance.ReturnsTab.Visibility = Visibility.Collapsed;
            Instance.MyMemberTab.Visibility = Visibility.Visible;
            Instance.MyLoansTab.Visibility = Visibility.Visible;
            Instance.MyReturnsTab.Visibility = Visibility.Visible;
            Instance.TabViews.SelectedIndex = 5;
        }

        public void OnLoginSuccess()
        {
            
        }
    }
}
