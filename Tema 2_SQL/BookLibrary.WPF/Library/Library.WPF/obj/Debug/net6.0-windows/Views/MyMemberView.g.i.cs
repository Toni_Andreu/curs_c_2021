﻿#pragma checksum "..\..\..\..\Views\MyMemberView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A09AA1CA6C47EE0B4A552114922CABCF00F20174"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Library.UI.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Library.UI.Views {
    
    
    /// <summary>
    /// MyMemberView
    /// </summary>
    public partial class MyMemberView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DadesMember;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbMyMember;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbDni;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbDni;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbName;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbName;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbCognom1;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbSurname1;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbCognom2;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbSurname2;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbPhone;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbPhone;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbEmail;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TbEmail;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbPass;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox TbPass;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock LbStatus;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox TbStatus;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\Views\MyMemberView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtSaveEdit;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Library.UI.WPF;component/views/mymemberview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\MyMemberView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "6.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.DadesMember = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.LbMyMember = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.LbDni = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.TbDni = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.LbName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.TbName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.LbCognom1 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.TbSurname1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.LbCognom2 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.TbSurname2 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.LbPhone = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.TbPhone = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.LbEmail = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.TbEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.LbPass = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 16:
            this.TbPass = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 17:
            this.LbStatus = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 18:
            this.TbStatus = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 19:
            this.BtSaveEdit = ((System.Windows.Controls.Button)(target));
            
            #line 77 "..\..\..\..\Views\MyMemberView.xaml"
            this.BtSaveEdit.Click += new System.Windows.RoutedEventHandler(this.BtEditMyMember_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

