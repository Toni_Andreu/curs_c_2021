﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Lib.Models
{
    public class Book : Entity
    {
        public string Title { get; set; }
        public string? Synopsis { get; set; }
        public string Author { get; set; }
        public string? ISBN { get; set; }
        public string Edition { get; set; }
        public int Year { get; set; }
        public string? Publisher { get; set; }
        public string? Description { get; set; }
        public string? Signature { get; set; }
        public string TitleAuthor => $"{Title} -{Author}";
        public virtual List<BookCopy> BookCopies { get; set; }

        public Book()
        {

        }
        public Book(string title, string synopis, string author, string isbn, string edition, int year, string publisher, string description, string signature)
        {
            this.Id = Guid.NewGuid();
            this.Title = title;
            this.Synopsis = synopis;
            this.Author = author;
            this.ISBN = isbn;
            this.Edition = edition;
            this.Year = year;
            this.Publisher = publisher;
            this.Description = description;
            this.Signature = signature;

        }
        public Book Clone()
        {
            return Clone<Book>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Book;

            output.Title = this.Title;
            output.Synopsis = this.Synopsis;
            output.Author = this.Author;
            output.ISBN = this.ISBN;
            output.Edition = this.Edition;
            output.Year = this.Year;
            output.Publisher = this.Publisher;
            output.Description = this.Description;
            output.Signature = this.Signature;

            return output as T;
        }


        public void ApplyChanges(Book book)
        {
            ApplyChanges<Book>(book);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var book = entity as Book;

            if (book == null)
                throw new Exception("entity is not of type book");

            this.Title = book.Title;
            this.Synopsis = book.Synopsis;
            this.Author = book.Author;
            this.ISBN = book.ISBN;
            this.Edition = book.Edition;
            this.Year = book.Year;
            this.Publisher = book.Publisher;
            this.Description = book.Description;
            this.Signature = book.Signature;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        #endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidAuthor = "L'autor no pot estar buid!";
        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidTitle = "El títol del llibre no pot estar buid!";
        // DNI existent
        public const string ErrExisteix = "Aquest títol ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix aquest ´titol:";
        internal static readonly object Books;
        #endregion
    }
    public enum BookValidationsTypes
    {
        Ok,
        BookDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        BookNotFound
    }
}