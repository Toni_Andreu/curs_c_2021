﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Lib.Models
{
    public class Loan : Entity
    {
        public DateTime LoanDate { get; set; }
        public DateTime? ReturnDate { get; set; }

        public Guid BookCopyId { get; set; }
        public BookCopy BookCopy { get; set; }
        public Guid MemberId { get; set; }
        public Member Member { get; set; }
        public Loan()
        {

        }

        public Loan (DateTime loanDate, DateTime returnDate)
        {
            this.LoanDate = loanDate;
            this.ReturnDate = returnDate;
        }

        public Loan Clone()
        {
            return Clone<Loan>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Loan;

            output.LoanDate = this.LoanDate;
            output.ReturnDate = this.ReturnDate;
            output.BookCopyId = this.BookCopyId;
            output.MemberId = this.MemberId;

            return output as T;
        }


        public void ApplyChanges(Loan loan)
        {
            ApplyChanges<Loan>(loan);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var load = entity as Loan;

            if (load == null)
                throw new Exception("entity is not of type book");

            this.LoanDate = load.LoanDate;
            this.ReturnDate = load.ReturnDate;
            this.BookCopyId = load.BookCopyId;
            this.MemberId = load.MemberId;
        }

        #region CONSTANTS

        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "La data no pot estar buida!";
        public const string ErrNoExisteix = "NO existeix aquest prèstec";
        internal static readonly object Loans;
        #endregion

    }
    public enum LoanValidationsTypes
    {
        Ok,
        WrongFormat,
        LoanDuplicated,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        LoanNotFound
    }
}
