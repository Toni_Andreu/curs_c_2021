﻿using Common.Lib.Authentification;
using System;
using System.Collections.Generic;

namespace Library.Lib.Models
{
    public class Member : User
    {
        public string Name { get; set; }
        public string Surname1 { get; set; }
        public string Surname2 { get; set; }
        public string Dni { get; set; }
        public string Email { get; set; }
        public string ContactPhone { get; set; }
        public string Pass { get; set; }
        public bool? Status { get; set; }
        public string FullName => $"{Name} {Surname1} {Surname2}";
        public virtual List<Loan> Loans { get; set; }

        public Member()
        {

        }

        //constructor per carrega
        public Member(string name, string surname1, string surname2, string dni, string email, string contactPhone, string pass, bool status)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
            this.Surname1 = surname1;
            this.Surname2 = surname2;
            this.Dni = dni;
            this.Email = email;
            this.ContactPhone = contactPhone;
            this.Pass = pass;
            this.Status = status;
        }

        public Member Clone()
        {
            return Clone<Member>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as Member;

            output.Name = this.Name;
            output.Surname1 = this.Surname1;
            output.Surname2 = this.Surname2;
            output.Dni = this.Dni;
            output.Email = this.Email;
            output.Dni = this.ContactPhone;
            output.Pass = this.Pass;
            output.Status = this.Status;

            return output as T;
        }


        public void ApplyChanges(Member member)
        {
            ApplyChanges<Member>(member);
        }

        public override void ApplyChanges<T>(T entity)
        {
            base.ApplyChanges(entity);

            var member = entity as Member;

            if (member == null)
                throw new Exception("entity is not of type member");

            this.Name = member.Name;
            this.Surname1 = member.Surname1;
            this.Surname2 = member.Surname2;
            this.Dni = member.Dni;
            this.Email = member.Email;
            this.ContactPhone = member.ContactPhone;
            this.Pass = member.Pass;
            this.Status = member.Status;
        }

        #region static VALIDACIONS
        // Nom not empty
        public static bool ValidateEmpty(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        // Nom Length = 9
        public static bool ValidateLength(string nom)
        {
            if (nom.Length != 9)
            {
                return true;
            }
            return false;
        }

        // Dni existeix
        public static bool DniExist(string nom)
        {
            return false;
        }

        // Nom existent
        public static bool ValidateExist(string nom)
        {
            if (string.IsNullOrEmpty(nom.Trim()))
            {
                return true;
            }
            return false;
        }

        #endregion

        #region CONSTANTS
        // DNI not empty
        public const string ErrBuidDni = "El dni no pot estar buid!";
        // ID not empty
        public const string ErrBuidID = "l'ID no pot estar buid!";
        // Nom not empty
        public const string ErrBuidNom = "El Nom del Lector no pot estar buid!";
        public const string ErrBuidSurname1 = "El Cognom del Lector no pot estar buid!";
        // DNI Length = 9
        public const string ErrNo9 = "El dni està en un format incorrecte\n" +
            "(xxxxxxxxL)";
        public const string ErrPhoneNo9 = "El telèfon ha de tenir 9 dígits";
        // DNI existent
        public const string ErrExisteix = "Aquest Dni ja existeix!";
        // DNI NO existent
        public const string ErrNoExisteix = "NO existeix el DNI:";
        internal static readonly object Customers;
        #endregion
    }
    public enum MemberValidationsTypes
    {
        Ok,
        WrongDniFormat,
        DniDuplicated,
        WrongNameFormat,
        IdNotEmpty,
        IdDuplicated,
        IdEmpty,
        StudentNotFound
    }

}

