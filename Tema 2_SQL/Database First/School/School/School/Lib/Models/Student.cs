//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Academy.Lib.Models
{
    using School.Lib.Models;
    using System;
    using System.Collections.Generic;
    
    public partial class Student : Entity
    {
        public string name { get; set; }
        public string email { get; set; }
        public string dni { get; set; }
    
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
