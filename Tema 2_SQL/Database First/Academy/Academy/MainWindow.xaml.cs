﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Academy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var repository = new AcademyDbContext();
            var students = repository.Students.ToList();


            if (repository.Students.Count() == 0)
            {
                var student1 = new Student()
                {
                    name = "Raimon",
                    dni = "37765435S",
                    email = "pe@pep.com"
                };

                var subject1 = new Subject()
                {
                    name = "Ciberseguretat"
                };
                repository.Students.Add(student1);
                repository.Subjects.Add(subject1);
                repository.SaveChanges();


                var registration1 = new Registration()
                {
                    StudentId = student1.id,
                    SubjectId = subject1.id,
                    date = DateTime.Now
                };
                repository.Registrations.Add(registration1);
                repository.SaveChanges();


            }
            DgStudents.ItemsSource = repository.Students.ToList();
            DgSubjects.ItemsSource = repository.Subjects.ToList();
            DgRegistrations.ItemsSource = repository.Registrations.ToList();
            DgTeachers.ItemsSource = repository.Teachers.ToList();
            DgAsssignements.ItemsSource = repository.Assignements.ToList();
        }

    }
}
